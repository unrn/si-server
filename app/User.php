<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
		protected $casts = [
				'id' => 'integer',
				'type' => 'boolean',
				'avatar' => 'string'
		];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

		/**
		 * Validation rules
		 *
		 * @var array
		 */
		public static $rules = [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users|regex:/(.*)unrn\.edu\.ar$/i',//solo acepta dominios unrn.edu.ar
			'password' => 'required|min:6|confirmed',
			'type' => 'required',
			'avatar' => 'image|mimes:jpeg,jpg,png'
		];

		public function getHistorialIncentivos()
		{
				return $this->hasMany('App\Models\HistorialIncentivo')->orderBy('id')->get();
		}
}
