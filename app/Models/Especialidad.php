<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Especialidad extends Model
{

    public $table = 'especialidades';
    public $timestamps = false;

    public $fillable = [
        'nombre',
				'area_tematica_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
				'area_tematica_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|unique:especialidades,nombre',
				'area_tematica_id' => 'required'
    ];

		public function areaTematica()
		{
			return $this->belongsTo('App\Models\AreaTematica')->first();
		}
}
