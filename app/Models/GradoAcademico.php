<?php namespace App\Models;

use Eloquent as Model;

class GradoAcademico extends Model
{
	public $table = 'grado_academicos';

	public $fillable = [
			'nombre'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
			'nombre' => 'string',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
			'nombre' => 'required',
	];

	public function getHistorialAcademicos()
	{
			return $this->hasMany('App\Models\HistorialAcademico')->orderBy('id')->get();
	}
}
