<?php namespace App\Models;

use Eloquent as Model;

class HistorialIncentivo extends Model
{
	public $table = 'historial_incentivos';
	public $timestamps = false;

	public $fillable = [
			'investigador_id',
			'cat_incentivo_id',
			'fecha_inicio',
			'fecha_fin',
			'user_id',
			'update'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'investigador_id' => 'string',
		'cat_incentivo_id' => 'string',
		'fecha_inicio' => 'string',
		'fecha_fin' => 'string',
		'user_id' => 'string',
		'update' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'investigador_id' => 'required',
		'cat_incentivo_id' => 'required',
		'fecha_inicio' => 'required',
		'fecha_fin' => 'required',
		'user_id' => 'required',
		'update' => 'required'
	];

	public function investigador()
	{
			return $this->belongsTo('App\Models\Investigador')->first();
	}

	public function categoriaIncentivo()
	{
			return $this->belongsTo('App\Models\CategoriaIncentivo', 'cat_incentivo_id')->first();
	}

	public function user()
	{
			return $this->belongsTo('App\User', 'user_id')->first();
	}
}
