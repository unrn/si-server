<?php namespace App\Models;

use Eloquent as Model;

class HistorialLugarTrabajo extends Model
{
	public $table = 'historial_lugar_trabajos';
	public $timestamps = false;

	public $fillable = [
			'investigador_id',
			'lugar_trabajo_id',
			'rol_id',
			'fecha_inicio',
			'fecha_fin',
			'user',
			'update'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'investigador_id' => 'string',
		'lugar_trabajo_id' => 'string',
		'rol_id' => 'string',
		'fecha_inicio' => 'string',
		'fecha_fin' => 'string',
		'user' => 'string',
		'update' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'investigador_id' => 'required',
		'lugar_trabajo_id' => 'required',
		'rol_id' => 'required',
		'fecha_inicio' => 'required',
		'fecha_fin' => 'required'
	];

	public function investigador()
	{
			return $this->belongsTo('App\Models\Investigador')->first();
	}

	public function lugarTrabajo()
	{
			return $this->belongsTo('App\Models\LugarTrabajo')->first();
	}

	public function rol()
	{
			return $this->belongsTo('App\Models\Rol')->first();
	}
}
