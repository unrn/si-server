<?php namespace App\Models;

use Eloquent as Model;
use App\Models\GradoAcademico;
use App\Models\CategoriaIncentivo;
use Illuminate\Database\Eloquent\SoftDeletes;
//Facades
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

/**
 * @SWG\Definition(
 *      definition="Investigador",
 *      required={legajo, apellido, nombre, dni, cuil, fecha_nac, sexo, cat_incentivo, grado_academico},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="apellido",
 *          description="apellido",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fecha_nac",
 *          description="fecha_nac",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="sexo",
 *          description="sexo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email_personal",
 *          description="email_personal",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email_institucional",
 *          description="email_institucional",
 *          type="string"
 *      )
 * )
 */
class Investigador extends Model
{
    public $table = 'investigadores';
		public $timestamps = false;

    public $fillable = [
        'legajo',
        'apellido',
        'nombre',
        'dni',
        'cuil',
        'fecha_nac',
        'sexo',
        'email_personal',
        'email_institucional',
				'especialidad_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'apellido' => 'string',
        'nombre' => 'string',
				'dni' => 'string',
				'cuil' => 'string',
        'fecha_nac' => 'string',
        'sexo' => 'string',
        'email_personal' => 'string',
        'email_institucional' => 'string',
				'especialidad_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'legajo' => 'unique:investigadores,legajo',
        'apellido' => 'required',
        'nombre' => 'required',
        'dni' => 'required|min:7|max:10|unique:investigadores,dni',
        'cuil' => 'required|min:10|max:11|unique:investigadores,cuil',
        'fecha_nac' => 'required',
        'sexo' => 'required',
        'email_personal' => 'email',
        'email_institucional' => 'email',
				'especialidad_id' => 'required'
    ];

		public function especialidad()
		{
			return $this->belongsTo('App\Models\Especialidad')->first();
		}

		public function getHistorialRoles()
		{
				return $this->hasMany('App\Models\HistorialRol')->orderBy('id')->get();
		}

		public function getHistorialAcademicos()
		{
				return $this->hasMany('App\Models\HistorialAcademico')->orderBy('id')->get();
		}

		public function getHistorialIncentivos()
		{
				return $this->hasMany('App\Models\HistorialIncentivo')->orderBy('id')->get();
		}

		public function getHistorialLugarTrabajos()
		{
				return $this->hasMany('App\Models\HistorialLugarTrabajo')->orderBy('id')->get();
		}

		public function getUltimoHistorialAcademico()
		{
			return HistorialAcademico::where('investigador_id', $this->id)->orderBy('fecha_inicio', 'DESC')->first();
		}

		public function getUltimoHistorialIncentivo()
		{
			return HistorialIncentivo::where('investigador_id', $this->id)->orderBy('fecha_inicio', 'DESC')->first();
		}

		public function getUltimoHistorialLugarTrabajo()
		{
			return HistorialLugarTrabajo::where('investigador_id', $this->id)->orderBy('fecha_inicio', 'DESC')->first();
		}

		public function getUltimoHistorialRol()
		{
			return HistorialRol::where('investigador_id', $this->id)->orderBy('fecha_inicio', 'DESC')->first();
		}

		public function getIntegrantes()
		{
				return $this->hasMany('App\Models\Integrante')->orderBy('id')->get();
		}

		public function fullName()
		{
				return $this->apellido . ', ' . $this->nombre;
		}

		public function isExtern() {
			$sedes = [1,2,3,4];//las sedes y el rectorado son los primeros 4 ids del lugar de trabajo
			return !in_array($this->getUltimoHistorialLugarTrabajo()->lugarTrabajo()->institucion()->id, $sedes);
		}
}
