<?php namespace App\Models;

use Eloquent as Model;

class Proyecto extends Model
{
    public $table = 'proyectos';
		public $timestamps = false;
    public $fillable = [
        'codigo',
        'titulo',
        'resumen',
        'fecha_inicio',
        'fecha_fin',
        'fecha_prorroga',
        'duracion',
        'convocatoria',
        'acreditado',
        'tipo_investigacion',
        'monto',
        'area_tematica_id',
				'area_sigeva_id',
        'institucion_id',
        'tipo_id',
				'resolucion_alta_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'codigo' => 'string|unique:proyectos',
        'titulo' => 'string',
        'resumen' => 'string',
				'fecha_inicio' => 'string',
        'fecha_fin' => 'string',
        'fecha_prorroga' => 'string',
        'duracion' => 'string',
        'convocatoria' => 'string',
        'acreditado' => 'string',
        'tipo_investigacion' => 'string',
        'monto' => 'string',
        'area_tematica_id' => 'string',
				'area_sigeva_id' => 'string',
        'institucion_id' => 'string',
        'tipo_id' => 'string',
				'resolucion_alta_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'codigo' => 'required|unique:proyectos',
        'titulo' => 'required',
        'resumen' => 'required',
        'fecha_inicio' => 'required',
        'fecha_fin' => 'required',
        'duracion' => 'required',
        'convocatoria' => 'required',
        'tipo_investigacion' => 'required',
        'monto' => 'required',
        'area_tematica_id' => 'required',
				'area_sigeva_id' => 'required',
        'institucion_id' => 'required',
        'tipo_id' => 'required',
				'resolucion_alta_id' => 'required|exists:resolucion_altas,id'
    ];

		public function areaTematica()
		{
			return $this->belongsTo('App\Models\AreaTematica')->first();
		}

		public function areaSigeva()
		{
			return $this->belongsTo('App\Models\AreaSigeva')->first();
		}

		public function tipoProyecto()
		{
			return $this->belongsTo('App\Models\TipoProyecto', 'tipo_id')->first();
		}

		public function resolucionAlta()
		{
			return $this->belongsTo('App\Models\ResolucionAlta')->first();
		}

		public function institucion()
		{
			return $this->belongsTo('App\Models\Institucion')->first();
		}

		public function getIntegrantes()
		{
				return $this->hasMany('App\Models\Integrante')->orderBy('perfil_id')->get();
		}

		public function getInformeProyectos()
		{
				return $this->hasMany('App\Models\InformeProyecto')->orderBy('id')->get();
		}

		public function existeDirector($perfilActual, $perfilNuevo)
		{
			if ($perfilActual != 1 && $perfilNuevo == 1) {
				foreach ($this->getIntegrantes() as $integrante) {
					if ($integrante->perfil_id == 1) {
						return true;
					}
				}
			}
			return false;
		}
}
