<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaTematica extends Model
{
    public $table = 'area_tematicas';
    public $timestamps = false;

    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required'
    ];

		public function getEspecialidades()
		{
				return $this->hasMany('App\Models\Especialidad')->orderBy('id')->get();
		}
}
