<?php namespace App\Models;

use Eloquent as Model;

class CategoriaIncentivo extends Model
{
	public $table = 'categoria_incentivos';

	public $fillable = [
			'nombre'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
			'nombre' => 'string',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
			'nombre' => 'required',
	];

	public function getHistorialIncentivos()
	{
			return $this->hasMany('App\Models\HistorialIncentivo')->orderBy('id')->get();
	}
}
