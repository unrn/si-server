<?php namespace App\Models;

use Eloquent as Model;

class ResolucionAlta extends Model
{

    public $table = 'resolucion_altas';
		public $timestamps = false;
    public $fillable = [
        'numero',
        'fecha',
        'fecha_inicio',
        'fecha_fin_anual',
        'fecha_presentacion_1',
        'fecha_fin_bienal',
        'fecha_presentacion_2',
        'fecha_fin_trienal',
        'fecha_presentacion_3',
				'path',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'numero' => 'string',
        'fecha' => 'string',
        'fecha_inicio' => 'string',
        'fecha_fin_anual' => 'string',
        'fecha_presentacion_1' => 'string',
        'fecha_fin_bienal' => 'string',
        'fecha_presentacion_2' => 'string',
        'fecha_fin_trienal' => 'string',
        'fecha_presentacion_3' => 'string',
				'path' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'numero' => 'required|unique:resolucion_altas',
        'fecha' => 'required',
        'fecha_inicio' => 'required',
        'fecha_fin_anual' => 'required',
        'fecha_presentacion_1' => 'required',
        'fecha_fin_bienal' => 'required',
        'fecha_presentacion_2' => 'required',
        'fecha_fin_trienal' => 'required',
        'fecha_presentacion_3' => 'required'
    ];

		public function getProyectos()
		{
				return $this->hasMany('App\Models\Proyecto')->orderBy('id')->get();
		}
}
