<?php namespace App\Models;

use Eloquent as Model;

class InformeProyecto extends Model
{
    public $table = 'informe_proyectos';
		public $timestamps = false;

    public $fillable = [
        'tipo',
        'descripcion',
        'fecha_presentacion',
        'fecha_entrega_papel',
				'fecha_entrega_digital',
				'proyecto_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tipo' => 'string',
        'descripcion' => 'string',
        'fecha_presentacion' => 'string',
        'fecha_entrega_papel' => 'string',
				'fecha_entrega_digital' => 'string',
				'proyecto_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tipo' => 'required',
        'fecha_presentacion' => 'required',
				'proyecto_id' => 'required'
    ];

		public function proyecto()
		{
			return $this->belongsTo('App\Models\Proyecto')->first();
		}
}
