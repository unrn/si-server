<?php namespace App\Models;

use Eloquent as Model;

class HistorialAcademico extends Model
{
	public $table = 'historial_academicos';
	public $timestamps = false;

	public $fillable = [
			'investigador_id',
			'grado_academico_id',
			'fecha_inicio',
			'fecha_fin',
			'user',
			'update'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'investigador_id' => 'string',
		'grado_academico_id' => 'string',
		'fecha_inicio' => 'string',
		'fecha_fin' => 'string',
		'user' => 'string',
		'update' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'investigador_id' => 'required',
		'grado_academico_id' => 'required',
		'fecha_inicio' => 'required',
		'fecha_fin' => 'required',
		'user' => 'required',
		'update' => 'required'
	];

	public function investigador()
	{
			return $this->belongsTo('App\Models\Investigador')->first();
	}

	public function gradoAcademico()
	{
			return $this->belongsTo('App\Models\GradoAcademico')->first();
	}
}
