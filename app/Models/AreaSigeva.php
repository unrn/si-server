<?php namespace App\Models;

use Eloquent as Model;

class AreaSigeva extends Model
{

    public $table = 'area_sigevas';
    public $timestamps = false;

    public $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|unique:area_sigevas'
    ];

		public function getAreaSigevas()
		{
				return $this->hasMany('App\Models\AreaSigeva')->orderBy('id')->get();
		}
}
