<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Institucion",
 *      required={nombre, abreviacion},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="abreviacion",
 *          description="abreviacion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Institucion extends Model
{

    public $table = 'instituciones';

    public $fillable = [
        'nombre',
        'abreviacion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'abreviacion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'abreviacion' => 'required|unique:instituciones,abreviacion'
    ];

		public function getHistorialRoles()
		{
				return $this->hasMany('App\Models\HistorialRol')->orderBy('id')->get();
		}

		public function getLugarTrabajos()
		{
				return $this->hasMany('App\Models\LugarTrabajo')->orderBy('id')->get();
		}

		public function getProyectos()
		{
				return $this->hasMany('App\Models\Proyecto')->orderBy('id')->get();
		}
}
