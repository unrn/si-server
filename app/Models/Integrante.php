<?php namespace App\Models;

use Eloquent as Model;

class Integrante extends Model
{
	public $table = 'integrantes';
	public $timestamps = false;
	public $fillable = [
			'proyecto_id',
			'investigador_id',
			'perfil_id',
			'fecha_alta',
			'fecha_baja',
			'horas'
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'proyecto_id' => 'string',
		'investigador_id' => 'string',
		'perfil_id' => 'string',
		'fecha_alta' => 'string',
		'fecha_baja' => 'string',
		'horas' => 'string'
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [
		'proyecto_id' => 'required',
		'investigador_id' => 'required',
		'perfil_id' => 'required',
		'fecha_alta' => 'required',
		'fecha_baja' => 'required',
		'horas' => 'required'
	];

	public function investigador()
	{
		return $this->belongsTo('App\Models\Investigador')->first();
	}

	public function perfil()
	{
		return $this->belongsTo('App\Models\Perfil')->first();
	}

	public function proyecto()
	{
		return $this->belongsTo('App\Models\Proyecto')->first();
	}
}
