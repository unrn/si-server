<?php namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="LugarTrabajo",
 *      required={nombre, abreviacion},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="abreviacion",
 *          description="abreviacion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class LugarTrabajo extends Model
{
    public $table = 'lugar_trabajos';
		public $timestamps = false;
    public $fillable = [
        'nombre',
        'abreviacion',
				'institucion_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'abreviacion' => 'string',
				'institucion_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'abreviacion' => 'required|unique:lugar_trabajos',
				'institucion_id' => 'required'
    ];

		public function institucion()
		{
			return $this->belongsTo('App\Models\Institucion')->first();
		}

		public function getHistorialLugarTrabajos()
		{
				return $this->hasMany('App\Models\HistorialLugarTrabajo')->orderBy('id')->get();
		}
}
