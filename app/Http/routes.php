<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'web'],function (){
	//Nothing here
});
Route::get('/', 'HomeController@index');
// LogsViewer
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
// Login Routes...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');
// Password Reset Routes...
Route::get('password/reset', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//Grupo middleware => auth
Route::group(['middleware' => ['auth']], function ()
{
	/*
	 | API SuperAdministrator Routes
	*/
	Route::group(['middleware' => ['super.admin']], function () {
		//Usuarios
		Route::resource('usuarios', 'UserController');
	});
	/*
	 | API Administrator Routes
	*/
	Route::group(['middleware' => ['admin']], function () {
		//Users Registration Routes...
		Route::get('register', 'Auth\AuthController@getRegister');
		Route::post('register', 'Auth\AuthController@postRegister');
		//Lugares de Trabajo
		Route::resource('lugarTrabajos', 'LugarTrabajoController');
		//Instituciones
		Route::resource('instituciones', 'InstitucionController');
		//Roles
		Route::resource('roles', 'RolController');
		//Investigadores
		Route::resource('investigadores', 'InvestigadorController');
		Route::get('investigadores/{id}/historial', [ 'as' => 'investigadores.historial', 'uses' => 'InvestigadorController@historial']);
		//Historial Academico
		Route::resource('historialAcademicos', 'HistorialAcademicoController');
		//Historial Catergoria Incentivo
		Route::resource('historialIncentivos', 'HistorialIncentivoController');
		//Historial Lugar de Trabajo
		Route::resource('historialLugarTrabajos', 'historialLugarTrabajoController');
		//Historial Roles
		Route::resource('historialRoles', 'HistorialRolController');
		//Areas Tematicas
		Route::resource('areaTematicas', 'AreaTematicaController');
		//Especialidades
		Route::resource('especialidades', 'EspecialidadController');
		//Perfiles de Integrantes
		Route::resource('perfiles', 'PerfilController');
		//Tipo de Proyectos
		Route::resource('tipoProyectos', 'TipoProyectoController');
		//Proyectos
		Route::resource('proyectos', 'ProyectoController');
		//Resolucion Alta
		Route::resource('resolucionAltas', 'ResolucionAltaController');
		Route::get('autocomplete/resolucionAltas', [ 'as' => 'resolucionAltas.autocomplete', 'uses' => 'ResolucionAltaController@autocomplete']);
		//Areas de Sigeva
		Route::resource('areaSigevas', 'AreaSigevaController');
		//Integrantes
		Route::resource('integrantes', 'IntegranteController');
		//Informe de Proyectos
		Route::resource('informeProyectos', 'InformeProyectoController');
		Route::put('proyectos/{id}/informes', [ 'as' => 'informeProyectos.updateFrom', 'uses' => 'InformeProyectoController@updateFrom']);
	});
	/*
	 | API No Administrator Routes
	*/
	// Home
	Route::get('/home', 'HomeController@index');
	Route::get('contact', ['as' => 'contact.get', 'uses' => 'HomeController@getContact']);
	Route::post('contact', ['as' => 'contact.send', 'uses' => 'HomeController@postContact']);
	// Perfil de Usuario
	Route::get('/profile', ['as' => 'users.profile', 'uses' => 'UserController@profile']);
	Route::post('/profile/resetPassword', ['as' => 'users.profile.resetPassword', 'uses' => 'UserController@resetPassword']);
	Route::post('/profile/updateAvatar', ['as' => 'users.profile.updateAvatar', 'uses' => 'UserController@updateAvatar']);
	// Investigadores
	Route::resource('investigadores', 'InvestigadorController', ['only' => ['index','show']]);
	// Proyectos
	Route::resource('proyectos', 'ProyectoController', ['only' => ['index','show']]);
	// Autocompletes
	Route::get('autocomplete/investigadores', [ 'as' => 'investigadores.autocomplete', 'uses' => 'InvestigadorController@autocomplete']);
	Route::get('autocomplete/proyectos', [ 'as' => 'proyectos.autocomplete', 'uses' => 'ProyectoController@autocomplete']);
	// Reportes
	Route::get('reportes/integrantes', [ 'as' => 'reportes.integrantes', 'uses' => 'ReporteController@integrantes']);
	Route::get('reportes/integrantes/get', [ 'as' => 'reportes.integrantes.get', 'uses' => 'ReporteController@getReporteIntegrantes']);
	Route::get('reportes/proyectos', [ 'as' => 'reportes.proyectos', 'uses' => 'ReporteController@proyectos']);
	Route::get('reportes/proyectos/get', [ 'as' => 'reportes.proyectos.get', 'uses' => 'ReporteController@getReporteProyectos']);
	Route::get('reportes/proyectosActivos/get', [ 'as' => 'reportes.proyectosActivos.get', 'uses' => 'ReporteController@getReporteProyectosActivos']);
});
