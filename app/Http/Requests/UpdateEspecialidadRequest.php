<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Especialidad;

class UpdateEspecialidadRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rulesUpdate = Especialidad::$rules;
				$rulesUpdate['nombre'] = 'required|unique:especialidades,nombre,'.$this->route('especialidades');
				return $rulesUpdate;
    }
}
