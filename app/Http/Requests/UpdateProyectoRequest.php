<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Proyecto;

class UpdateProyectoRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
			$rulesUpdate = Proyecto::$rules;
			$rulesUpdate['codigo'] = 'required|unique:proyectos,codigo,'.$this->route('proyectos');
			return $rulesUpdate;
    }
}
