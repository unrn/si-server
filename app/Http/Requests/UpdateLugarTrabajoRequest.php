<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\LugarTrabajo;

class UpdateLugarTrabajoRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
				$rulesUpdate = LugarTrabajo::$rules;
				$rulesUpdate['abreviacion'] = 'required|unique:lugar_trabajos,abreviacion,'.$this->route('lugarTrabajos');
				return $rulesUpdate;
    }
}
