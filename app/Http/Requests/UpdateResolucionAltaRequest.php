<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\ResolucionAlta;

class UpdateResolucionAltaRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
			$rulesUpdate = ResolucionAlta::$rules;
			$rulesUpdate['numero'] = 'required|unique:resolucion_altas,numero,'.$this->route('resolucionAltas');
			return $rulesUpdate;
    }
}
