<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Investigador;

class UpdateInvestigadorRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rulesUpdate = Investigador::$rules;
				$rulesUpdate['legajo'] = 'unique:investigadores,legajo,'.$this->route('investigadores');
				$rulesUpdate['dni'] = 'required|min:7|max:8|unique:investigadores,dni,'.$this->route('investigadores');
        $rulesUpdate['cuil'] = 'required|min:10|max:11|unique:investigadores,cuil,'.$this->route('investigadores');
				return $rulesUpdate;
    }
}
