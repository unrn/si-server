<?php namespace App\Http\Controllers;

use App\DataTables\ProyectoDataTable;
use Illuminate\Http\Request;
use App\Http\Requests\CreateProyectoRequest;
use App\Http\Requests\UpdateProyectoRequest;
use App\Repositories\ProyectoRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
//Models
use App\Models\AreaTematica;
use App\Models\Institucion;
use App\Models\TipoProyecto;
use App\Models\AreaSigeva;
use App\Models\InformeProyecto;
use App\Models\Proyecto;

class ProyectoController extends AppBaseController
{
    /** @var  ProyectoRepository */
    private $proyectoRepository;

    public function __construct(ProyectoRepository $proyectoRepo)
    {
        $this->proyectoRepository = $proyectoRepo;
    }

    /**
     * Display a listing of the Proyecto.
     *
     * @param ProyectoDataTable $proyectoDataTable
     * @return Response
     */
    public function index(ProyectoDataTable $proyectoDataTable)
    {
        return $proyectoDataTable->render('proyectos.index');
    }

    /**
     * Show the form for creating a new Proyecto.
     *
     * @return Response
     */
    public function create()
    {
			$instituciones = Institucion::lists('nombre', 'id')->all();
			$tematicas = AreaTematica::lists('nombre', 'id')->all();
			$tipo_proyectos = TipoProyecto::lists('abreviacion', 'id')->all();
			$area_sigevas = AreaSigeva::lists('nombre', 'id')->all();
      return view('proyectos.create')
				->with('instituciones', $instituciones)
				->with('tematicas', $tematicas)
				->with('tipo_proyectos', $tipo_proyectos)
				->with('area_sigevas', $area_sigevas);
    }

    /**
     * Store a newly created Proyecto in storage.
     *
     * @param CreateProyectoRequest $request
     *
     * @return Response
     */
    public function store(CreateProyectoRequest $request)
    {
        $input = $request->all();
				$input['fecha_prorroga'] = $request->fecha_prorroga == null ? null : $request->fecha_prorroga;
        $proyecto = $this->proyectoRepository->create($input);
				$this->crearInformeProyecto($proyecto);
        Flash::success('Proyecto creado exitosamente');
        return redirect(route('proyectos.index'));
    }

    /**
     * Display the specified Proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $proyecto = $this->proyectoRepository->findWithoutFail($id);
        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');
            return redirect(route('proyectos.index'));
        }
        $instituciones = Institucion::lists('nombre', 'id')->all();
				$tematicas = AreaTematica::lists('nombre', 'id')->all();
				$tipo_proyectos = TipoProyecto::lists('abreviacion', 'id')->all();
				$area_sigevas = AreaSigeva::lists('nombre', 'id')->all();
        return view('proyectos.edit')
					->with('proyecto', $proyecto)
					->with('instituciones', $instituciones)
					->with('tematicas', $tematicas)
					->with('tipo_proyectos', $tipo_proyectos)
					->with('integrantes', $proyecto->getIntegrantes())
					->with('area_sigevas', $area_sigevas)
					->with('term', $proyecto->resolucionAlta()->numero)
					->with('informes', $proyecto->getInformeProyectos());
    }

    /**
     * Show the form for editing the specified Proyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $proyecto = $this->proyectoRepository->findWithoutFail($id);
        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');
            return redirect(route('proyectos.index'));
        }
				$instituciones = Institucion::lists('nombre', 'id')->all();
				$tematicas = AreaTematica::lists('nombre', 'id')->all();
				$tipo_proyectos = TipoProyecto::lists('abreviacion', 'id')->all();
				$area_sigevas = AreaSigeva::lists('nombre', 'id')->all();
        return view('proyectos.edit')
					->with('proyecto', $proyecto)
					->with('instituciones', $instituciones)
					->with('tematicas', $tematicas)
					->with('tipo_proyectos', $tipo_proyectos)
					->with('integrantes', $proyecto->getIntegrantes())
					->with('area_sigevas', $area_sigevas)
					->with('term', $proyecto->resolucionAlta()->numero)
					->with('informes', $proyecto->getInformeProyectos());
    }

    /**
     * Update the specified Proyecto in storage.
     *
     * @param  int              $id
     * @param UpdateProyectoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProyectoRequest $request)
    {
        $proyecto = $this->proyectoRepository->findWithoutFail($id);
        if (empty($proyecto)) {
            Flash::error('Proyecto no encontrado');
            return redirect(route('proyectos.index'));
        }
        $proyecto = $this->proyectoRepository->update($request->all(), $id);
        Flash::success('Proyecto modificado exitosamente.');
        return redirect(route('proyectos.index'));
    }

    /**
     * Remove the specified Proyecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return view('errors.403');
    }

		public function autocomplete(Request $request)
		{
			$term = $request->term;
			$results = array();
			$proyectos = Proyecto::where('codigo', 'LIKE', '%'.$term.'%')
				->take(5)->get();
			foreach ($proyectos as $proy) {
				$results[] = [ 'id' => $proy->id, 'value' => $proy->codigo ];
			}
			return Response::json($results);
		}

		private function crearInformeProyecto($proyecto)
		{
			$duracion = ['Anual' => 1, 'Bienal' => 2, 'Trienal' => 3];
			for ($i=1; $i <= $duracion[$proyecto->duracion]; $i++) {
				InformeProyecto::create([
					'tipo' => $i == $duracion[$proyecto->duracion] ? 'Final' : 'Avance',
					'descripcion' => 'Agregar descripción',
					'fecha_presentacion' => $proyecto->resolucionAlta()['fecha_presentacion_'.$i],
					'proyecto_id' => $proyecto->id
				]);
			}
		}
}
