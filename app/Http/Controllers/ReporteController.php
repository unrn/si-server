<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DataTables\ReporteIntegranteDataTable;
use App\DataTables\ReporteProyectoDataTable;
use App\DataTables\EspecialidadDataTable;
use App\Http\Requests;
//Models
use App\Models\AreaTematica;
use App\Models\AreaSigeva;
use App\Models\Investigador;
use App\Models\Institucion;
use App\Models\Proyecto;
use App\Models\TipoProyecto;
use App\Models\CategoriaIncentivo;
//Facades
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Flash;
use DB;

class ReporteController extends Controller
{
		private $where;
		public function __construct()
    {
				$this->middleware('auth');
        $this->where = [];
    }
    public function integrantes()
    {
    	return view('reportes.integrantes.create')
				->with('area_tematicas', AreaTematica::lists('nombre', 'id')->all())
				->with('categorias', CategoriaIncentivo::lists('nombre', 'id')->all());
    }

		public function proyectos()
		{
			return view('reportes.proyectos.create')
				->with('instituciones', Institucion::lists('nombre', 'id')->take(4))//solo lista las sedes
				->with('area_tematicas', AreaTematica::lists('nombre', 'id')->all())
				->with('area_sigevas', AreaSigeva::lists('nombre', 'id')->all())
				->with('tipo_proyectos', TipoProyecto::lists('abreviacion', 'id')->all());
		}

		public function getReporteIntegrantes(ReporteIntegranteDataTable $reporteDataTable)
		{
			$fields = [
				'Legajo' => ['name' => 'investigadores.legajo', 'data' => 'legajo'],
				'Apellido' => ['name' => 'investigadores.apellido', 'data' => 'apellido'],
				'Nombre' => ['name' => 'investigadores.nombre', 'data' => 'nombre'],
				'Cuil' => ['name' => 'investigadores.cuil', 'data' => 'cuil'],
				'Proyecto' => ['name' => 'proyectos.codigo', 'data' => 'proyecto'],
				'Perfil' => ['name' => 'perfiles.nombre', 'data' => 'perfil'],
				'Hs' => ['name' => 'integrantes.horas', 'data' => 'horas'],
				'% Ejec' => ['name' => 'proyectos.ejecucion', 'data' => 'ejecucion', 'visible' => false],
				'Cat. Inc' => ['name' => 'categoria_incentivos.nombre', 'data' => 'categoria', 'visible' => false],
				'Cat. Inc Fecha' => ['name' => 'historial_incentivos.fecha_inicio', 'data' => 'catfecha', 'visible' => false]
			];
			//Make a Query
			$query = Investigador::join('especialidades', 'investigadores.especialidad_id', '=', 'especialidades.id')
									->join('area_tematicas', 'especialidades.area_tematica_id', '=', 'area_tematicas.id')
									->join('integrantes', 'investigadores.id', '=', 'integrantes.investigador_id')
									->join('proyectos', 'integrantes.proyecto_id', '=', 'proyectos.id')
									->join('perfiles', 'integrantes.perfil_id', '=', 'perfiles.id')
									->join('historial_incentivos', 'investigadores.id', '=', 'historial_incentivos.investigador_id')
									->join('categoria_incentivos', 'historial_incentivos.cat_incentivo_id', '=', 'categoria_incentivos.id')
									->select(['investigadores.*', 'especialidades.nombre AS especialidad', 'area_tematicas.nombre AS area', 'proyectos.codigo AS proyecto', 'perfiles.nombre AS perfil', 'integrantes.horas', 'proyectos.ejecucion', 'categoria_incentivos.nombre AS categoria', 'historial_incentivos.fecha_inicio as catfecha'])
									->groupBy('proyectos.id');
			$input = $reporteDataTable->request();
			//Área Tématica
			if(! empty($input->area_tematica)) {
				$query = $query->where('especialidades.area_tematica_id', $input->area_tematica);
			}
			//Edad
			if(! empty($input->edad_min) && !empty($input->edad_max)) {
				if($input->edad_min == $input->edad_max) {
					$query = $query->whereRaw('FLOOR(DATEDIFF(\''. Carbon::now()->format('Y-m-d') . '\', investigadores.fecha_nac)/365) = '. $input->edad_min);
				} else {
					$query = $query->whereRaw('FLOOR(DATEDIFF(\''. Carbon::now()->format('Y-m-d') . '\', investigadores.fecha_nac)/365) BETWEEN ' . $input->edad_min . ' and ' . $input->edad_max);
				}
			}
			//Sexo
			if (! empty($input->sexo)) {
				$query = $query->where('sexo', $input->sexo);
			}
			//Investigador particular
			if(! empty($input->investigador_id)) {
				$query = $query->where('investigadores.id','=', $input->investigador_id);
			}
			//Categoria Incentivo
			if (! empty($input->cat_incentivo)) {
				//$query = $query->where('historial_incentivos.cat_incentivo_id', '=', $input->cat_incentivo);
				$categoria = $input->cat_incentivo;
				$query = $query->whereIn('historial_incentivos.fecha_inicio', function ($subQuery) use ($categoria) {
					$subQuery->selectRaw('MAX(fecha_inicio)')->from('historial_incentivos')->where('historial_incentivos.cat_incentivo_id', '=', $categoria);
				});
			}
			//Make a DataTables
			$reporteDataTable->setConsulta($query);
			$reporteDataTable->setColumnas($fields);
			$reporteDataTable->setNombreArchivo('investigadores');
			return $reporteDataTable->render('reportes.integrantes.show');
		}

		public function getReporteProyectos(ReporteProyectoDataTable $reporteDataTable)
		{
			$input = $reporteDataTable->request();
			$query = $this->getQueryProyecto();
			//Institución
			if(! empty($input->institucion)) {
				$query = $query->where('institucion_id', $input->institucion);
			}
			//Área Tématica
			if(! empty($input->area_tematica)) {
				$query = $query->where('area_tematica_id', $input->area_tematica);
			}
			//Área SIGEVA
			if(! empty($input->area_sigeva)) {
				$query = $query->where('area_sigeva_id', $input->area_sigeva);
			}
			//Convocatoria
			if(! empty($input->convocatoria)) {
				$query = $query->where('convocatoria', $input->convocatoria);
			}
			//Tipo de Investigación
			if(! empty($input->tipo_investigacion)) {
				$query = $query->where('tipo_investigacion', $input->tipo_investigacion);
			}
			//Duración
			if(! empty($input->duracion)) {
				$query = $query->where('duracion', $input->duracion);
			}
			//Tipo de Proyecto
			if(! empty($input->tipo_proyecto)) {
				$query = $query->where('tipo_id', $input->tipo_proyecto);
			}
			//Director
			if(! empty($input->investigador_id)) {
				$query = $query->whereRaw('(integrantes.perfil_id = 1 or integrantes.perfil_id = 2) and integrantes.investigador_id = '.$input->investigador_id);
			}
			//Proyectos Activos
			if(! empty($input->activo_desde)) {
				if(! empty($input->activo_hasta) && $input->activo_hasta < $input->activo_desde) {
					Flash::error('La fecha Desde no puede ser mayor a la fecha Hasta');
					return back()->withInput();
				} else {
					$fecha_desde = (new Carbon($input->activo_desde))->format('Y-m-d');
					$fecha_hasta = ! empty($input->activo_hasta) == true ? (new Carbon($input->activo_hasta))->format('Y-m-d') : Carbon::now()->format('Y-m-d');
					$query = $query->whereRaw('fecha_inicio between \''. $fecha_desde .'\' and \''. $fecha_hasta .'\' or fecha_fin between \''. $fecha_desde .'\' and \''. $fecha_hasta .'\' or fecha_prorroga between \''. $fecha_desde .'\' and \''. $fecha_hasta . '\'');
				}
			}
			//Make a DataTables
			$reporteDataTable->setConsulta($query);
			return $reporteDataTable->render('reportes.proyectos.show');
		}

		public function getReporteProyectosActivos(ReporteProyectoDataTable $reporteDataTable)
		{
			$query = $this->getQueryProyecto();
			$hoy = Carbon::now()->format('Y-m-d');
			$query = $query->whereRaw('(proyectos.fecha_fin >= \''. $hoy .'\' or proyectos.fecha_prorroga >= \''. $hoy .'\') and proyectos.fecha_baja is null');
			//$query = $query->where('integrantes.perfil_id', '=', 1);
			$reporteDataTable->setConsulta($query);
			return $reporteDataTable->render('reportes.proyectos.show');
		}

		private function getQueryProyecto()
		{
			return Proyecto::join('tipo_proyectos', 'proyectos.tipo_id', '=', 'tipo_proyectos.id')
				->leftjoin('integrantes', 'proyectos.id', '=', 'integrantes.proyecto_id')
				->leftjoin('investigadores', 'integrantes.investigador_id', '=', 'investigadores.id')
				->join('area_tematicas', 'proyectos.area_tematica_id', '=', 'area_tematicas.id')
				->join('instituciones', 'proyectos.institucion_id', '=', 'instituciones.id')
				->whereRaw('integrantes.perfil_id BETWEEN 1 and 2')
				->select(['proyectos.*',
									'integrantes.investigador_id',
									DB::raw('CONCAT(investigadores.apellido, \' \',investigadores.nombre) AS director'),
									'tipo_proyectos.abreviacion AS tipo_proyecto',
									'area_tematicas.nombre AS area_tematica',
									'instituciones.nombre AS institucion'])
				->orderBy('proyectos.id','DESC')
				->groupBy('proyectos.id');
		}
}
