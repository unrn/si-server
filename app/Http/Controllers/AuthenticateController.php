<?php

namespace App\Http\Controllers;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class AuthenticateController extends Controller
{
		protected $redirectTo = '/';

		/**
		* Create a new authentication controller instance.
		*
		* @return void
		*/
		public function __construct()
		{
			$this->middleware('api', ['except' => ['authenticate']]);
		}

		public function authenticate(Request $request)
    {
				$credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
              //return response()->json(['error' => 'invalid_credentials'], 401);
              $msg = 'Usuario o contraseña incorrectos!';
							return view('auth/login')->with(compact('msg'));
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'something went wrong!'], 500);
        }
        // all good so return the token

				//$user = JWTAuth::toUser($token)->email;
				//return response()->json(compact('user'));
				//return response()->json(compact('token'));
				//return redirect('admin/home?token=' . $token);
				return Redirect::action('HomeController@index', $token);
		}

		public function getAuthenticatedUser()
		{
    	try {
        if (! $user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
        }
    	} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
					return response()->json(['token_expired'], $e->getStatusCode());
    	} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      		return response()->json(['token_invalid'], $e->getStatusCode());
    	} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
      		return response()->json(['token_absent'], $e->getStatusCode());
			}
    	// the token is valid and we have found the user via the sub claim
    	return response()->json(compact('user'));
		}

		/**
		* Create a new user instance after a valid registration.
		*
		* @param  array  $data
		* @return User
		*/
		protected function create(array $data)
		{
			return User::create([
					'name' => $data['name'],
					'password' => bcrypt($data['password']),
			]);
		}
}
