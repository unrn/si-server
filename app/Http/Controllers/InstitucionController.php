<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateInstitucionRequest;
use App\Http\Requests\UpdateInstitucionRequest;
use App\Repositories\InstitucionRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class InstitucionController extends AppBaseController
{
    /** @var  InstitucionRepository */
    private $institucionRepository;

    public function __construct(InstitucionRepository $institucionRepo)
    {
				$this->middleware('auth');
        $this->institucionRepository = $institucionRepo;
    }

    /**
     * Display a listing of the Institucion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->institucionRepository->pushCriteria(new RequestCriteria($request));
        //$instituciones = $this->institucionRepository->all();
				$instituciones = $this->institucionRepository->paginate(10);
        return view('instituciones.index')
            ->with('instituciones', $instituciones);
    }

    /**
     * Show the form for creating a new Institucion.
     *
     * @return Response
     */
    public function create()
    {
        return view('instituciones.create');
    }

    /**
     * Store a newly created Institucion in storage.
     *
     * @param CreateInstitucionRequest $request
     *
     * @return Response
     */
    public function store(CreateInstitucionRequest $request)
    {
        $input = $request->all();

        $institucion = $this->institucionRepository->create($input);

        Flash::success('Institución creada exitosamente.');

        return redirect(route('instituciones.index'));
    }

    /**
     * Display the specified Institucion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $institucion = $this->institucionRepository->findWithoutFail($id);

        if (empty($institucion)) {
            Flash::error('Institución no encontrada');

            return redirect(route('instituciones.index'));
        }

        return view('instituciones.show')->with('institucion', $institucion);
    }

    /**
     * Show the form for editing the specified Institucion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $institucion = $this->institucionRepository->findWithoutFail($id);

        if (empty($institucion)) {
            Flash::error('Institución no encontrada');

            return redirect(route('instituciones.index'));
        }

        return view('instituciones.edit')->with('institucion', $institucion);
    }

    /**
     * Update the specified Institucion in storage.
     *
     * @param  int              $id
     * @param UpdateInstitucionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInstitucionRequest $request)
    {
        $institucion = $this->institucionRepository->findWithoutFail($id);

        if (empty($institucion)) {
            Flash::error('Institución no encontrada');

            return redirect(route('instituciones.index'));
        }

        $institucion = $this->institucionRepository->update($request->all(), $id);

        Flash::success('Institución modificada exitosamente.');

        return redirect(route('instituciones.index'));
    }

    /**
     * Remove the specified Institucion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $institucion = $this->institucionRepository->findWithoutFail($id);
        if (empty($institucion)) {
            Flash::error('Institución no encontrada');

            return redirect(route('instituciones.index'));
        }
        $this->institucionRepository->delete($id);
        Flash::success('Institución eliminada exitosamente.');
        return redirect(route('instituciones.index'));
    }
}
