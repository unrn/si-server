<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\HistorialIncentivo;
//Facades
use Illuminate\Support\Facades\DB;
use Auth;
use Flash;
use Response;
use Illuminate\Support\Facades\Redirect;

class HistorialIncentivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request, $investigador_id = null)
    {
			if ($request->fecha_inicio_incentivo != null) {
				$historia = HistorialIncentivo::create([
					'fecha_inicio' => $request->fecha_inicio_incentivo,
					'fecha_fin' => $request->fecha_fin_incentivo == null ? null : $request->fecha_fin_incentivo,
					'investigador_id' => $investigador_id == null ? $request->investigador_id : $investigador_id,
					'cat_incentivo_id' => $request->cat_incentivo_id,
					'user_id' => Auth::user()->id, //ver si guardar el id o solo el email
					'update' => date('Y-m-d')
				]);
				if ($investigador_id == null) {
					Flash::success('Historial Incentivo creado exitosamente');
					return redirect(route('investigadores.historial', ['id' => $request->investigador_id]));
				}
				return $historia;
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($investigador_id)
    {
			//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function update(Request $request, $investigador_id)
    {
			if ($request->historial_incentivo_id == null) {
				return HistorialIncentivoController::store($request, $investigador_id);
			}
			$historial = HistorialIncentivo::findOrfail($request->historial_incentivo_id);
      if($historial->getOriginal('cat_incentivo_id') != $request->cat_incentivo_id){
				return HistorialIncentivoController::store($request, $investigador_id);
			}
			return $historial->update([
				'fecha_inicio' => $request->fecha_inicio_incentivo,
				'fecha_fin' => $request->fecha_fin_incentivo,
				'user_id' => Auth::user()->id, //ver si guardar el id o solo el email
				'update' => date('Y-m-d')
			]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			$historial = HistorialIncentivo::find($id);
			if (empty($historial)) {
					Flash::error('Historial Incentivo no encontrado');
					return redirect(route('investigadores.index'));
			}
			$investigador_id = $historial->investigador_id;
			$historial->delete();
			Flash::success('Historial Incentivo eliminado exitosamente.');
			return redirect(route('investigadores.historial', ['id' => $investigador_id]));
    }
}
