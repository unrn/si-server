<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePerfilRequest;
use App\Http\Requests\UpdatePerfilRequest;
use App\Repositories\PerfilRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PerfilController extends InfyOmBaseController
{
    /** @var  PerfilRepository */
    private $perfilRepository;

    public function __construct(PerfilRepository $perfilRepo)
    {
        $this->perfilRepository = $perfilRepo;
    }

    /**
     * Display a listing of the Perfil.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->perfilRepository->pushCriteria(new RequestCriteria($request));
        //$perfiles = $this->perfilRepository->all();
				$perfiles = $this->perfilRepository->paginate(10);
        return view('perfiles.index')
            ->with('perfiles', $perfiles);
    }

    /**
     * Show the form for creating a new Perfil.
     *
     * @return Response
     */
    public function create()
    {
        return view('perfiles.create');
    }

    /**
     * Store a newly created Perfil in storage.
     *
     * @param CreatePerfilRequest $request
     *
     * @return Response
     */
    public function store(CreatePerfilRequest $request)
    {
        $input = $request->all();
        $perfil = $this->perfilRepository->create($input);
        Flash::success('Perfil creado exitosamente');
        return redirect(route('perfiles.index'));
    }

    /**
     * Display the specified Perfil.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $perfil = $this->perfilRepository->findWithoutFail($id);
        if (empty($perfil)) {
            Flash::error('Perfil no encontrado');
            return redirect(route('perfiles.index'));
        }
        return view('perfiles.show')->with('perfil', $perfil);
    }

    /**
     * Show the form for editing the specified Perfil.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $perfil = $this->perfilRepository->findWithoutFail($id);
        if (empty($perfil)) {
            Flash::error('Perfil no encontrado');
            return redirect(route('perfiles.index'));
        }
        return view('perfiles.edit')->with('perfil', $perfil);
    }

    /**
     * Update the specified Perfil in storage.
     *
     * @param  int              $id
     * @param UpdatePerfilRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePerfilRequest $request)
    {
        $perfil = $this->perfilRepository->findWithoutFail($id);
        if (empty($perfil)) {
            Flash::error('Perfil no encontrado');
            return redirect(route('perfiles.index'));
        }
        $perfil = $this->perfilRepository->update($request->all(), $id);
        Flash::success('Perfil modificado exitosamente.');
        return redirect(route('perfiles.index'));
    }

    /**
     * Remove the specified Perfil from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $perfil = $this->perfilRepository->findWithoutFail($id);
        if (empty($perfil)) {
            Flash::error('Perfil no encontrado');
            return redirect(route('perfiles.index'));
        }
        $this->perfilRepository->delete($id);
        Flash::success('Perfil eliminado exitosamente.');
        return redirect(route('perfiles.index'));
    }
}
