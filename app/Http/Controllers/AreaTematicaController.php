<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAreaTematicaRequest;
use App\Http\Requests\UpdateAreaTematicaRequest;
use App\Repositories\AreaTematicaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AreaTematicaController extends InfyOmBaseController
{
    /** @var  AreaTematicaRepository */
    private $areaTematicaRepository;

    public function __construct(AreaTematicaRepository $areaTematicaRepo)
    {
        $this->areaTematicaRepository = $areaTematicaRepo;
    }

    /**
     * Display a listing of the AreaTematica.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->areaTematicaRepository->pushCriteria(new RequestCriteria($request));
        //$areaTematicas = $this->areaTematicaRepository->all();
				$areaTematicas = $this->areaTematicaRepository->paginate(10);
        return view('areaTematicas.index')
            ->with('areaTematicas', $areaTematicas);
    }

    /**
     * Show the form for creating a new AreaTematica.
     *
     * @return Response
     */
    public function create()
    {
        return view('areaTematicas.create');
    }

    /**
     * Store a newly created AreaTematica in storage.
     *
     * @param CreateAreaTematicaRequest $request
     *
     * @return Response
     */
    public function store(CreateAreaTematicaRequest $request)
    {
        $input = $request->all();

        $areaTematica = $this->areaTematicaRepository->create($input);

        Flash::success('Área Temática guardada exitosamente.');

        return redirect(route('areaTematicas.index'));
    }

    /**
     * Display the specified AreaTematica.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $areaTematica = $this->areaTematicaRepository->findWithoutFail($id);

        if (empty($areaTematica)) {
            Flash::error('Área Temática no encontrada');

            return redirect(route('areaTematicas.index'));
        }

        return view('areaTematicas.show')->with('areaTematica', $areaTematica);
    }

    /**
     * Show the form for editing the specified AreaTematica.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $areaTematica = $this->areaTematicaRepository->findWithoutFail($id);

        if (empty($areaTematica)) {
            Flash::error('Área Temática no encontrada');

            return redirect(route('areaTematicas.index'));
        }

        return view('areaTematicas.edit')->with('areaTematica', $areaTematica);
    }

    /**
     * Update the specified AreaTematica in storage.
     *
     * @param  int              $id
     * @param UpdateAreaTematicaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAreaTematicaRequest $request)
    {
        $areaTematica = $this->areaTematicaRepository->findWithoutFail($id);

        if (empty($areaTematica)) {
            Flash::error('Área Temática no encontrada');

            return redirect(route('areaTematicas.index'));
        }

        $areaTematica = $this->areaTematicaRepository->update($request->all(), $id);

        Flash::success('Área Temática modificada exitosamente.');

        return redirect(route('areaTematicas.index'));
    }

    /**
     * Remove the specified AreaTematica from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $areaTematica = $this->areaTematicaRepository->findWithoutFail($id);

        if (empty($areaTematica)) {
            Flash::error('Área Temática no encontrada');

            return redirect(route('areaTematicas.index'));
        }

        $this->areaTematicaRepository->delete($id);

        Flash::success('Área Temática eliminada exitosamente.');

        return redirect(route('areaTematicas.index'));
    }
}
