<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAreaSigevaRequest;
use App\Http\Requests\UpdateAreaSigevaRequest;
use App\Repositories\AreaSigevaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AreaSigevaController extends InfyOmBaseController
{
    /** @var  AreaSigevaRepository */
    private $areaSigevaRepository;

    public function __construct(AreaSigevaRepository $areaSigevaRepo)
    {
        $this->areaSigevaRepository = $areaSigevaRepo;
    }

    /**
     * Display a listing of the AreaSigeva.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->areaSigevaRepository->pushCriteria(new RequestCriteria($request));
        //$areaSigevas = $this->areaSigevaRepository->all();
				$areaSigevas = $this->areaSigevaRepository->paginate(10);
        return view('areaSigevas.index')
            ->with('areaSigevas', $areaSigevas);
    }

    /**
     * Show the form for creating a new AreaSigeva.
     *
     * @return Response
     */
    public function create()
    {
        return view('areaSigevas.create');
    }

    /**
     * Store a newly created AreaSigeva in storage.
     *
     * @param CreateAreaSigevaRequest $request
     *
     * @return Response
     */
    public function store(CreateAreaSigevaRequest $request)
    {
        $input = $request->all();

        $areaSigeva = $this->areaSigevaRepository->create($input);

        Flash::success('Área Sigeva creada exitosamente.');

        return redirect(route('areaSigevas.index'));
    }

    /**
     * Display the specified AreaSigeva.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $areaSigeva = $this->areaSigevaRepository->findWithoutFail($id);

        if (empty($areaSigeva)) {
            Flash::error('Área Sigeva no encontrada');

            return redirect(route('areaSigevas.index'));
        }

        return view('areaSigevas.show')->with('areaSigeva', $areaSigeva);
    }

    /**
     * Show the form for editing the specified AreaSigeva.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $areaSigeva = $this->areaSigevaRepository->findWithoutFail($id);

        if (empty($areaSigeva)) {
            Flash::error('Area Sigeva no encontrada');
            return redirect(route('areaSigevas.index'));
        }

        return view('areaSigevas.edit')->with('areaSigeva', $areaSigeva);
    }

    /**
     * Update the specified AreaSigeva in storage.
     *
     * @param  int              $id
     * @param UpdateAreaSigevaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAreaSigevaRequest $request)
    {
        $areaSigeva = $this->areaSigevaRepository->findWithoutFail($id);

        if (empty($areaSigeva)) {
            Flash::error('Área Sigeva no encontrada');
            return redirect(route('areaSigevas.index'));
        }

        $areaSigeva = $this->areaSigevaRepository->update($request->all(), $id);

        Flash::success('Área Sigeva modificado exitosamente.');

        return redirect(route('areaSigevas.index'));
    }

    /**
     * Remove the specified AreaSigeva from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $areaSigeva = $this->areaSigevaRepository->findWithoutFail($id);

        if (empty($areaSigeva)) {
            Flash::error('AreaSigeva no encontrada');

            return redirect(route('areaSigevas.index'));
        }

        $this->areaSigevaRepository->delete($id);

        Flash::success('AreaSigeva elimnada exitosamente.');

        return redirect(route('areaSigevas.index'));
    }
}
