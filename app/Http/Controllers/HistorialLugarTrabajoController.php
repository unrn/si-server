<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\HistorialLugarTrabajo;
//Facades
use Illuminate\Support\Facades\DB;
use Flash;
use Response;
use Auth;
use Illuminate\Support\Facades\Redirect;

class HistorialLugarTrabajoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request, $investigador_id = null)
    {
			if ($request->fecha_inicio_lugartrabajo != null) {
				$historia = HistorialLugarTrabajo::create([
					'fecha_inicio' => $request->fecha_inicio_lugartrabajo,
					'fecha_fin' => $request->fecha_fin_lugartrabajo == null ? null : $request->fecha_fin_lugartrabajo,
					'investigador_id' => $investigador_id == null ? $request->investigador_id : $investigador_id,
					'lugar_trabajo_id' => $request->lugar_trabajo_id,
					'rol_id' => $request->rol_id,
					'user' => Auth::user()->email, //ver si guardar el id o solo el email
					'update' => date('Y-m-d')
				]);
				if ($investigador_id == null) {
					Flash::success('Historial Lugar de Trabajo creado exitosamente');
					return redirect(route('investigadores.historial', ['id' => $request->investigador_id]));
				}
				return $historia;
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($investigador_id)
    {
			//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function update(Request $request, $investigador_id)
    {
			if ($request->historial_lugartrabajo_id == null) {
				return HistorialLugarTrabajoController::store($request, $investigador_id);
			}
			$historial = HistorialLugarTrabajo::findOrfail($request->historial_lugartrabajo_id);
      if($historial->getOriginal('lugar_trabajo_id') != $request->lugar_trabajo_id){
				return HistorialLugarTrabajoController::store($request, $investigador_id);
			}
			return $historial->update([
				'fecha_inicio' => $request->fecha_inicio_lugartrabajo,
				'fecha_fin' => $request->fecha_fin_lugartrabajo,
				'user' => Auth::user()->email, //ver si guardar el id o solo el email
				'update' => date('Y-m-d')
			]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			$historial = HistorialLugarTrabajo::find($id);
			if (empty($historial)) {
					Flash::error('Historial Lugar de Trabajo no encontrado');
					return redirect(route('investigadores.index'));
			}
			$investigador_id = $historial->investigador_id;
			$historial->delete();
			Flash::success('Historial Lugar de Trabajo eliminado exitosamente.');
			return redirect(route('investigadores.historial', ['id' => $investigador_id]));
    }
}
