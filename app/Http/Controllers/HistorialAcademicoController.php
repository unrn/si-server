<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\HistorialAcademicoController;
//Models
use App\Models\HistorialAcademico;
//Facades
use Illuminate\Support\Facades\DB;
use Flash;
use Auth;
use Response;
use Illuminate\Support\Facades\Redirect;

class HistorialAcademicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store(Request $request, $investigador_id = null)
    {
			if ($request->fecha_inicio_academico != null) {
				$historia = HistorialAcademico::create([
					'fecha_inicio' => $request->fecha_inicio_academico,
					'fecha_fin' => $request->fecha_fin_academico == null ? null : $request->fecha_fin_academico,
					'investigador_id' => $investigador_id == null ? $request->investigador_id : $investigador_id,
					'grado_academico_id' => $request->grado_academico_id,
					'user' => Auth::user()->email, //ver si guardar el id o solo el email
					'update' => date('Y-m-d')
				]);
				if ($investigador_id == null) {
					Flash::success('Historial Academico creado exitosamente.');
					return redirect(route('investigadores.historial', ['id' => $request->investigador_id]));
				}
				return $historia;
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function show($investigador_id)
    {
				//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function update(Request $request, $investigador_id)
    {
			if($request->historial_academico_id == null) {
				return HistorialAcademicoController::store($request, $investigador_id);
			}
			$historial = HistorialAcademico::findOrfail($request->historial_academico_id);
      if($historial->getOriginal('grado_academico_id') != $request->grado_academico_id){
				return HistorialAcademicoController::store($request, $investigador_id);
			}
			return $historial->update([
				'fecha_inicio' => $request->fecha_inicio_academico,
				'fecha_fin' => $request->fecha_fin_academico,
				'user' => Auth::user()->email, //ver si guardar el id o solo el email
				'update' => date('Y-m-d')
			]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $historial = HistorialAcademico::find($id);
			if (empty($historial)) {
					Flash::error('Historial Academico no encontrado');
					return redirect(route('investigadores.index'));
			}
			$investigador_id = $historial->investigador_id;
			$historial->delete();
			Flash::success('Historial Academico eliminado exitosamente');
			return redirect(route('investigadores.historial', ['id' => $investigador_id]));
    }
}
