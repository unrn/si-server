<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\DataTables\UserDataTable;
use App\User;
use Auth, Image, Flash, File, Validator, Hash;

class UserController extends Controller
{
		/**
		 * Create a new controller instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
				$this->middleware('auth');
		}

		public function index(UserDataTable $userDataTable) {
			return $userDataTable->render('users.index');
		}

		public function create() {
			return view('auth.register');
		}

		public function store(Request $request) {
			//Nothing to here
			return view('errors.400');
		}

		public function show($id) {
			//Nothing to here
			return view('errors.400');
		}

		public function edit($id) {
			$user = User::find($id);
			if (empty($user)) {
				Flash::error('Usuario no encontrado');
				return redirect(route('usuarios.index'));
			}
			return view('users.edit')->with('user', $user);
		}

		public function profile() {
			return view('users.profile');
		}

		public function update($id, Request $request) {
			$user = User::find($id);
			if (empty($user)) {
				Flash::error('Usuario no encontrado');
				return redirect(route('usuarios.index'));
			}
			$input = $request->all();
			$user = User::where('id', $id)->update([
					'name' => $input['name'],
					'email' => $input['email'],
					'password' => ! isset($input['password']) ? $user->password : bcrypt($input['password']),
					'type' => $input['type']
			]);
			Flash::success('Usuario modificado exitosamente.');
			return redirect(route('usuarios.index'));
		}

		public function updateAvatar(Request $request) {
			//Handle the user upload of avatar
			if($request->hasFile('avatar')){
				$avatar = $request->file('avatar');
				$validator = Validator::make($request->all(), ['avatar' => 'required|image']);
				if ($validator->fails()) {
					return redirect(route('users.profile'))->withErrors($validator->errors());
				}
				$filename = time() . '.' . $avatar->getClientOriginalExtension();
				Image::make($avatar)->resize(300, 300)->save( public_path('/images/avatars/' . $filename ) );
				$user = Auth::user();
				if ($user->avatar !== 'default.jpg') {
					File::delete('images/avatars/' . $user->avatar);
				}
				$user->avatar = $filename;
				$user->save();
			}
			return redirect(route('users.profile'));
		}

		public function destroy($id) {

		}

		public function resetPassword(Request $request) {
			$user = Auth::user();
			if (! password_verify($request->password_actual, $user->password)) {
				return redirect(route('users.profile'))->withErrors(['password_actual' => 'La contraseña actual es incorrecta']);
			}
			$validator = Validator::make($request->all(), ['password' => 'required|min:6|confirmed']);
			if ($validator->fails()) {
				return redirect(route('users.profile'))->withErrors($validator->errors());
			}
			$user->password = bcrypt($request->password);
			$user->save();
			Flash::success('Contraseña modificada exitosamente.');
			Auth::logout();
			return view('auth.login');
		}
}
