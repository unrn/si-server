<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTipoProyectoRequest;
use App\Http\Requests\UpdateTipoProyectoRequest;
use App\Repositories\TipoProyectoRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TipoProyectoController extends InfyOmBaseController
{
    /** @var  TipoProyectoRepository */
    private $tipoProyectoRepository;

    public function __construct(TipoProyectoRepository $tipoProyectoRepo)
    {
        $this->tipoProyectoRepository = $tipoProyectoRepo;
    }

    /**
     * Display a listing of the TipoProyecto.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tipoProyectoRepository->pushCriteria(new RequestCriteria($request));
        //$tipoProyectos = $this->tipoProyectoRepository->all();
				$tipoProyectos = $this->tipoProyectoRepository->paginate(15);
        return view('tipoProyectos.index')
            ->with('tipoProyectos', $tipoProyectos);
    }

    /**
     * Show the form for creating a new TipoProyecto.
     *
     * @return Response
     */
    public function create()
    {
        return view('tipoProyectos.create');
    }

    /**
     * Store a newly created TipoProyecto in storage.
     *
     * @param CreateTipoProyectoRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoProyectoRequest $request)
    {
        $input = $request->all();
        $tipoProyecto = $this->tipoProyectoRepository->create($input);
        Flash::success('Tipo de Proyecto creado exitosamente');
        return redirect(route('tipoProyectos.index'));
    }

    /**
     * Display the specified TipoProyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoProyecto = $this->tipoProyectoRepository->findWithoutFail($id);
        if (empty($tipoProyecto)) {
            Flash::error('Tipo de Proyecto no encontrado');
            return redirect(route('tipoProyectos.index'));
        }
        return view('tipoProyectos.show')->with('tipoProyecto', $tipoProyecto);
    }

    /**
     * Show the form for editing the specified TipoProyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoProyecto = $this->tipoProyectoRepository->findWithoutFail($id);
        if (empty($tipoProyecto)) {
            Flash::error('Tipo de Proyecto no encontrado');
            return redirect(route('tipoProyectos.index'));
        }
        return view('tipoProyectos.edit')->with('tipoProyecto', $tipoProyecto);
    }

    /**
     * Update the specified TipoProyecto in storage.
     *
     * @param  int              $id
     * @param UpdateTipoProyectoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoProyectoRequest $request)
    {
        $tipoProyecto = $this->tipoProyectoRepository->findWithoutFail($id);
        if (empty($tipoProyecto)) {
            Flash::error('Tipo de Proyecto no encontrado');
            return redirect(route('tipoProyectos.index'));
        }
        $tipoProyecto = $this->tipoProyectoRepository->update($request->all(), $id);
        Flash::success('Tipo de Proyecto modificado exitosamente.');
        return redirect(route('tipoProyectos.index'));
    }

    /**
     * Remove the specified TipoProyecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoProyecto = $this->tipoProyectoRepository->findWithoutFail($id);
        if (empty($tipoProyecto)) {
            Flash::error('Tipo de Proyecto no encontrado');
            return redirect(route('tipoProyectos.index'));
        }
        $this->tipoProyectoRepository->delete($id);
        Flash::success('Tipo de Proyecto eliminado exitosamente.');
        return redirect(route('tipoProyectos.index'));
    }
}
