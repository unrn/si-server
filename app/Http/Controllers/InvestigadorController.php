<?php

namespace App\Http\Controllers;

use App\DataTables\InvestigadorDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateInvestigadorRequest;
use App\Http\Requests\UpdateInvestigadorRequest;
use App\Http\Controllers\HistorialAcademicoController;
use App\Http\Controllers\HistorialIncentivoController;
use App\Http\Controllers\HistorialLugarTrabajoController;
use App\Repositories\InvestigadorRepository;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
//Models
use App\Models\Investigador;
use App\Models\LugarTrabajo;
use App\Models\Rol;
use App\Models\GradoAcademico;
use App\Models\CategoriaIncentivo;
use App\Models\Institucion;
use App\Models\HistorialAcademico;
use App\Models\HistorialIncentivo;
use App\Models\HistorialLugarTrabajo;
use App\Models\Especialidad;
//Facades
use Flash;
use Response;
use Illuminate\Support\Facades\DB;

class InvestigadorController extends AppBaseController
{
    /** @var  InvestigadorRepository */
    private $investigadorRepository;

    public function __construct(InvestigadorRepository $investigadorRepo)
    {
				$this->middleware('auth');
        $this->investigadorRepository = $investigadorRepo;
    }

    /**
     * Display a listing of the Investigador.
     *
     * @param Request $request
     * @return Response
     */
    public function index(InvestigadorDataTable $investigadorDataTable)
    {
				return $investigadorDataTable->render('investigadores.index');
    }

    /**
     * Show the form for creating a new Investigador.
     *
     * @return Response
     */
    public function create()
    {
				foreach (LugarTrabajo::all() as $lugartrabajo) {
					$lugartrabajos[$lugartrabajo->id] = $lugartrabajo->abreviacion . ' - ' .$lugartrabajo->nombre;
				}
      	return view('investigadores.create')
						->with('lugartrabajos', $lugartrabajos)
						->with('categorias', CategoriaIncentivo::lists('nombre', 'id')->all())
						->with('grados', GradoAcademico::lists('nombre', 'id')->all())
						->with('roles', Rol::lists('nombre', 'id')->all())
						->with('instituciones', Institucion::lists('nombre', 'id')->all())
						->with('especialidades', Especialidad::lists('nombre', 'id')->all());
    }

    /**
     * Store a newly created Investigador in storage.
     *
     * @param CreateInvestigadorRequest $request
     *
     * @return Response
     */
    public function store(CreateInvestigadorRequest $request)
    {
				$input = $request->all();
				//UpperCase - Mayusculas
				$input['apellido'] = strtoupper($input['apellido']);
				$input['legajo'] = $request->legajo === '' ? null : $request->legajo;
				$investigador = $this->investigadorRepository->create($input);
				if ($request->fecha_inicio_academico !== '' && $request->grados_academico_id !== '') {
					$nuevo_academico = HistorialAcademicoController::store($request, $investigador->id);
				}
				if ($request->fecha_inicio_incentivo !== '' && $request->cat_incentivo_id !== '') {
					$nuevo_incentivo = HistorialIncentivoController::store($request, $investigador->id);
				}
				if ($request->fecha_inicio_lugartrabajo !== '' && $request->lugar_trabajo_id !== '' && $request->rol_id !== '') {
					$nuevo_lugartrabajo = HistorialLugarTrabajoController::store($request, $investigador->id);
				}
        Flash::success('Investigador creado exitosamente.');
        return redirect(route('investigadores.index'));
    }

    /**
     * Display the specified Investigador.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
			$investigador = $this->investigadorRepository->findWithoutFail($id);
			if (empty($investigador)) {
					Flash::error('Investigador no encontrado');
					return redirect(route('investigadores.index'));
			}
			foreach (LugarTrabajo::all() as $lugartrabajo) {
				$lugartrabajos[$lugartrabajo->id] = $lugartrabajo->abreviacion . ' - ' .$lugartrabajo->nombre;
			}
			$data['lugartrabajos'] = $lugartrabajos;
			$data['roles'] = Rol::lists('nombre', 'id')->all();
			$data['categorias'] = CategoriaIncentivo::lists('nombre', 'id')->all();
			$data['grados'] = GradoAcademico::lists('nombre', 'id')->all();
			$data['instituciones'] = Institucion::lists('nombre', 'id')->all();
			$data['academico_actual'] = $investigador->getUltimoHistorialAcademico();
			$data['historial_academicos'] = $investigador->getHistorialAcademicos()->take(3);
			$data['incentivo_actual'] = $investigador->getUltimoHistorialIncentivo();
			$data['historial_incentivos'] = $investigador->getHistorialIncentivos()->take(3);
			$data['lugartrabajo_actual'] = $investigador->getUltimoHistorialLugarTrabajo();
			$data['historial_lugartrabajos'] = $investigador->getHistorialLugarTrabajos()->take(3);
			$data['especialidades'] = Especialidad::lists('nombre', 'id')->all();
			$data['integrantes'] = $investigador->getIntegrantes();

			return view('investigadores.show')->with('investigador', $investigador)->with($data);
    }

    /**
     * Show the form for editing the specified Investigador.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $investigador = $this->investigadorRepository->findWithoutFail($id);
        if (empty($investigador)) {
            Flash::error('Investigador no encontrado');
            return redirect(route('investigadores.index'));
        }
				foreach (LugarTrabajo::all() as $lugartrabajo) {
					$lugartrabajos[$lugartrabajo->id] = $lugartrabajo->abreviacion . ' - ' .$lugartrabajo->nombre;
				}
				//dd($investigador->getHistorialAcademicos()->take(3));
				$data['lugartrabajos'] = $lugartrabajos;
				$data['roles'] = Rol::lists('nombre', 'id')->all();
				$data['categorias'] = CategoriaIncentivo::lists('nombre', 'id')->all();
				$data['grados'] = GradoAcademico::lists('nombre', 'id')->all();
				$data['instituciones'] = Institucion::lists('nombre', 'id')->all();
				$data['academico_actual'] = $investigador->getUltimoHistorialAcademico();
				$data['historial_academicos'] = $investigador->getHistorialAcademicos()->take(3);
				$data['incentivo_actual'] = $investigador->getUltimoHistorialIncentivo();
				$data['historial_incentivos'] = $investigador->getHistorialIncentivos()->take(3);
				$data['lugartrabajo_actual'] = $investigador->getUltimoHistorialLugarTrabajo();
				$data['historial_lugartrabajos'] = $investigador->getHistorialLugarTrabajos()->take(3);
				$data['especialidades'] = Especialidad::lists('nombre', 'id')->all();
				$data['integrantes'] = $investigador->getIntegrantes();
				return view('investigadores.edit')->with('investigador', $investigador)->with($data);
    }

    /**
     * Update the specified Investigador in storage.
     *
     * @param  int $id
     * @param UpdateInvestigadorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvestigadorRequest $request)
    {
        $investigador = $this->investigadorRepository->findWithoutFail($id);
        if (empty($investigador)) {
            Flash::error('Investigador no encontrado');
            return redirect(route('investigadores.index'));
        }
				$input = $request->all();
				$input['legajo'] = $request->legajo === '' ? null : $request->legajo;
				$update_academico = $request->historial_academico_id !== '' ? HistorialAcademicoController::update($request, $investigador->id) : null;
				//$update_incentivo = $request->historial_incentivo_id !== '' ? HistorialIncentivoController::update($request, $investigador->id) : null;
				$update_incentivo = HistorialIncentivoController::update($request, $investigador->id);
				$update_lugartrabajo = $request->historial_lugartrabajo_id !== '' ? HistorialLugarTrabajoController::update($request, $investigador->id) : null;
				$investigador = $this->investigadorRepository->update($input, $id);
        Flash::success('Investigador modificado exitosamente.');
        return redirect(route('investigadores.edit', $id));
    }

    /**
     * Remove the specified Investigador from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $investigador = $this->investigadorRepository->findWithoutFail($id);
        if (empty($investigador)) {
            Flash::error('Investigador no encontrado');
            return redirect(route('investigadores.index'));
        }
        $this->investigadorRepository->delete($id);
        Flash::success('Investigador eliminado exitosamente.');
        return redirect(route('investigadores.index'));
    }

		public function historial($id)
		{
			$investigador = $this->investigadorRepository->findWithoutFail($id);
			if (empty($investigador)) {
					Flash::error('Investigador no encontrado');
					return redirect(route('investigadores.index'));
			}
			$data['historial_academicos'] = $investigador->getHistorialAcademicos();
			$data['grados'] = GradoAcademico::lists('nombre', 'id')->all();
			$data['historial_incentivos'] = $investigador->getHistorialIncentivos();
			$data['categorias'] = CategoriaIncentivo::lists('nombre', 'id')->all();
			$data['historial_lugartrabajos'] = $investigador->getHistorialLugarTrabajos();
			$data['lugartrabajos'] = LugarTrabajo::lists('nombre', 'id')->all();
			$data['roles'] = Rol::lists('nombre', 'id')->all();
			$data['instituciones'] = Institucion::lists('nombre', 'id')->all();

			return view('investigadores.historial.index')->with('investigador', $investigador)->with($data);
		}

		public function autocomplete(Request $request)
		{
			$term = $request->term;
			$results = array();
			$queries = Investigador::whereRaw('CONCAT(apellido, \', \', nombre) LIKE \'%'. $term .'%\'')
				->take(5)->get();
			foreach ($queries as $query) {
	    	$results[] = [ 'id' => $query->id, 'value' => $query->apellido.', '.$query->nombre ];
			}
			return Response::json($results);
		}
}
