<?php

namespace App\Http\Controllers;

use App\DataTables\IntegranteDataTable;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreateIntegranteRequest;
use App\Http\Requests\UpdateIntegranteRequest;
use App\Models\Integrante;
use App\Models\Proyecto;
use App\Models\Perfil;
use App\Models\Investigador;
use Flash;

class IntegranteController extends AppBaseController
{
		public function __construct()
		{
				$this->middleware('auth');
		}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IntegranteDataTable $integranteDataTable)
    {
			return $integranteDataTable->render('integrantes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
			$proyecto = Proyecto::find($request->proyecto_id);
			if(!isset($request->proyecto_id) || $proyecto == null){
				return view('errors.403'); //cambiar
			}
			$perfiles = Perfil::lists('nombre', 'id');
      return view('integrantes.create')
				->with('proyecto', $proyecto)
				->with('perfiles', $perfiles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateIntegranteRequest $request)
    {
			$input = $request->all();
			foreach (Integrante::all() as $int) {
				if ($request->investigador_id == $int->investigador_id && $request->proyecto_id == $int->proyecto_id) {
					if($request->fecha_alta < $int->fecha_baja) {
						Flash::error('El Integrante '. $int->investigador()->fullName() .' ya se encuentra en el proyecto entre las fechas publicadas');
						return back()->withInput();
					}
				}
			}
			$proyecto = Proyecto::find($request->proyecto_id);
			if ($proyecto->existeDirector(null,$request->perfil_id)) {
				Flash::error('Ya existe un DIRECTOR en el proyecto');
				return back()->withInput();
			}
			$integrante = Integrante::create($input);
			Flash::success('Integrante '. $integrante->investigador()->fullName() .' creado exitosamente.');
			return redirect(route('integrantes.create', ['proyecto_id' => $integrante->proyecto_id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
			$integrante = Integrante::find($id);

			if (empty($integrante)) {
					Flash::error('Integrante no encontrado');
					return redirect(route('integrantes.index'));
			}
			$proyecto = $integrante->proyecto();
			$perfiles = Perfil::lists('nombre', 'id')->all();
			$term = $integrante->investigador()->fullName();
			return view('integrantes.edit')
				->with('integrante', $integrante)
				->with('proyecto', $proyecto)
				->with('perfiles', $perfiles)
				->with('term', $term);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateIntegranteRequest $request, $id)
    {
      $integrante = Integrante::find($id);
			if (empty($integrante)) {
					Flash::error('Integrante no encontrado');
					return redirect(route('integrantes.index'));
			}
			if($integrante->investigador_id != $request->investigador_id) {
				foreach (Integrante::all() as $int) {
					if ($request->investigador_id == $int->investigador_id && $request->proyecto_id == $int->proyecto_id) {
						Flash::error('El Integrante '. $int->investigador()->fullName() .' ya se encuentra en el proyecto');
						return back()->withInput();
					}
				}
			}
			$proyecto = $integrante->proyecto();
			if ($proyecto->existeDirector($integrante->perfil_id, $request->perfil_id)) {
				Flash::error('Ya existe un DIRECTOR en el proyecto');
				return back()->withInput();
			}
			$update = $integrante->update($request->all());
			Flash::success('Integrante '. $integrante->investigador()->fullName() .' modificado exitosamente.');
			return redirect(route('proyectos.edit', $proyecto->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			$integrante = Integrante::find($id);
			$proyecto = $integrante->proyecto();
			if (empty($integrante)) {
					Flash::error('Integrante no encontrado');
					return redirect(route('integrantes.index'));
			}
			$integrante->delete();
			Flash::success('Integrante '. $integrante->investigador()->fullName() .' eliminado exitosamente.');
			return redirect(route('proyectos.edit', $proyecto->id));
    }
}
