<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
//Models
use App\Models\Investigador;
use App\Models\Institucion;
use App\Models\Proyecto;
//Facades
use Mail;
use Flash;
use Auth;
use Config;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$porSexo = Investigador::selectRaw('(CASE WHEN sexo = \'F\' THEN \'Femenino\' ELSE \'Masculino\' END) as sexo, count(id) as cantidad')->groupBy('sexo')->get();
			$porInstitucion = Institucion::join('proyectos', 'instituciones.id', '=', 'proyectos.institucion_id')
				->selectRaw('instituciones.abreviacion,
					sum(case when proyectos.tipo_id = 1 then 1 else 0 end) as pi,
					sum(case when proyectos.tipo_id = 2 then 1 else 0 end) as pict,
					sum(case when proyectos.tipo_id = 3 then 1 else 0 end) as picto,
					sum(case when proyectos.tipo_id = 8 then 1 else 0 end) as proevo,
					sum(case when proyectos.tipo_id = 9 then 1 else 0 end) as prosap')->groupBy('instituciones.id')->get();

			return view('home.index')
				->with('totalInvestigadores', Investigador::all()->count())
				->with('totalProyectos', Proyecto::all()->count())
				->with('sexos', $porSexo)
				->with('instituciones', $porInstitucion)
				->with('docentes', Investigador::all()->count());
			}

		private function investigadoresPorSexo($sexo)
		{
			return Investigador::where('sexo', $sexo)->count();
		}

    public function getContact()
    {
      return view('home.contact.index');
    }

    public function postContact(Request $request)
    {
      $data = [
        'email' => Auth::user()->email,
        'subject' => $request->subject,
        'messageBody' => $request->messageBody
      ];
      Mail::send('home.contact.email', $data, function($message) use ($data) {
        $message->from($data['email']);
        $message->to('msanhueza@unrn.edu.ar');
        $message->subject($data['subject']);
      });
      Flash::success('Mensaje enviado correctamente');
      return redirect('/home');
    }
}
