<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateResolucionAltaRequest;
use App\Http\Requests\UpdateResolucionAltaRequest;
use App\Repositories\ResolucionAltaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
//Models
use App\Models\ResolucionAlta;

class ResolucionAltaController extends InfyOmBaseController
{
    /** @var  ResolucionAltaRepository */
    private $resolucionAltaRepository;

    public function __construct(ResolucionAltaRepository $resolucionAltaRepo)
    {
        $this->resolucionAltaRepository = $resolucionAltaRepo;
    }

    /**
     * Display a listing of the ResolucionAlta.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->resolucionAltaRepository->pushCriteria(new RequestCriteria($request));
        //$resolucionAltas = $this->resolucionAltaRepository->all();
				$resolucionAltas = $this->resolucionAltaRepository->paginate(10);
        return view('resolucionAltas.index')
            ->with('resolucionAltas', $resolucionAltas);
    }

    /**
     * Show the form for creating a new ResolucionAlta.
     *
     * @return Response
     */
    public function create()
    {
        return view('resolucionAltas.create');
    }

    /**
     * Store a newly created ResolucionAlta in storage.
     *
     * @param CreateResolucionAltaRequest $request
     *
     * @return Response
     */
    public function store(CreateResolucionAltaRequest $request)
    {
        $input = $request->all();
        $resolucionAlta = $this->resolucionAltaRepository->create($input);
        Flash::success('Resolución de Alta creada exitosamente');
        return redirect(route('resolucionAltas.index'));
    }

    /**
     * Display the specified ResolucionAlta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $resolucionAlta = $this->resolucionAltaRepository->findWithoutFail($id);
        if (empty($resolucionAlta)) {
            Flash::error('Resolución de Alta no encontrada');
            return redirect(route('resolucionAltas.index'));
        }
        return view('resolucionAltas.show')->with('resolucionAlta', $resolucionAlta);
    }

    /**
     * Show the form for editing the specified ResolucionAlta.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $resolucionAlta = $this->resolucionAltaRepository->findWithoutFail($id);
        if (empty($resolucionAlta)) {
            Flash::error('Resolución de Alta no encontrada');
            return redirect(route('resolucionAltas.index'));
        }
        return view('resolucionAltas.edit')->with('resolucionAlta', $resolucionAlta);
    }

    /**
     * Update the specified ResolucionAlta in storage.
     *
     * @param  int              $id
     * @param UpdateResolucionAltaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResolucionAltaRequest $request)
    {
        $resolucionAlta = $this->resolucionAltaRepository->findWithoutFail($id);
        if (empty($resolucionAlta)) {
            Flash::error('Resolución de Alta no encontrada');
            return redirect(route('resolucionAltas.index'));
        }
        $resolucionAlta = $this->resolucionAltaRepository->update($request->all(), $id);
        Flash::success('Resolución de Alta modificada exitosamente.');
        return redirect(route('resolucionAltas.index'));
    }

    /**
     * Remove the specified ResolucionAlta from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $resolucionAlta = $this->resolucionAltaRepository->findWithoutFail($id);
        if (empty($resolucionAlta)) {
            Flash::error('Resolución de Alta no encontrado');
            return redirect(route('resolucionAltas.index'));
        }
        $this->resolucionAltaRepository->delete($id);
        Flash::success('Resolución de Alta eliminada exitosamente.');
        return redirect(route('resolucionAltas.index'));
    }

		public function autocomplete(Request $request)
		{
			$term = $request->term;
			$results = array();
			$resoluciones = ResolucionAlta::where('numero', 'LIKE', '%'.$term.'%')
				->take(5)->get();
			foreach ($resoluciones as $resol) {
				$results[] = [ 'id' => $resol->id, 'value' => $resol->numero, 'resolucion' => $resol ];
				//$results[] = $resol;
			}
			return Response::json($results);
		}
}
