<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateInformeProyectoRequest;
use App\Http\Requests\UpdateInformeProyectoRequest;
use App\Repositories\InformeProyectoRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use App\Models\Proyecto;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class InformeProyectoController extends InfyOmBaseController
{
    /** @var  InformeProyectoRepository */
    private $informeProyectoRepository;

    public function __construct(InformeProyectoRepository $informeProyectoRepo)
    {
        $this->informeProyectoRepository = $informeProyectoRepo;
    }

    /**
     * Display a listing of the InformeProyecto.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->informeProyectoRepository->pushCriteria(new RequestCriteria($request))->orderBy('proyecto_id', 'tipo');
        //$informeProyectos = $this->informeProyectoRepository->all();
        $informeProyectos = $this->informeProyectoRepository->paginate(10);
        return view('informeProyectos.index')
            ->with('informeProyectos', $informeProyectos);
    }

    /**
     * Show the form for creating a new InformeProyecto.
     *
     * @return Response
     */
    public function create(Request $request)
    {
			$proyecto = Proyecto::find($request->proyecto_id);
			if(!isset($request->proyecto_id) || $proyecto == null){
				return view('errors.403'); //cambiar
			}
			return view('informeProyectos.create')
				->with('proyecto', $proyecto);
    }

    /**
     * Store a newly created InformeProyecto in storage.
     *
     * @param CreateInformeProyectoRequest $request
     *
     * @return Response
     */
    public function store(CreateInformeProyectoRequest $request)
    {
        $input = $this->fechasNull($request);
        $informeProyecto = $this->informeProyectoRepository->create($input);
        Flash::success('Informe de Proyecto creado exitosamente.');
        return redirect(route('proyectos.edit', $informeProyecto->proyecto_id));
    }

    /**
     * Display the specified InformeProyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $informeProyecto = $this->informeProyectoRepository->findWithoutFail($id);
        if (empty($informeProyecto)) {
            Flash::error('Informe de Proyecto no encontrado');
            return redirect(route('informeProyectos.index'));
        }
        return view('informeProyectos.show')->with('informeProyecto', $informeProyecto);
    }

    /**
     * Show the form for editing the specified InformeProyecto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $informeProyecto = $this->informeProyectoRepository->findWithoutFail($id);
        if (empty($informeProyecto)) {
            Flash::error('InformeProyecto no encontrado');
            return redirect(route('informeProyectos.index'));
        }
        return view('informeProyectos.edit')
					->with('informeProyecto', $informeProyecto)
					->with('proyecto', $informeProyecto->proyecto());
    }

    /**
     * Update the specified InformeProyecto in storage.
     *
     * @param  int              $id
     * @param UpdateInformeProyectoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInformeProyectoRequest $request)
    {
        $informeProyecto = $this->informeProyectoRepository->findWithoutFail($id);
        if (empty($informeProyecto)) {
            Flash::error('Informe de Proyecto no encontrado');
            return redirect(route('informeProyectos.index'));
        }
				$input = $this->fechasNull($request);
        $informeProyecto = $this->informeProyectoRepository->update($input, $id);
				Flash::success('Informe de Proyecto modificado exitosamente.');
				return redirect(route('proyectos.edit', $informeProyecto->proyecto_id));
    }

    /**
     * Remove the specified InformeProyecto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $informeProyecto = $this->informeProyectoRepository->findWithoutFail($id);
        if (empty($informeProyecto)) {
            Flash::error('InformeProyecto no encontrado');
            return redirect(route('informeProyectos.index'));
        }
				$proyecto = $informeProyecto->proyecto();
        $this->informeProyectoRepository->delete($id);
        Flash::success('Informe de Proyecto eliminado exitosamente.');
        return redirect(route('proyectos.edit', $proyecto->id));
    }

		public function updateFrom($id, UpdateInformeProyectoRequest $request)
		{
			$informes = $request->informe_id;
			for ($i=0; $i < sizeof($informes); $i++) {
				$informeProyecto = $this->informeProyectoRepository->findWithoutFail($informes[$i]);
				if (empty($informeProyecto)) {
						Flash::error('Informe de Proyecto no encontrado');
						return redirect(route('informeProyectos.index'));
				}
				$fields = [
					'proyecto_id' => $request->proyecto_id,
					'tipo' => $request->tipo[$i],
					'fecha_presentacion' => $request->fecha_presentacion[$i],
					'fecha_entrega_papel' => $request->fecha_entrega_papel[$i] !== '' ? $request->fecha_entrega_papel[$i] : NULL,
					'fecha_entrega_digital' => $request->fecha_entrega_digital[$i] !== '' ? $request->fecha_entrega_digital[$i] : NULL,
					'descripcion' => $request->descripcion[$i]
				];
				$informeProyecto = $this->informeProyectoRepository->update($fields, $informes[$i]);
			}
			Flash::success('Informes de Proyecto modificado exitosamente.');
			return redirect(route('proyectos.edit', $id));
		}

		protected function fechasNull($request)
		{
			$input = $request->all();
			//Si estan vacios los pongo en NULL xq sino la BD pone 0000-00-00
			$input['fecha_entrega_papel'] = $request->fecha_entrega_papel == null ? null : $request->fecha_entrega_papel;
			$input['fecha_entrega_digital'] = $request->fecha_entrega_digital == null ? null : $request->fecha_entrega_digital;
			return $input;
		}
}
