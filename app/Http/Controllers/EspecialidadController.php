<?php

namespace App\Http\Controllers;

use App\DataTables\EspecialidadDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEspecialidadRequest;
use App\Http\Requests\UpdateEspecialidadRequest;
use App\Repositories\EspecialidadRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\AreaTematica;
use App\Models\Especialidad;
use Illuminate\Support\Facades\DB;

class EspecialidadController extends InfyOmBaseController
{
    /** @var  EspecialidadRepository */
    private $especialidadRepository;

    public function __construct(EspecialidadRepository $especialidadRepo)
    {
        $this->especialidadRepository = $especialidadRepo;
    }

    /**
     * Display a listing of the Especialidad.
     *
     * @param Request $request
     * @return Response
     */
    // public function index(Request $request)
    // {
    //     $this->especialidadRepository->pushCriteria(new RequestCriteria($request));
    //     $especialidades = $this->especialidadRepository->all();
		// 		$tematicas = AreaTematica::lists('nombre', 'id')->all();
    //     return view('especialidades.index')
    //         ->with('especialidades', $especialidades)
		// 				->with('tematicas', $tematicas);
    // }
    // DataTables
    public function index(EspecialidadDataTable $especialidadDataTable)
		{
			return $especialidadDataTable->render('especialidades.index');
		}

    /**
     * Show the form for creating a new Especialidad.
     *
     * @return Response
     */
    public function create()
    {
				$tematicas = AreaTematica::lists('nombre', 'id')->all();
        return view('especialidades.create')->with('tematicas', $tematicas);
    }

    /**
     * Store a newly created Especialidad in storage.
     *
     * @param CreateEspecialidadRequest $request
     *
     * @return Response
     */
    public function store(CreateEspecialidadRequest $request)
    {
        $input = $request->all();
        $especialidad = $this->especialidadRepository->create($input);
        Flash::success('Especialidad guardada exitosamente.');
        return redirect(route('especialidades.index'));
    }

    /**
     * Display the specified Especialidad.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $especialidad = $this->especialidadRepository->findWithoutFail($id);
        if (empty($especialidad)) {
            Flash::error('Especialidad no encontrada');
            return redirect(route('especialidades.index'));
        }
        return view('especialidades.show')->with('especialidad', $especialidad);
    }

    /**
     * Show the form for editing the specified Especialidad.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $especialidad = $this->especialidadRepository->findWithoutFail($id);
        if (empty($especialidad)) {
            Flash::error('Especialidad no encontrada');
            return redirect(route('especialidades.index'));
        }
				$tematicas = AreaTematica::lists('nombre', 'id')->all();
        return view('especialidades.edit')->with('especialidad', $especialidad)->with('tematicas', $tematicas);
    }

    /**
     * Update the specified Especialidad in storage.
     *
     * @param  int              $id
     * @param UpdateEspecialidadRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEspecialidadRequest $request)
    {
        $especialidad = $this->especialidadRepository->findWithoutFail($id);
        if (empty($especialidad)) {
            Flash::error('Especialidad no encontrada');
            return redirect(route('especialidades.index'));
        }
        $especialidad = $this->especialidadRepository->update($request->all(), $id);
        Flash::success('Especialidad modificada exitosamente.');
        return redirect(route('especialidades.index'));
    }

    /**
     * Remove the specified Especialidad from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $especialidad = $this->especialidadRepository->findWithoutFail($id);
        if (empty($especialidad)) {
            Flash::error('Especialidad no encontrada');
            return redirect(route('especialidades.index'));
        }
        $this->especialidadRepository->delete($id);
        Flash::success('Especialidad eliminada exitosamente.');
        return redirect(route('especialidades.index'));
    }
}
