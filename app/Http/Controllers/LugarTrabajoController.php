<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateLugarTrabajoRequest;
use App\Http\Requests\UpdateLugarTrabajoRequest;
use App\Repositories\LugarTrabajoRepository;
use Illuminate\Http\Request;
//Models
use App\Models\Institucion;
//Facades
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class LugarTrabajoController extends AppBaseController
{
    /** @var  LugarTrabajoRepository */
    private $lugarTrabajoRepository;

    public function __construct(LugarTrabajoRepository $lugarTrabajoRepo)
    {
				$this->middleware('auth');
        $this->lugarTrabajoRepository = $lugarTrabajoRepo;
    }

    /**
     * Display a listing of the LugarTrabajo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->lugarTrabajoRepository->pushCriteria(new RequestCriteria($request));
        //$lugarTrabajos = $this->lugarTrabajoRepository->all();
				$lugarTrabajos = $this->lugarTrabajoRepository->paginate(10);
        return view('lugarTrabajos.index')->with('lugarTrabajos', $lugarTrabajos);
    }

    /**
     * Show the form for creating a new LugarTrabajo.
     *
     * @return Response
     */
    public function create()
    {
			$instituciones = Institucion::lists('nombre', 'id')->all();
      return view('lugarTrabajos.create')->with('instituciones', $instituciones);
    }

    /**
     * Store a newly created LugarTrabajo in storage.
     *
     * @param CreateLugarTrabajoRequest $request
     *
     * @return Response
     */
    public function store(CreateLugarTrabajoRequest $request)
    {
        $input = $request->all();
        $lugarTrabajo = $this->lugarTrabajoRepository->create($input);
        Flash::success('Lugar de Trabajo creado exitosamente.');
        return redirect(route('lugarTrabajos.index'));
    }

    /**
     * Display the specified LugarTrabajo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $lugarTrabajo = $this->lugarTrabajoRepository->findWithoutFail($id);
        if (empty($lugarTrabajo)) {
            Flash::error('Lugar de Trabajo no encontrado');
            return redirect(route('lugarTrabajos.index'));
        }
        return view('lugarTrabajos.show')->with('lugarTrabajo', $lugarTrabajo);
    }

    /**
     * Show the form for editing the specified LugarTrabajo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $lugarTrabajo = $this->lugarTrabajoRepository->findWithoutFail($id);
        if (empty($lugarTrabajo)) {
            Flash::error('Lugar de Trabajo no encontrado');
            return redirect(route('lugarTrabajos.index'));
        }
				$instituciones = Institucion::lists('nombre', 'id')->all();
        return view('lugarTrabajos.edit')
						->with('lugarTrabajo', $lugarTrabajo)
						->with('instituciones', $instituciones);
    }

    /**
     * Update the specified LugarTrabajo in storage.
     *
     * @param  int              $id
     * @param UpdateLugarTrabajoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLugarTrabajoRequest $request)
    {
        $lugarTrabajo = $this->lugarTrabajoRepository->findWithoutFail($id);
        if (empty($lugarTrabajo)) {
            Flash::error('Lugar de Trabajo no encontrado');
            return redirect(route('lugarTrabajos.index'));
        }
        $lugarTrabajo = $this->lugarTrabajoRepository->update($request->all(), $id);
        Flash::success('Lugar de Trabajo modificado exitosamente.');
        return redirect(route('lugarTrabajos.index'));
    }

    /**
     * Remove the specified LugarTrabajo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $lugarTrabajo = $this->lugarTrabajoRepository->findWithoutFail($id);

        if (empty($lugarTrabajo)) {
            Flash::error('Lugar de Trabajo no encontrado');

            return redirect(route('lugarTrabajos.index'));
        }

        $this->lugarTrabajoRepository->delete($id);

        Flash::success('Lugar de Trabajo eliminado exitosamente.');

        return redirect(route('lugarTrabajos.index'));
    }
}
