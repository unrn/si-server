<?php namespace App\Http\Middleware;

use Closure, Auth;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
			//Es un simple Middleware que pregunta si el usuario es Admin
			if (Auth::check() && Auth::user()->id === 1) {
				return $next($request);
			}
			return response()->view('errors/403', [], 403);
    }
}
