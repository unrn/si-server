<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
			//Es un simple Middleware que pregunta si el usuario es Admin
			if (Auth::check() && Auth::user()->type) {
				return $next($request);
			}
			return response()->view('errors/403', [], 403);
			//return redirect('home');//Retornar error 403 No Autorizado
    }
}
