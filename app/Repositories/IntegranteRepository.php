<?php

namespace App\Repositories;

use App\Models\Integrante;
use InfyOm\Generator\Common\BaseRepository;

class IntegranteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
			'proyecto_id',
			'investigador_id',
			'perfil_id',
			'fecha_alta',
			'fecha_baja',
			'horas'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Integrante::class;
    }
}
