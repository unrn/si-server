<?php

namespace App\Repositories;

use App\Models\InformeProyecto;
use InfyOm\Generator\Common\BaseRepository;

class InformeProyectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo',
        'descripcion',
        'fecha_presentacion',
        'fecha_entrega'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InformeProyecto::class;
    }
}
