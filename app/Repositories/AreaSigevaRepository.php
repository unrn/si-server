<?php

namespace App\Repositories;

use App\Models\AreaSigeva;
use InfyOm\Generator\Common\BaseRepository;

class AreaSigevaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AreaSigeva::class;
    }
}
