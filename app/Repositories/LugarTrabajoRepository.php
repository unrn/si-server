<?php

namespace App\Repositories;

use App\Models\LugarTrabajo;
use InfyOm\Generator\Common\BaseRepository;

class LugarTrabajoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'abreviacion',
				'institucion_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LugarTrabajo::class;
    }
}
