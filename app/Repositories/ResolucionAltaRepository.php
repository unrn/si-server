<?php

namespace App\Repositories;

use App\Models\ResolucionAlta;
use InfyOm\Generator\Common\BaseRepository;

class ResolucionAltaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'numero',
        'anio'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ResolucionAlta::class;
    }
}
