<?php

namespace App\Repositories;

use App\Models\Investigador;
use InfyOm\Generator\Common\BaseRepository;

class InvestigadorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'legajo',
        'apellido',
        'nombre',
        'dni',
        'cuil',
        'fecha_nac',
        'sexo',
        'email_personal',
        'email_institucional',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Investigador::class;
    }
}
