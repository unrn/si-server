<?php

namespace App\Repositories;

use App\Models\TipoProyecto;
use InfyOm\Generator\Common\BaseRepository;

class TipoProyectoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'abreviacion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoProyecto::class;
    }
}
