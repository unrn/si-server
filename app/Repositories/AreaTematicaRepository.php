<?php

namespace App\Repositories;

use App\Models\AreaTematica;
use InfyOm\Generator\Common\BaseRepository;

class AreaTematicaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AreaTematica::class;
    }
}
