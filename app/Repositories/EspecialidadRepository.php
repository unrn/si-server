<?php

namespace App\Repositories;

use App\Models\Especialidad;
use InfyOm\Generator\Common\BaseRepository;

class EspecialidadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
				'area_tematica_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Especialidad::class;
    }
}
