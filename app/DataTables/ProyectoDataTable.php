<?php namespace App\DataTables;

use App\Models\Proyecto;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;

class ProyectoDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('Acciones', 'proyectos.datatables_actions')
						->editColumn('acreditado', function($proyecto) {
							return $proyecto->acreditado == 0 ? '<i class="fa fa-close" style="color:red;">NO</i>' : '<i class="fa fa-check" style="color:green;">SI</i>';
						})
						->editColumn('fecha_inicio', function ($proyecto) {
		        	return $proyecto->fecha_inicio ? with(new Carbon($proyecto->fecha_inicio))->format('d-m-Y') : '';
		        })
						->editColumn('fecha_fin', function ($proyecto) {
		        	return $proyecto->fecha_fin ? with(new Carbon($proyecto->fecha_fin))->format('d-m-Y') : '';
		        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $proyectos = Proyecto::join('tipo_proyectos', 'proyectos.tipo_id', '=', 'tipo_proyectos.id')
					->join('area_tematicas', 'proyectos.area_tematica_id', '=', 'area_tematicas.id')
					->join('instituciones', 'proyectos.institucion_id', '=', 'instituciones.id')
					->select(['proyectos.*',
										'tipo_proyectos.abreviacion AS tipo_proyecto',
										'area_tematicas.nombre AS area_tematica',
										'instituciones.nombre AS institucion'])
					->orderBy('id','DESC');

        return $this->applyScopes($proyectos);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
						->columns(array_merge(
								$this->getColumns(),
								[
										'Acciones' => [
												'orderable' => false,
												'searchable' => false,
												'printable' => false,
												'exportable' => false
										]
								]
						))
            ->parameters([
								'aLengthMenu' => [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
              	'dom' => 'Blfrtip',
                'scrollX' => true,
								'stateSave' => true,
								'oLanguage' => [
										'sInfo' => 'Mostrando _START_ de _END_ de _TOTAL_ entradas',
										'sInfoEmpty' => 'Mostrando 0 de 0 de 0 entradas',
										'sInfoFiltered' => '(filtrada de _MAX_ entradas en total)',
										'sSearch' => 'Buscar:',
										'sLengthMenu' => 'Mostrar _MENU_ entradas',
										'sZeroRecords' => 'No se encontraron registros coincidentes',
										'oPaginate' => [
												'sFirst' => 'Primero',
												'sLast' => 'Ultimo',
												'sNext' => 'Siquiente',
												'sPrevious' => 'Anterior'
										],
										'buttons' => [
												'print' => 'Imprimir',
												'reset' => 'Limpiar',
												'reload' => 'Recargar',
												'create' => 'Crear'
										]
								],
                'buttons' => [
                    'create',
										'excel',
                    'print',
                    'reset',
                    'reload'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'Código' => ['name' => 'codigo', 'data' => 'codigo'],
						'Tipo' => ['name' => 'tipo_id', 'data' => 'tipo_proyecto', 'orderable' => false],
						'Acred' => ['name' => 'acreditado', 'data' => 'acreditado', 'orderable' => false],
            'Título' => ['width' => '30%', 'name' => 'titulo', 'data' => 'titulo', 'orderable' => false],
            'Inicio' => ['name' => 'fecha_inicio', 'data' => 'fecha_inicio', 'orderable' => false],
            'Fin' => ['name' => 'fecha_fin', 'data' => 'fecha_fin', 'orderable' => false],
						'Área Temática' => ['name' => 'area_tematica_id', 'data' => 'area_tematica', 'orderable' => false],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'proyectos';
    }
}
