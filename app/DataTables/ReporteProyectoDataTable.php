<?php namespace App\DataTables;

use App\Models\Investigador;
use Form;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;

class ReporteProyectoDataTable extends DataTable
{
		private $consulta = null;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
						->addColumn('Editar', function ($proyecto) {
							return '<a href="' . route('proyectos.edit', [$proyecto->id]) . '" class=\'btn btn-success btn-xs\'><i class="fa fa-edit"></i></a>';
						})
						->editColumn('fecha_inicio', function ($proyecto) {
		        	return $proyecto->fecha_inicio ? with(new Carbon($proyecto->fecha_inicio))->format('d-m-Y') : '';
		        })
						->editColumn('fecha_fin', function ($proyecto) {
		        	return $proyecto->fecha_fin ? with(new Carbon($proyecto->fecha_fin))->format('d-m-Y') : '';
		        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
      return $this->consulta;
    }

		public function setConsulta($query)
		{
			$this->consulta = $query;
		}

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
						->columns(array_merge(
							$this->getColumns(),
							[
								'Editar' => [
									'orderable' => false,
									'searchable' => false,
									'printable' => false,
									'exportable' => false
								]
							]
						))
            ->parameters([
								'aLengthMenu' => [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
              	'dom' => 'Blfrtip',
                'scrollX' => true,
								'stateSave' => true,
								'oLanguage' => [
												'sInfo' => 'Mostrando _START_ de _END_ de _TOTAL_ entradas',
												'sInfoEmpty' => 'Mostrando 0 de 0 de 0 entradas',
												'sInfoFiltered' => '(filtrada de _MAX_ entradas en total)',
												'sSearch' => 'Buscar:',
												'sLengthMenu' => 'Mostrar _MENU_ entradas',
												'sZeroRecords' => 'No se encontraron registros coincidentes',
												'oPaginate' => [
															'sFirst' => 'Primero',
															'sLast' => 'Ultimo',
															'sNext' => 'Siquiente',
															'sPrevious' => 'Anterior'
												],
												'buttons' => [
													'print' => 'Imprimir',
													'reset' => 'Limpiar',
													'reload' => 'Recargar',
													'create' => 'Crear'
												],
												'select' => [
													'rows' => [
														'_' => '%d filas seleccionadas',
														1 => '1 fila selecionada'
														]
												]
								],
                'buttons' => [
									[
										'extend' => 'colvis',
										'text' => '<i class="fa fa-th"></i> Seleccionar Columnas',
									],
									[
										'extend' => 'collection',
										'text' => '<i class="fa fa-download"></i> Exportar Todo',
										'buttons' => [
											[
												'extend' => 'excel',
												'text' => '<i class="fa fa-file-excel-o"></i> Excel',
												'title' => 'Reporte Integrantes',
												'exportOptions' => [
													'columns' => ':visible',
												]
											],
											[
												'extend' => 'print',
												'text' => '<i class="fa fa-print"></i> Imprimir',
												'title' => 'Reporte Integrantes',
												'exportOptions' => [
													'columns' => ':visible',
												]
											]
										]
									],
									[
										'extend' => 'collection',
										'text' => '<i class="fa fa-download"></i> Exportar Filas Seleccionadas',
										'buttons' => [
											[
												'extend' => 'excel',
												'text' => '<i class="fa fa-file-excel-o"></i> Excel',
												'title' => 'Reporte Integrantes',
												'exportOptions' => [
													'columns' => ':visible',
													'modifier' => [
														'selected' => true
													]
												]
											],
											[
												'extend' => 'print',
												'text' => '<i class="fa fa-print"></i> Imprimir',
												'title' => 'Reporte Integrantes',
												'exportOptions' => [
													'columns' => ':visible',
													'modifier' => [
														'selected' => true
													]
												]
											]
										]
									],
									//'reset',
									//'reload',
								],
								'columnDefs' => [
									'targets' => -1,
									'visible' => false
								],
								'select' => true
            	]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
			return [
				'Código' => ['name' => 'codigo', 'data' => 'codigo'],
				'Tipo' => ['name' => 'tipo_id', 'data' => 'tipo_proyecto', 'orderable' => false],
				'Director' => ['name' => 'integrantes.investigador_id', 'data' => 'director', 'orderable' => false],
				'Título' => ['width' => '30%', 'name' => 'titulo', 'data' => 'titulo', 'orderable' => false],
				'Inicio' => ['name' => 'fecha_inicio', 'data' => 'fecha_inicio', 'orderable' => false],
				'Fin' => ['name' => 'fecha_fin', 'data' => 'fecha_fin', 'orderable' => false],
				'Área Temática' => ['name' => 'area_tematica_id', 'data' => 'area_tematica', 'orderable' => false],
			];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'proyectos' . time();
    }
}
