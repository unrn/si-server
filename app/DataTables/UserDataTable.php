<?php namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;

class UserDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
			return $this->datatables
					->eloquent($this->query())
					->addColumn('Acciones', 'users.datatables_actions')
					->editColumn('type', function ($user) {
						return $user->type ? 'Administrador' : 'Usuario';
					})
					->editColumn('avatar', function ($user) {
						return '<img src="'. url('/images/avatars/', $user->avatar) . '" class="profile-user-img img-responsive"/>';
					})
					->make(true);

    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
      	$users = User::select();
				return $users;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'Acciones' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
							'processing' => true,
							'serverSide' => true,
							'oLanguage' => [
									'sInfo' => 'Mostrando _START_ de _END_ de _TOTAL_ entradas',
									'sInfoEmpty' => 'Mostrando 0 de 0 de 0 entradas',
									'sInfoFiltered' => '(filtrada de _MAX_ entradas en total)',
									'sSearch' => 'Buscar:',
									'sLengthMenu' => 'Mostrar _MENU_ entradas',
									'sZeroRecords' => 'No se encontraron registros coincidentes',
									'oPaginate' => [
											'sFirst' => 'Primero',
											'sLast' => 'Ultimo',
											'sNext' => 'Siquiente',
											'sPrevious' => 'Anterior'
									],
									'buttons' => [
											'print' => 'Imprimir',
											'reset' => 'Limpiar',
											'reload' => 'Recargar',
											'create' => 'Crear'
									]
							],
							'buttons' => [
										'create',
										'excel',
										'print',
										'reset',
										'reload'
							],
							'aLengthMenu' => [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
              'dom' => 'Blfrtip',
              'scrollX' => true
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
						'Imagen' => ['name' => 'users.avatar', 'data' => 'avatar'],
						'Nombre' => ['name' => 'users.name', 'data' => 'name'],
						'Email' => ['name' => 'users.email', 'data' => 'email'],
						'Tipo' => ['name' => 'users.type', 'data' => 'type']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usuarios';
    }
}
