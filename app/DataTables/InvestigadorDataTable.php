<?php namespace App\DataTables;

use App\Models\Investigador;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;

class InvestigadorDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
						->eloquent($this->query())
						->addColumn('Acciones', 'investigadores.datatables_actions')
						->editColumn('fecha_nac', function ($investigador) {
		        	return $investigador->fecha_nac ? with(new Carbon($investigador->fecha_nac))->format('d-m-Y') : '';
		        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $especialidades = Investigador::leftjoin('especialidades', 'investigadores.especialidad_id', '=', 'especialidades.id')->select(['investigadores.*', 'especialidades.nombre AS especialidad']);
				return $especialidades;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'Acciones' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
              'stateSave' => true,
							'oLanguage' => [
									'sInfo' => 'Mostrando _START_ de _END_ de _TOTAL_ entradas',
									'sInfoEmpty' => 'Mostrando 0 de 0 de 0 entradas',
									'sInfoFiltered' => '(filtrada de _MAX_ entradas en total)',
									'sSearch' => 'Buscar:',
									'sLengthMenu' => 'Mostrar _MENU_ entradas',
									'sZeroRecords' => 'No se encontraron registros coincidentes',
									'oPaginate' => [
											'sFirst' => 'Primero',
											'sLast' => 'Ultimo',
											'sNext' => 'Siquiente',
											'sPrevious' => 'Anterior'
									],
									'buttons' => [
											'print' => 'Imprimir',
											'reset' => 'Limpiar',
											'reload' => 'Recargar',
											'create' => 'Crear'
									]
							],
							'buttons' => [
										'create',
										'excel',
										'print',
										'reset',
										'reload'
							],
							'aLengthMenu' => [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
              'dom' => 'Blfrtip',
              'scrollX' => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
      return [
        'Leg' => ['name' => 'investigadores.legajo', 'data' => 'legajo'],
				'Apellido' => ['name' => 'investigadores.apellido', 'data' => 'apellido'],
				'Nombre' => ['name' => 'investigadores.nombre', 'data' => 'nombre'],
				'Cuil' => ['name' => 'investigadores.cuil', 'data' => 'cuil'],
				// 'Nacimiento' => ['name' => 'investigadores.fecha_nac', 'data' => 'fecha_nac'],
				'Email Personal' => ['name' => 'investigadores.email_personal', 'data' => 'email_personal'],
				'Email Institucional' => ['name' => 'investigadores.email_institucional', 'data' => 'email_institucional'],
				'Especialidad' => ['name' => 'especialidades.nombre', 'data' => 'especialidad']
      ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'investigadores';
    }
}
