<?php namespace App\DataTables;

use App\Models\Integrante;
use App\Models\Investigador;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;

class IntegranteDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('Acciones', 'integrantes.datatables_actions')
						->editColumn('codigo_proyecto', function ($integrante) {
							return '<a href="'. route('proyectos.edit', [$integrante->proyecto()->id]) .'" class="btn btn-warning btn-xs">'. $integrante->proyecto()->codigo .'</a>';
						})
						->editColumn('investigador_id', function ($integrante) {
							return $integrante->investigador()->fullName();
						})
						->editColumn('fecha_alta', function ($integrante) {
		        	return $integrante->fecha_alta ? with(new Carbon($integrante->fecha_alta))->format('d-m-Y') : '';
		        })
						->editColumn('fecha_baja', function ($integrante) {
		        	return $integrante->fecha_baja ? with(new Carbon($integrante->fecha_baja))->format('d-m-Y') : '';
		        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $integrantes = Integrante::join('proyectos', 'integrantes.proyecto_id', '=', 'proyectos.id')
					->join('perfiles', 'integrantes.perfil_id', '=', 'perfiles.id')
					->select(['integrantes.*',
										'proyectos.codigo AS codigo_proyecto',
										'perfiles.nombre AS perfil']);

        return $this->applyScopes($integrantes);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
						->columns(array_merge(
								$this->getColumns(),
								[
										'Acciones' => [
												'orderable' => false,
												'searchable' => false,
												'printable' => false,
												'exportable' => false
										]
								]
						))
            //->addAction(['width' => '10%'])
            //->ajax('')
            ->parameters([
								'aLengthMenu' => [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
              	'dom' => 'Blfrtip',
                'scrollX' => true,
								'oLanguage' => [
										'sInfo' => 'Mostrando _START_ de _END_ de _TOTAL_ entradas',
										'sInfoEmpty' => 'Mostrando 0 de 0 de 0 entradas',
										'sInfoFiltered' => '(filtrada de _MAX_ entradas en total)',
										'sSearch' => 'Buscar:',
										'sLengthMenu' => 'Mostrar _MENU_ entradas',
										'sZeroRecords' => 'No se encontraron registros coincidentes',
										'oPaginate' => [
												'sFirst' => 'Primero',
												'sLast' => 'Ultimo',
												'sNext' => 'Siquiente',
												'sPrevious' => 'Anterior'
										],
										'buttons' => [
												'print' => 'Imprimir',
												'reset' => 'Limpiar',
												'reload' => 'Recargar',
												'create' => 'Crear'
										]
								],
                'buttons' => [
										'excel',
                    'print',
                    'reset',
                    'reload'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'Integrante' => ['name' => 'investigador_id', 'data' => 'investigador_id'],
						'Proyecto' => ['name' => 'proyecto_id', 'data' => 'codigo_proyecto'],
						'Perfil' => ['name' => 'perfil_id', 'data' => 'perfil'],
						'Fecha Alta' => ['name' => 'fecha_alta', 'data' => 'fecha_alta'],
            'Fecha Baja' => ['name' => 'fecha_baja', 'data' => 'fecha_baja'],
            'Horas' => ['name' => 'horas', 'data' => 'horas'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'integrantes';
    }
}
