<?php namespace App\DataTables;

use App\Models\Investigador;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;

class ReporteIntegranteDataTable extends DataTable
{
		private $consulta = null;
		private $columnas = null;
		private $nombre_archivo = null;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->of($this->query())
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
      return $this->consulta;
    }

		public function setConsulta($query)
		{
			$this->consulta = $query;
		}

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
						->columns(array_merge($this->getColumns()))
            ->parameters([
								'stateSave' => true,
								'aLengthMenu' => [[10, 25, 50, -1], [10, 25, 50, 'Todo']],
              	'dom' => 'Blfrtip',
                'scrollX' => true,
								'oLanguage' => [
												'sInfo' => 'Mostrando _START_ de _END_ de _TOTAL_ entradas',
												'sInfoEmpty' => 'Mostrando 0 de 0 de 0 entradas',
												'sInfoFiltered' => '(filtrada de _MAX_ entradas en total)',
												'sSearch' => 'Buscar:',
												'sLengthMenu' => 'Mostrar _MENU_ entradas',
												'sZeroRecords' => 'No se encontraron registros coincidentes',
												'oPaginate' => [
															'sFirst' => 'Primero',
															'sLast' => 'Ultimo',
															'sNext' => 'Siquiente',
															'sPrevious' => 'Anterior'
												],
												'buttons' => [
													'print' => 'Imprimir',
													'reset' => 'Limpiar',
													'reload' => 'Recargar',
													'create' => 'Crear'
												],
												'select' => [
													'rows' => [
														'_' => '%d filas seleccionadas',
														1 => '1 fila selecionada'
														]
												]
								],
                'buttons' => [
										[
											'extend' => 'colvis',
											'text' => '<i class="fa fa-th"></i> Seleccionar Columnas',
										],
										[
											'extend' => 'collection',
											'text' => '<i class="fa fa-download"></i> Exportar Todo',
											'buttons' => [
												[
													'extend' => 'excel',
													'text' => '<i class="fa fa-file-excel-o"></i> Excel',
													'title' => 'Reporte Integrantes',
													'exportOptions' => [
														'columns' => ':visible',
													]
												],
												[
													'extend' => 'print',
													'text' => '<i class="fa fa-print"></i> Imprimir',
													'title' => 'Reporte Integrantes',
													'exportOptions' => [
														'columns' => ':visible',
													]
												]
											]
										],
										[
											'extend' => 'collection',
											'text' => '<i class="fa fa-download"></i> Exportar Filas Seleccionadas',
											'buttons' => [
												[
													'extend' => 'excel',
													'text' => '<i class="fa fa-file-excel-o"></i> Excel',
													'title' => 'Reporte Integrantes',
													'exportOptions' => [
														'columns' => ':visible',
														'modifier' => [
															'selected' => true
														]
													]
												],
												[
													'extend' => 'print',
													'text' => '<i class="fa fa-print"></i> Imprimir',
													'title' => 'Reporte Integrantes',
													'exportOptions' => [
														'columns' => ':visible',
														'modifier' => [
															'selected' => true
														]
													]
												]
											]
										],
                    //'reset',
                    //'reload',
                ],
								'columnDefs' => [
            			'targets' => -1,
            			'visible' => false
        				],
								'select' => true
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
			return $this->columnas;
    }

		public function setColumnas($columns)
		{
			$this->columnas = $columns;
		}

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return $this->nombre_archivo . time();
    }

		public function setNombreArchivo($filename)
		{
			$this->nombre_archivo = $filename;
		}
}
