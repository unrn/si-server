# Changelog
## ¿Cómo escribir?
- [Año-Mes-Día] `Tipo`: descripción.

## TO DO - Segundo Sprint
- Auntenticación y Roles: Seguridad
	- Resolver el error: `TokenMismatchException in VerifyCsrfToken.php line 67`
- Modificar el paquete InfyOm para que cumpla las reglas de HTML5
- Gestionar el manejo de los errores de página tales como: 401, 403, 404, 500, 503, etc.

## v0.4.3 Incentivos :grinning:
- [2017-06-23] `resources/views/home/info`: se cambió la informacion de docentes investigadores a total de investigadores. El motivo del cambio se ve afectado a que todos los investigadores con legajo no necesariamente son docentes; los mismos pueden ser No Docentes también.
- [2017-06-23] `resources/views/investigadores/historial/incentivos/table`: se corrigió un error de modelo. En ves de referencial al usuario así `$historia->user()->name`, se hacia así `$historia->user`.

## v0.4.3 Send me an email :mailbox_with_mail:
- [2017-06-23] `resources/views/proyectos/edit`: se restringió el acceso a la pestaña de los informes de proyecto. Solo los usuarios administradores podrán acceder. Sin embargo, esta funcionalidad todavia esta en revisión.
- [2017-06-23] `resources/views/investigadores/edit`: se restringió el acceso a la pestaña de la ampliacioón del historial. Solo los usuarios administradores podrán acceder.
- [2017-06-22] `Http/Controllers/HomeController@postContact`: se creó el metodo que recibe el request de la vista y lo procesa para formar el mail. Utilizando la clase Mail que provee Laravel, pude enviar un email a la plataforma `MailTrap`. Mailtrap es una solución que permite probar las notificaciones por correo electrónico sin enviarlas a los usuarios reales de la aplicación. También permite ver todos los correos electrónicos en línea, enviarlos a un buzón de correo regular, compartir con el equipo y mucho más! Mailtrap no es un sistema transaccional de entrega de correo electrónico.
- [2017-06-22] `views/home/contact`: se crearon las vistas para la página de contacto con el fin de enviar mail desde la aplicación.

## v0.4.2 Back to work :bust_in_silhouette:
- [2017-06-22] `app/Exceptions/Handler`: se modificó el método render para que al encontrar la excepción `NotFoundHttpException` redirija el error a la página 404.
- [2017-06-21] `resources/views/users/profile`: se modificó la vista para que solo acepte avatars con formato de imagen jpg, jpeg, png.
- [2017-06-19] `app/Exceptions/Handler`: se modificó el metodo render para filtrar los errores y direccionarlos a sus respectivas páginas.
- [2017-06-19] `Http/Controllers/Auth/AuthController`: se modificó el metodo register para que redireccione a la vista `users.index` mostrando un mensaje de confirmación.
- [2017-06-19] `app/User`: se modificaron las vistas de los usuarios.
- [2017-06-19] `app/models/HistorialIncentivo`: se agregó el atributo `user_id`. Se modificó el modelo, el contralador y la base de datos agregando los mapeos.
- [2017-06-14] `resources/views/errors`: se modificaron las vistas de los errores (400, 403, 404) para que el botón vuelva a la página visitada anteriormente.
- [2017-06-14] `resources/views/investigadores`: se eliminó código repetido en cuanto a la vista de del usuario no administrador. En cuanto a la tabla, ya no se  mostrará la fecha de nacimiento.
- [2017-06-14] `resources/viewa/reportes`: se agregó un botón para regresar a la página anteriormente vista.
- [2017-06-13] `resources/views/investigadores/show_fields`: se eliminó el archivo que contenia los campos de vista. Ahora, se creó un método en el archivo `investigadores/fields.blade.php` que deshabilita los campos si el rol del usuario no es administrador.
- [2017-06-13] `resources/views/home/buscadores`: se editó el archivo para que el usuario no administrador pueda buscar y visualizar los investigadores y proyectos.
- [2017-06-13] `config/session`: se cambió el valor de `lifetime` de 432000 minutos a 60 (la sesión expirará en 1h). Tambien se cambió el valor de `expire_on_close` de true a false (al cerrar el navegador no se destruye la sesión).
- [2017-06-13] `resources/views/investigadores/proyectos_asociados`: tanto en ésta vista como en la `pryectos/integrantes_proyecto_table` se cambiaron las fechas de formato inglés al español.

## v0.4.1 Upload Information
- [2017-03-14] `App/Http/Controller/Integrante`: se modificaron los mensajes para que contengan información del integrante que se da de alta, baja o es modificado.
- [2017-03-14] `App/Http/Controller/Integrante`: ahora, un integrante puede aparacer más de una vez en el proyecto siempre y cuando se haya dado de baja anteriormente. Ejemplo: un becario beita se da de baja para pasar a ser investigador.
- [2017-03-13] `App/Investigador`: se creó un método para verificar si el investigador es externo o no.
- [2017-02-24] `App/Http/Controller/HistorialLugarTrabajo`: se eliminó el `HistorialRol`, ya que se integró al HistorialLugarTrabajo. De esta manera podemos comprobar mas facilmente quienes son los investigadores externos.
- [2017-02-24] `App/Http/Controller/HistorialIncentivo`: se cambió la fecha de inicio y fin por el año solamente, ya que los días y meses no importan en la categorización (se modificaron tanto las clases y archivos intervinientes como la base de datos).
- [2017-02-14] `Database/migrations/informe_proyecto_table`: se acgregaron los campos `fecha_evaluacion` y `satisfaccion`. El segundo especifica si el informe fue satisfactorio o no en la evaluación.
- [2017-02-13] `App/Http/Routes`: las rutas de autocomplete se sacaron del Middleware `admin` para que los usuarios comunes puedan acceder.
- [2017-02-13] `App`: se modificaron algunas vistas (nada que documentar). La mayor parte del mes me concentré en subir los datos requeridos. Los reportes no se pueden probar sin estos datos.

## v0.4.0 Customized Google Charts
- [2016-12-29] `App/Http/Controller/UserController@updateAvatar`: se agregó un Validator que asegure que el archivo subido como foto de perfil sea una imagen. De lo contrario mostrará un mensaje de error.
- [2016-12-29] `App/Http/Controller/UserController@updateAvatar`: se creó el metodo para modificar la imagen de perfil del usuario. Al cambiar la imagen, se elimina la anterior, a menos que sea la imagen `default.jpg`.
- [2016-12-29] `App/Http/Controller/UserController@profile`: se creó el perfil de usuario para que se pueda modificar la contraseña y la foto de perfil (por separado). Cada usuario podrá modificar unicamente su perfil. Para ello se crearon los metodos `ResetPassword` y `updateAvatar` dentro del controlador UserController.
- [2016-12-28] `views/users`: se crearon las vistas de usuarios (create, edit, profile) para poder modificar los nombres, email, restablecer contraseñas y cambiar el tipo de usuario (normal o administrador).
- [2016-12-28] `App/Http/Middleware/SuperAdmin`: se creó un Middleware para que el usuario con id = 1 sea el encargado de manipular a los demás usuarios. Cada usuarios solo podrá cambiar su foto de perfil y restablecer su contraseña.
- [2016-12-27] `App/Http/Controller/UserController`: se creó un controlador para los usuarios, el cuál provee de los metodos del resources y el perfil de usuario (que puede ser accedido por todos los usuarios).
- [2016-12-16] `App/Http/Controllers/Auth/AuthController`: sólo los administradores pueden crear usuarios. Solo se puede crear usuarios con emails de dominio de la Universidad (Ej: test@unrn.edu.ar)
- [2016-12-15] `views/home`: se reestructuró la vista del home. Se creó una carpeta que contenga las vistas iniciales. El home, por el momento, se divide en las vistas: `index`, `charts`, `buscadores` e `info`. La vista `index` contiene a las demás (ver comentario al pie).
- [2016-12-15] `views/home`: se agregaron buscadores. Uno para proyectos (por código) y otro para investigadores (por apellido y nombre).
- [2016-12-15] `App/Controllers/Proyecto@autocomplete`: se creó el método para buscar un proyecto por código.
- [2016-12-14] `views/home`: se crearon los gráficos de torta (investigadores por genero) y uno de barras verticales (cantidad de proyectos por sede) con la herramienta de Google Charts. Fuente: https://developers.google.com/chart/.
- [2016-12-14] `App/Controllers/HomeController`: se crearon las consultas para los charts de `investigadores por sexo` y `cantidad de proyectos por sede` (discriminando por tipo).
- [2016-12-14] `App/Controllers/InvestigadorController@autocomplete`: se mejoró el buscador de investigadores utilizando la función CONCAT de MySql.

## v0.3.1 Example Version
- [2016-12-05] `views/proyectos/informe_proyecto_table`: se creó una nueva vista para los informes de proyectos. Anteriormente era rebuscada y confusa.
- [2016-12-02] `database/migrations/historiales`: se agregaron los campos `user` y `update` a las 4 tablas de los historiales para poder auditar quien y cuando se realizan los cambios.
- [2016-12-02] `App/Models/InvestigadorNota`: se elimino el modelo, vista, controlador y base de datos del InvestigadorNota. No será utilizado ya que los historiales remplazaron está funcionalidad.
- [2016-12-01] `views/investigadores`: se cambió el tab `notas` por `proyectos`. Ahora muestra los proyectos en los que participó o participa el investigador con su perfil y horas.
- [2016-12-01] `views/proyectos/fields`: se editó la forma en que se buscan las resoluciones de alta. Al seleccionar una, carga automaticamente las fechas.
- [2016-11-24] `App/DataTables/ProyectoDataTables`: se agregó un atributo `stateSave => true` con el fin de mantener el estado de dicha tabla en la navegación.
- [2016-11-23] `views/proyectos/integrantes_proyecto_table`: se agregó la cantidad de integrantes.
- [2016-11-18] `Reportes`: se corrigió un error que no permitía imprimir si no se seleccionaba filas.
- [2016-11-18] `Reportes`: se agregó una funcionalidad para que se pueda elegir las filas y las columnas para exportar o imprimir, despues de hacer los filtros.
- [2016-11-18] `ResetPassword`: se probó exitosamente el reseteo de contraseña usando como servidor a Mailtrap.io.
- [2016-11-18] `Reportes`: se cambió la forma en que imprimia y exportaba los reportes ya que lo hacía por el lado del servidor. Esto provocaba que se refresque la página y se pierdan los filtros.
- [2016-11-15] `App/models/proyecto`: se modifico un bug en el metodo de director.
- [2016-11-15] `views/investigadores/datatables_actions`: se separó la parte del html de php para una mayor organización.
- [2016-11-15] `views/especialidades/datatables_actions`: se separó la parte del html de php para una mayor organización.

## v0.3.0 Come reports
- [2016-11-07] `views/proyectos/show`: se creó la vista de proyectos para el usuario no administrador.
- [2016-11-02] `views`: se añadió la validación a los campos.
- [2016-11-01] `views/layaouts/menu_admin`: se modificó el menú de los administradores para que sea más leible y fácil de entender.
- [2016-10-31] `Models`: se eliminó el SoftDelete de los modelos y los atributos protected="deleted_at".
- [2016-10-31] `Proyecto`: refactorización de código.
- [2016-10-24] `Reportes`: primera versión del reporte de proyectos.
- [2016-10-24] `views/reportes/proyectos`: se crearon las vistas necesarias con sus respectivos filtros para realizar los reportes de los proyectos.
- [2016-10-24] `App/Http/Controllers/ReporteController`: se creó un controlador para gestionar las consultas referidas a los reportes. El controlador se encarga de obtener el DataTablesRequest de la vista para luego ser modificado y enviado a la clase para su creación. Una vez creado se renderiza y se vuelve a enviar a la vista.
- [2016-10-24] `DataTables/ReporteDataTable`: se creó una clase DataTables para administar los reportes. Los mismos son seteados y cargados desde el controlador.
- [2016-10-13] `DataTables`: se cambiaron las librerias y se agregaron las de bootstrap. Ver ejemplo en `views/proyectos/datatables_actions`
- [2016-10-12] `Proyecto`: se cambió el idioma al español de todos los mensajes Flash y los Alerts.
- [2016-10-11] `App/controllers/ReporteController`: Se crearon los reportes de prueba para los investigadores de acuerdo con la edad y el area tematica.

## v0.2.2 Perfiles, Proyecto, Resolucion Alta, Sigeva, Informes Proyecto, Integrantes
- [2016-10-06] `views/proyectos/informe_proyecto`: se creó una vista que genera los informes del proyecto.
- [2016-10-06] `views/layouts/menu_admin`: se actualizó el menú del administrador.
- [2016-10-06] `App/Models/InformeProyecto`: se creó el migrate, seeders, views, controller y model de los informes del proyecto de investigación.
- [2016-10-06] `views/proyectos/integrantes_proyecto_table`: se creó una vista que muestra los integrantes del proyecto, pudiendo agregar, modificar, dar de baja o eliminar.
- [2016-09-30] `App/Models/Integrante`: se creó el migrate, seeders, views, controller y model de los integrantes que componen eun proyecto de investigación.
- [2016-09-30] `App/Models/Proyecto`: se agregó el atributo area_sigeva_id relacionando con la tabla de las areas de SIGEVA.
- [2016-09-30] `App/Models/AreaSigeva`: se creó el migrate, seeders, views, controller y model de las areas tematicas que tiene SIGEVA. Las mismas van a servir para clasificar los proyectos para futuras evaluaciones.
- [2016-09-23] `App/Models/Proyectos`: se agregó el atributo resolucion_alta_id tanto en el modelo como en las vistas y controllers.
- [2016-09-20] `App/Models/ResolucionAlta`: se creó el migrate, seeders, views, controller y model de las resoluciones de alta de proyectos.
- [2016-09-15] `App/Models/Proyectos`: se creó el migrate, seeders, views, controller y model de los tipos de proyectos. Está es una primera versión pendiente a modificarla: agregar el director en la vista.
- [2016-09-15] `views/layouts/menu_admin`: se actualizó el menú del administrador.
- [2016-09-15] `App/Models/TipoProyectos`: se creó el migrate, seeders, views, controller y model de los tipos de proyectos.
- [2016-09-14] `App/Models/Perfiles`: se creó el migrate, seeders, views, controller y model de los perfiles de los integrantes.

## v0.2.1 AreaTematica, Especialidad, Datatables, HerokuApp
- [2016-09-13] `App/DataTables/InvestigadorDataTable`: se cambió la consulta por un leftjoin.
- [2016-09-12] `App/views/investigadores/historial`: se cambiaron las clases de los input a `required => true`.
- [2016-09-09] `App/DataTables/InvestigadorDataTable`: se añadió seguridad en la columna `acciones` permitiendo al usuario administrador ver todas las opciones y al usuario comun ver unicamente la opcion de show.
- [2016-09-08] `Proyecto`: se creó el archivo `Procfile` para el deploy en Heroku.
- [2016-09-08] `Proyecto`: se instaló el plugin Carbon para administar los formatos de las fechas para los DataTables.
- [2016-09-08] `App/Http/Historiales`: se modificaron los historiales para que no guarde las fechas con el siguiente formato `0000-00-00`.
- [2016-09-08] `App/Datatables/InvestigadorDataTable`: se creó este archivo para ordenar la configuración del datatable del modelo Especialidad. En el index de dicho modelo se realizaó una renderización para pasar el datatable generado a la vista.
- [2016-09-07] `App/Datatables/EspecialidadDataTable`: se creó este archivo para ordenar la configuración del datatable del modelo Especialidad. En el index de dicho modelo se realizaó una renderización para pasar el datatable generado a la vista.
- [2016-09-07] `Proyecto`: se instaló el plugin de Yajra de Datatables para Laravel(https://github.com/yajra/laravel-datatables).
- [2016-08-31] `App/Investigador`: se agregó el campo especialidad perteneciente al investigador.
- [2016-08-31] `App/Especialidad`: se creó el migrate, seeders, views, controller y model de la especialidad.
- [2016-08-30] `App/AreaTematica`: se creó el migrate, seeders, views, controller y model del area tematica.

## v0.2.0 Sprint 1 -First Version
- [2016-08-26] `views/layouts/menu_user`: se creó un menú para el usaurio común desligandolé toda creación y modificación de los investigadores y demás tablas.
- [2016-08-26] `views/Investigadores/show`: se modificó la vista para el usuario no administrador mostrando correctamente la información del Investigador.
- [2016-08-26] `views/roles`: se adapataron las vistas de fields, show_fields y table para la nueva bd.
- [2016-08-26] `app/HistorialRol`: primera versión funcionando del Historial de Roles por Institución (migration, sedeer, model, Controller, Routes).
- [2016-08-24] `app/HistorialLugarTrabajo`: primera versión funcionando del Historial de Lugares de Trabajo (migration, sedeer, model, Controller, Routes).
- [2016-08-24] `app/LugarTrabajo`: se corrigieron errores en las reglas de validación de Update. Se agregó la Institucion para poder crear y modificar.
- [2016-08-24] `app/HistorialIncentivo`: primera versión funcionando del Historial de categoria de Incentivo (migration, sedeer, model, Controller, Routes).
- [2016-08-24] `app/HistorialAcademico`: primera versión funcionando del HistorialAcademico (migration, sedeer, model, Controller, Routes).

## v0.1.1 Reestructuración de la BD
- [2016-08-19] `Proyecto`: se reestructuró la base de datos y cambió el modelo del proyecto. Se agregaron models de historiales y se eliminaron algunos que no eran necesario. Este cambio obliga a modificar las vistas de los investigadores sobre todo.
- [2016-08-12] `App/Sede`: se eliminó todo a lo referido a las sedes y se agregaron los mismo al modelo de Instituciones. De esta manera el codigo queda un poco mas limpio y entendible.
- [2016-08-12] `Database/Seeders`: se crearon los Seeders de LugarTrabajos y Roles. Se modificaron aquellos que generaban conflictos con claves foraneas.
- [2016-08-12] `Database/Migratios`: se modificaron todos los migrates para seguir con el modelo de datos planteado en la secretaría.
- [2016-08-11] `App/InvestigadorNotas`: se modificó el create y update de las notas. Se creó una nueva vista para ello.
- [2016-08-08] `Logs`: se instaló la libreria `https://github.com/rap2hpoutre/laravel-log-viewer` para llevar un registro de los logs.
- [2016-08-08] `Proyecto`: primera versión de las notas del investigador, se creó la migración, el modelo y el controlador.
- [2016-08-04] `Proyecto`: segunda versión del historial de Investigadores. Se modifico la BD y los campos guardan el valor y no el id. Ej: en campo de la sede, se guarda el nombre en lugar del id.
- [2016-08-04] `App\Http\Controllers\InvestigadorController`: se modifico el controlador para que permita adjuntar notas.(No se creo el modelo de Notas todavia).
- [2016-08-02] `App\Http\Middleware`: se creó un Middleware `Admin` para verificar el acceso a las páginas restringidas. Si dicho usuario no tiene acceso, lo redirige a una página de error 403.
- [2016-08-02] `Proyecto`: primera versión de la Seguridad. Utilicé, por ahora, la seguridad de Laravel por defecto agregando un atributo `type` en la tabla Users para definir los roles. Los roles son administrador y usuario final. Ambos necesitan loguearse para ingresar al sistema.
- [2016-06-30] `Proyecto`: primera versión del historial de Investigadores.
- [2016-06-28] `database\seeds`: se creó un seed para los Investigadores.
- [2016-06-27] `database\migrations`: se creó una migracion de base de datos para la tabla de historiales.
- [2016-06-27] `App\Models\Investigador`: se creó un evento que al modificar ciertas columnas guarde un historial en la tabla `historiales`.
- [2016-06-22] `Helpers`: busque información de como crear un archivo de Logs para los Investigadores a través de Laravel Events (y no por paquetes).
- [2016-06-21] `Helpers`: busque información con respecto a Datatables y cree un ejemplo guardado en `D://Dimax/sycadytt/sy-server`.

## v0.1.0 Sedes, Investigadores, Roles, Instituciones, LugarTrabajos
- [2016-06-14] `views\layouts`: se modificaron algunas vistas de la plantilla, tales como el menú de administrador.
- [2016-06-14] `Routes`: se agregaron todas las rutas al grupo del middleware `web`.
- [2016-06-09] `views\welcome`: se cambió la vista para que sea utilizada para el inicio de la aplicación.
- [2016-06-08] `App\Request\UpdateInvestigadorRequest`: se modificaron las reglas para que al momento de hacer un update valide si el dni existe, el legajo y el cuil.
- [2016-06-07] `App\Models\Investigador`: se modificó el modelo para que muestre correctamente las fechas.
- [2016-06-04] `App\Models\Investigador`: a través del paquete de InfyOm se creó el modelo, controlador, migración y vistas.
- [2016-06-03] `Rol`: se modificó el modelo, controlador, migración y vistas para que acepte claves foraneas.
- [2016-06-03] `App\Models\Rol`: a través del paquete de InfyOm se creó el modelo, controlador, migración y vistas.
- [2016-06-02] `App\Models\Institucion`: a través del paquete de InfyOm se creó el modelo, controlador, migración y vistas.
- [2016-06-02] `Proyecto`: se implementó las rutas para los administradores y para los invitados.
- [2016-06-01] `App\Models\LugarTrabajo`: a través del paquete de InfyOm se creó el modelo, controlador, migración y vistas.
- [2016-06-01] `Proyecto`: se modificó el las vistas del paquete InfyOm para que cumpla con las reglas de HTML5.
- [2016-06-01] `Proyecto`: se tradujeron todos los errores al español, copiando la carpeta y traduciendola desde `resources/lang/en`. Para mi fortuna ya existe alguien que la hizo, solo la baje y la edite para adaptarla lo mejor posible al proyecto.
- [2016-05-31] `App\Models\Sede`: se modificaron las vistas ya que el InfyOm no genera los form-group de altas y modificación.
- [2016-05-31] `App\Models\Sede`: a través del paquete de InfyOm se creó el modelo, controlador, migración y vistas.
- [2016-05-31] `Proyecto`: se intalaron las dependencias para poder hacer funcionar `InfyOm Laravel Generator` de acuerdo con la página oficial http://labs.infyom.com/laravelgenerator/ .
- [2016-05-30] `Proyecto`: puesta en marcha de laravel con el comando `laravel new` desde `Cmder`. Primer COMMIT.
