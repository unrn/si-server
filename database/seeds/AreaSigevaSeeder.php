<?php

use Illuminate\Database\Seeder;

class AreaSigevaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('area_sigevas')->delete();
			DB::table('area_sigevas')->insert([
				'nombre' => 'Arquitectura y Diseño'
			]);
			DB::table('area_sigevas')->insert([
				'nombre' => 'Artes'
			]);
			DB::table('area_sigevas')->insert([
				'nombre' => 'Cs. Biológicas de células y moléculas'
			]);
    }
}
