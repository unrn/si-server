<?php

use Illuminate\Database\Seeder;

class InvestigadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('investigadores')->delete();
			DB::table('investigadores')->insert([
				'legajo' => '1',
				'apellido' => 'Sanhueza',
				'nombre' => 'Matías Emanuel',
				'dni' => '34221016',
				'cuil' => '20342210164',
				'fecha_nac' => '1989-01-26',
				'sexo' => 'M',
				'email_personal' => 'manoloesanhueza@gmail.com',
				'email_institucional' => 'msanhueza@unrn.edu.ar',
				'especialidad_id' => '7'
			]);
    }
}
