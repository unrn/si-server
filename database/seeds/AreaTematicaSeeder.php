<?php

use Illuminate\Database\Seeder;

class AreaTematicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('area_tematicas')->delete();
			DB::table('area_tematicas')->insert([
				'nombre' => 'Cs. Exactas y Naturales'
			]);
			DB::table('area_tematicas')->insert([
				'nombre' => 'Tecnologías'
			]);
			DB::table('area_tematicas')->insert([
				'nombre' => 'Cs. Médicas'
			]);
			DB::table('area_tematicas')->insert([
				'nombre' => 'Cs. Agrícologanaderas'
			]);
			DB::table('area_tematicas')->insert([
				'nombre' => 'Cs. Sociales'
			]);
			DB::table('area_tematicas')->insert([
				'nombre' => 'Humanidades'
			]);
    }
}
