<?php

use Illuminate\Database\Seeder;

class EspecialidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('especialidades')->delete();
			DB::table('especialidades')->insert([
				'nombre' => 'Biología',
				'area_tematica_id' => '1'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Física',
				'area_tematica_id' => '1'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Geología',
				'area_tematica_id' => '1'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Matemática',
				'area_tematica_id' => '1'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Química',
				'area_tematica_id' => '1'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Otra',
				'area_tematica_id' => '1'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Arquitectura',
				'area_tematica_id' => '2'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Ingeniería',
				'area_tematica_id' => '2'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Otra',
				'area_tematica_id' => '2'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Bioquímica',
				'area_tematica_id' => '3'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Farmacéutica',
				'area_tematica_id' => '3'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Medicina',
				'area_tematica_id' => '3'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Otra',
				'area_tematica_id' => '3'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Ing. Agrónomica',
				'area_tematica_id' => '4'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Veterinaria',
				'area_tematica_id' => '4'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Otra',
				'area_tematica_id' => '4'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Abogacia',
				'area_tematica_id' => '5'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Antropología',
				'area_tematica_id' => '5'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Economía',
				'area_tematica_id' => '5'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Cs. de la Educación',
				'area_tematica_id' => '5'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Psicología',
				'area_tematica_id' => '5'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Sociología',
				'area_tematica_id' => '5'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Otra',
				'area_tematica_id' => '5'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Filosofía',
				'area_tematica_id' => '6'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Historia',
				'area_tematica_id' => '6'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Lingüística',
				'area_tematica_id' => '6'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Literatura',
				'area_tematica_id' => '6'
			]);
			DB::table('especialidades')->insert([
				'nombre' => 'Otra',
				'area_tematica_id' => '6'
			]);
    }
}
