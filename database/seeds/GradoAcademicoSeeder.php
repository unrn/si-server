<?php

use Illuminate\Database\Seeder;

class GradoAcademicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('grado_academicos')->delete();
			DB::table('grado_academicos')->insert([
				'nombre' => 'Universitario'
			]);
			DB::table('grado_academicos')->insert([
				'nombre' => 'Licenciado'
			]);
			DB::table('grado_academicos')->insert([
				'nombre' => 'Maestria'
			]);
			DB::table('grado_academicos')->insert([
				'nombre' => 'Doctorado'
			]);
			DB::table('grado_academicos')->insert([
				'nombre' => 'Otro'
			]);
    }
}
