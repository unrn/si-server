<?php

use Illuminate\Database\Seeder;

class LugarTrabajoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('lugar_trabajos')->delete();
			DB::table('lugar_trabajos')->insert([
				'nombre' => 'Laboratorio de Informática Aplicada',
				'abreviacion' => 'LIA',
				'institucion_id' => 1,
			]);
			DB::table('lugar_trabajos')->insert([
				'nombre' => 'Instituto De Investigación En Paleobiología Y Geología',
				'abreviacion' => 'IIPyG',
				'institucion_id' => 2,
			]);
			DB::table('lugar_trabajos')->insert([
				'nombre' => 'Instituto De Investigaciones En Diversidad Cultural Y Procesos De Cambio',
				'abreviacion' => 'IIDyPCA',
				'institucion_id' => 3,
			]);
			DB::table('lugar_trabajos')->insert([
				'nombre' => 'Sede Atlántica',
				'abreviacion' => 'ATL',
				'institucion_id' => 1,
			]);
			DB::table('lugar_trabajos')->insert([
				'nombre' => 'Sede Alto Valle y Valle Medio',
				'abreviacion' => 'AVyVM',
				'institucion_id' => 2,
			]);
			DB::table('lugar_trabajos')->insert([
				'nombre' => 'Sede Andina',
				'abreviacion' => 'AND',
				'institucion_id' => 3,
			]);
			DB::table('lugar_trabajos')->insert([
				'nombre' => 'Rectorado',
				'abreviacion' => 'REC',
				'institucion_id' => 4,
			]);
    }
}
