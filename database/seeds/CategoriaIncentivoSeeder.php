<?php

use Illuminate\Database\Seeder;

class CategoriaIncentivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('categoria_incentivos')->delete();
			DB::table('categoria_incentivos')->insert([
				'nombre' => 'I'
			]);
			DB::table('categoria_incentivos')->insert([
				'nombre' => 'II'
			]);
			DB::table('categoria_incentivos')->insert([
				'nombre' => 'III'
			]);
			DB::table('categoria_incentivos')->insert([
				'nombre' => 'IV'
			]);
			DB::table('categoria_incentivos')->insert([
				'nombre' => 'V'
			]);
			DB::table('categoria_incentivos')->insert([
				'nombre' => 'Vacia'
			]);
			DB::table('categoria_incentivos')->insert([
				'nombre' => 'No Tiene'
			]);
    }
}
