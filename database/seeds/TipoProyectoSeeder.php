<?php

use Illuminate\Database\Seeder;

class TipoProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('tipo_proyectos')->delete();
			DB::table('tipo_proyectos')->insert([
				'nombre' => 'Proyectos de Investigación',
				'abreviacion' => 'PI',
			]);
			DB::table('tipo_proyectos')->insert([
				'nombre' => 'Proyectos de Investigación Científica y Tecnológica',
				'abreviacion' => 'PICT',
			]);
			DB::table('tipo_proyectos')->insert([
				'nombre' => 'Proyectos de Investigación Científica y Tecnológica Orientados',
				'abreviacion' => 'PICTO',
			]);
			DB::table('tipo_proyectos')->insert([
				'nombre' => 'Proyectos de Desarrollo Tecnológico y Social',
				'abreviacion' => 'PDTS',
			]);
    }
}
