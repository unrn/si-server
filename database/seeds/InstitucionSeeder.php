<?php

use Illuminate\Database\Seeder;

class InstitucionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('instituciones')->delete();
			DB::table('instituciones')->insert([
				'nombre' => 'Atlántica',
				'abreviacion' => 'ATL',
			]);
			DB::table('instituciones')->insert([
				'nombre' => 'Alto Valle y Valle Medio',
				'abreviacion' => 'AVVM',
			]);
			DB::table('instituciones')->insert([
				'nombre' => 'Andina',
				'abreviacion' => 'AND',
			]);
			DB::table('instituciones')->insert([
				'nombre' => 'Rectorado',
				'abreviacion' => 'REC',
			]);
			DB::table('instituciones')->insert([
				'nombre' => 'Consejo Nacional de Investigaciones Científicas y Técnicas',
				'abreviacion' => 'CONICET',
			]);
    }
}
