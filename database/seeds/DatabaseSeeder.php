<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //         Eloquent::unguard();
        $this->call(InstitucionSeeder::class);
				$this->call(LugarTrabajoSeeder::class);
				$this->call(AreaTematicaSeeder::class);
				$this->call(EspecialidadSeeder::class);
				$this->call(InvestigadorSeeder::class);
				$this->call(UserSeeder::class);
				$this->call(RolSeeder::class);
				$this->call(CategoriaIncentivoSeeder::class);
				$this->call(GradoAcademicoSeeder::class);
				$this->call(PerfilSeeder::class);
				$this->call(TipoProyectoSeeder::class);
				$this->call(AreaSigevaSeeder::class);
				$this->call(ResolucionAltaSeeder::Class);
    }
}
