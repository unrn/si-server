<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('users')->delete();

			DB::table('users')->insert([
					'name' => 'Matías E. Sanhueza',
					'email' => 'manoloesanhueza@gmail.com',
					'password' => bcrypt('manolo'),
					'type' => true,
			]);
			DB::table('users')->insert([
					'name' => 'Gabriela A. Cayú',
					'email' => 'gcayu@unrn.edu.ar',
					'password' => bcrypt('manolo'),
					'type' => false,
			]);
    }
}
