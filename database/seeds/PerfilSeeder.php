<?php

use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('perfiles')->delete();
			DB::table('perfiles')->insert([
				'nombre' => 'Director'
			]);
			DB::table('perfiles')->insert([
				'nombre' => 'Co-Director'
			]);
			DB::table('perfiles')->insert([
				'nombre' => 'Resp. de Fondos'
			]);
			DB::table('perfiles')->insert([
				'nombre' => 'Investigador Interno'
			]);
			DB::table('perfiles')->insert([
				'nombre' => 'Investigador Externo'
			]);
			DB::table('perfiles')->insert([
				'nombre' => 'Integrante'
			]);
			DB::table('perfiles')->insert([
				'nombre' => 'Becario'
			]);
    }
}
