<?php

use Illuminate\Database\Seeder;

class ResolucionAltaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('resolucion_altas')->delete();
			DB::table('resolucion_altas')->insert([
				'numero' => 368,
				'fecha' => '2016-06-07',
				'fecha_inicio' => '2016-08-01',
				'fecha_fin_anual' => '2017-08-01',
				'fecha_presentacion_1' => '2017-09-01',
				'fecha_fin_bienal' => '2018-08-01',
				'fecha_presentacion_2' => '2018-09-03',
				'fecha_fin_trienal' => '2019-08-01',
				'fecha_presentacion_3' => '2019-09-02',
			]);
    }
}
