<?php

use Illuminate\Database\Seeder;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('roles')->delete();
			DB::table('roles')->insert([
				'nombre' => 'Personal de Apoyo'
			]);
			DB::table('roles')->insert([
				'nombre' => 'Personal Técnico'
			]);
			DB::table('roles')->insert([
				'nombre' => 'Estudiante'
			]);
			DB::table('roles')->insert([
				'nombre' => 'Concurrente'
			]);
			DB::table('roles')->insert([
				'nombre' => 'Becario Interno'
			]);
			DB::table('roles')->insert([
				'nombre' => 'Becario Externo'
			]);
			DB::table('roles')->insert([
				'nombre' => 'Investigador Interno'
			]);
			DB::table('roles')->insert([
				'nombre' => 'Investigador Externo'
			]);
    }
}
