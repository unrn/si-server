<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialAcademicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_academicos', function (Blueprint $table) {
            $table->increments('id');
						$table->date('fecha_inicio');
						$table->date('fecha_fin')->nullable();
						$table->string('user');
						$table->date('update');
						//Clave Foranea
						$table->integer('investigador_id')->unsigned()->nullable();
						$table->foreign('investigador_id')->references('id')->on('investigadores');
						$table->integer('grado_academico_id')->unsigned()->nullable();
						$table->foreign('grado_academico_id')->references('id')->on('grado_academicos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historial_academicos');
    }
}
