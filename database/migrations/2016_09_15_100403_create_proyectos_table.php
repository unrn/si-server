<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProyectosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->unique();
            $table->string('titulo', 300);
            $table->string('resumen', 2500)->nullable();
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->date('fecha_prorroga')->default(null)->nullable(); //es null si no hay prorroga
						$table->date('fecha_baja')->default(null)->nullable(); //es null si no hay baja
            $table->string('duracion');
            $table->string('convocatoria');
            $table->boolean('acreditado')->default(0);
            $table->string('tipo_investigacion');
            $table->double('monto', 8, 2);
						$table->double('ejecucion', 5, 2);
						//Clave Foranea
						$table->integer('area_tematica_id')->unsigned()->nullable();
						$table->foreign('area_tematica_id')->references('id')->on('area_tematicas');
						$table->integer('area_sigeva_id')->unsigned()->nullable();
						$table->foreign('area_sigeva_id')->references('id')->on('area_sigevas');
						$table->integer('institucion_id')->unsigned()->nullable();
						$table->foreign('institucion_id')->references('id')->on('instituciones');
						$table->integer('tipo_id')->unsigned()->nullable();
						$table->foreign('tipo_id')->references('id')->on('tipo_proyectos');
						$table->integer('resolucion_alta_id')->unsigned()->nullable();
						$table->foreign('resolucion_alta_id')->references('id')->on('resolucion_altas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyectos');
    }
}
