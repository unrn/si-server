<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrantes', function (Blueprint $table) {
            $table->increments('id');
						$table->date('fecha_alta');
						$table->date('fecha_baja')->nullable();
						$table->integer('horas');
						//Clave Foranea
            $table->integer('perfil_id')->unsigned()->nullable();
						$table->foreign('perfil_id')->references('id')->on('perfiles');
						$table->integer('proyecto_id')->unsigned()->nullable();
						$table->foreign('proyecto_id')->references('id')->on('proyectos');
						$table->integer('investigador_id')->unsigned()->nullable();
						$table->foreign('investigador_id')->references('id')->on('investigadores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('integrantes');
    }
}
