<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialIncentivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_incentivos', function (Blueprint $table) {
            $table->increments('id');
						$table->integer('fecha_inicio');
						$table->integer('fecha_fin')->nullable();
						$table->date('update');
						//Clave Foranea
						$table->integer('investigador_id')->unsigned()->nullable();
						$table->foreign('investigador_id')->references('id')->on('investigadores');
						$table->integer('cat_incentivo_id')->unsigned()->nullable();
						$table->foreign('cat_incentivo_id')->references('id')->on('categoria_incentivos');
            $table->integer('user_id')->unsigned()->nullable();
						$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historial_incentivos');
    }
}
