<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLugarTrabajosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lugar_trabajos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('abreviacion')->nullable();
						//Clave Foranea
						$table->integer('institucion_id')->unsigned()->nullable();
						$table->foreign('institucion_id')->references('id')->on('instituciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lugar_trabajos');
    }
}
