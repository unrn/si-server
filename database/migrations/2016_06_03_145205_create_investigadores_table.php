<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvestigadoresTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigadores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('legajo')->nullable();
            $table->string('apellido');
            $table->string('nombre');
            $table->char('dni', 10)->unique();
            $table->char('cuil', 11)->unique()->nullable();
            $table->date('fecha_nac')->nullable();
            $table->char('sexo', 1);
            $table->string('email_personal')->nullable();
            $table->string('email_institucional')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('investigadores');
    }
}
