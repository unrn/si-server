<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialLugarTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_lugar_trabajos', function (Blueprint $table) {
            $table->increments('id');
						$table->date('fecha_inicio');
						$table->date('fecha_fin')->nullable();
						$table->string('user');
						$table->date('update');
						//Clave Foranea
						$table->integer('investigador_id')->unsigned()->nullable();
						$table->foreign('investigador_id')->references('id')->on('investigadores');
						$table->integer('lugar_trabajo_id')->unsigned()->nullable();
						$table->foreign('lugar_trabajo_id')->references('id')->on('lugar_trabajos');
						$table->integer('rol_id')->unsigned()->nullable();
						$table->foreign('rol_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historial_lugar_trabajos');
    }
}
