<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInformeProyectosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informe_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->string('descripcion')->nullable();
            $table->date('fecha_presentacion');
            $table->date('fecha_entrega_papel')->nullable();
						$table->date('fecha_entrega_digital')->nullable();
						$table->date('fecha_evaluacion')->nullable();
						$table->boolean('satisfaccion')->default(NULL)->nullable();
						//Clave Foranea
						$table->integer('proyecto_id')->unsigned()->nullable();
						$table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informe_proyectos');
    }
}
