<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResolucionAltasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resolucion_altas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero');
            $table->date('fecha');
            $table->date('fecha_inicio');
            $table->date('fecha_fin_anual');
            $table->date('fecha_presentacion_1');
            $table->date('fecha_fin_bienal')->nullable();
            $table->date('fecha_presentacion_2')->nullable();
            $table->date('fecha_fin_trienal')->nullable();
            $table->date('fecha_presentacion_3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('resolucion_altas');
    }
}
