@extends('layouts.app')

@section('content')
<section class="content">
	<!-- <div class="row hold-transition register-page"> -->
	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Registrar Usuario</h3>
			</div>
			<div class="box-body">
				<form method="post" action="{{ url('/register') }}">
					{!! csrf_field() !!}
	        <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
						{!! Form::label('name', 'Nombre Completo (*):') !!}
						{!! Form::text('name', null, ['class' => 'form-control', 'value' => "{{ old('name') }}", 'required' => 'true', 'placeholder' => 'Nombre Completo']) !!}
	          <span class="glyphicon glyphicon-user form-control-feedback"></span>
	          @if ($errors->has('name'))
	            <span class="help-block">
	              <strong>{{ $errors->first('name') }}</strong>
	            </span>
	          @endif
	        </div>
	        <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
						{!! Form::label('email', 'Email (*):') !!}
						{!! Form::text('email', null, ['class' => 'form-control', 'value' => "{{ old('email') }}", 'required' => 'true', 'placeholder' => 'Email Institucional']) !!}
	          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	          @if ($errors->has('email'))
	            <span class="help-block">
	              <strong>{{ $errors->first('email') }}</strong>
	            </span>
	          @endif
	        </div>
	        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
						{!! Form::label('password', 'Password (*):') !!}
						<input type="password" name="password" class="form-control" placeholder="Password">
	          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	          @if ($errors->has('password'))
	            <span class="help-block">
	              <strong>{{ $errors->first('password') }}</strong>
	            </span>
	          @endif
	        </div>
	        <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						{!! Form::label('password_confirmation', 'Password (*):') !!}
						<input type="password" name="password_confirmation" class="form-control" placeholder="Confirmar password">
	          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	          @if ($errors->has('password_confirmation'))
	            <span class="help-block">
	              <strong>{{ $errors->first('password_confirmation') }}</strong>
	            </span>
	          @endif
	        </div>
					<div class="form-group has-feedback{{ $errors->has('type') ? ' has-error' : '' }}">
						{!! Form::label('type', 'Tipo de Usuario (*):') !!}
						{!! Form::select('type', ['0' => 'Usuario', '1' => 'Administrador'], null, ['class' => 'form-control', 'required' => 'true']) !!}
	        	@if ($errors->has('type'))
	          	<span class="help-block">
	            	<strong>{{ $errors->first('type') }}</strong>
	          	</span>
	        	@endif
	      	</div>
		      <div class="row">
		        <div class="col-xs-8">
		          <div class="checkbox icheck">
		            <label><input type="checkbox"> Acepto los <a href="#">términos y condiciones</a></label>
		          </div>
		        </div>
		        <!-- /.col -->
		        <div class="col-xs-4">
		          <button type="submit" class="btn btn-primary btn-block btn-flat">Registrar</button>
		        </div>
		        <!-- /.col -->
		      </div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<!-- iCheck -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
@endsection
