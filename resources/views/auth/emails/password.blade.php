Buen día,<br>
Haga click aquí para restablecer su contraseña: <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}" class="btn btn-primary"> {{$link}} </a><br>
Saludos,
