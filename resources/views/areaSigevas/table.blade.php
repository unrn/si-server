<table class="hover table table-bordered" id="areaSigevas-table">
    <thead>
				<th>ID</th>
        <th>Nombre</th>
        <th colspan="3">Acción</th>
    </thead>
    <tbody>
    @foreach($areaSigevas as $areaSigeva)
        <tr>
						<td>{!! $areaSigeva->id !!}</td>
            <td>{!! $areaSigeva->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['areaSigevas.destroy', $areaSigeva->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('areaSigevas.show', [$areaSigeva->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-search"></i></a>
                    <a href="{!! route('areaSigevas.edit', [$areaSigeva->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Desea eliminar el Área Sigeva?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $areaSigevas->links() !!}
</div>
