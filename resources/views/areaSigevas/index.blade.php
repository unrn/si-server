@extends('layouts.app')

@section('content')
  <section class="content-header">
		<h1>Áreas de SIGEVA</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ url('/areaSigevas') }}"> Áreas de SIGEVA</a></li>
			<li class="active">Listar</li>
		</ol>
  </section>
  <section class="content">
		<h1 class="pull-left">
			<a class="btn btn-block btn-social btn-primary" href="{!! route('areaSigevas.create') !!}">
				<i class="fa fa-plus-square"></i> Nuevo
			</a>
		</h1>
    <div class="clearfix"></div>
    @include('flash::message')
    <div class="clearfix"></div>
    <div class="box box-primary">
      <div class="box-body">
        @include('areaSigevas.table')
      </div>
    </div>
  </section>
@endsection
