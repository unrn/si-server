@extends('layouts.app')

@section('content')
    <section class="content-header">
			<h1>Áreas de SIGEVA</h1>
			<ol class="breadcrumb">
				<li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
				<li><a href="{{ url('/areaSigevas') }}"> Áreas de SIGEVA</a></li>
				<li class="active">Editar</li>
			</ol>
    </section>
    <section class="content">
      @include('adminlte-templates::common.errors')
      <div class="row">
      	{!! Form::model($areaSigeva, ['route' => ['areaSigevas.update', $areaSigeva->id], 'method' => 'patch']) !!}

            @include('areaSigevas.fields')

        {!! Form::close() !!}
      </div>
    </section>
@endsection
