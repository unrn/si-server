<div class="col-md-6">
	<div class="box box-primary">
		<div class="box-body">
			<!-- Nombre Field -->
			<div class="form-group">
			    {!! Form::label('nombre', 'Nombre (*):') !!}
			    {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<div class="form-group">
					{!! Form::label('area_tematica_id', 'Área Temática (*):') !!}
					{{ Form::select('area_tematica_id', $tematicas, null, ['class' => 'form-control', 'placeholder' => 'Seleecionar un Área', 'required' => 'true']) }}
			</div>
			<!-- Submit Field -->
			<div class="form-group">
			    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
			    <a href="{!! route('especialidades.index') !!}" class="btn btn-default">Cancelar</a>
			</div>
		</div>
	</div>
</div>
