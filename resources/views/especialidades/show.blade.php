@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Especialidad</h1>
				<ol class="breadcrumb">
			    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
			  	<li><a href="{{ url('/especialidades') }}"> Especialidades</a></li>
				  <li class="active">Mostrar</li>
				</ol>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('especialidades.show_fields')
                    <a href="{!! route('especialidades.index') !!}" class="btn btn-default">Volver</a>
                </div>
            </div>
        </div>
    </div>
@endsection
