@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Especialidades</h1>
				<ol class="breadcrumb">
		      <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
		      <li><a href="{{ url('/especialidades') }}"> Especialidades</a></li>
		      <li class="active">Listar</li>
		    </ol>
    </section>
    <section class="content">
      <div class="clearfix"></div>
      @include('flash::message')
      <div class="clearfix"></div>
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Listar</h3>
						</div>
						<div class="box-body">
							@include('especialidades.table')
						</div>
					</div>
				</div>
			</div>
    </section>
@endsection
