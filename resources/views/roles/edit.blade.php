@extends('layouts.app')

@section('content')
  <section class="content-header">
  	<h1>Roles</h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li><a href="{{ url('/roles') }}">Roles</a></li>
	    <li class="active">Editar</li>
	  </ol>
  </section>
  <section class="content">
  	@include('adminlte-templates::common.errors')
    <div class="row">
    	{!! Form::model($rol, ['route' => ['roles.update', $rol->id], 'method' => 'patch']) !!}

        		@include('roles.fields')

    	{!! Form::close() !!}
    </div>
  </section>
@endsection
