<div class="col-md-6">
	<div class="box box-primary">
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('nombre', 'Nombre (*):') !!}
				{{ Form::text('nombre', null, ['class' => 'form-control']) }}
			</div>
		</div>
		<!-- Submit Field -->
		<div class="box-footer">
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
			<a href="{!! route('roles.index') !!}" class="btn btn-default">Cancelar</a>
		</div>
	</div>
</div>
