<table class="hover table table-bordered" id="roles-table">
    <thead>
				<th>ID</th>
				<th>Nombre</th>
				<th colspan="3">Acción</th>
    </thead>
    <tbody>
    @foreach($roles as $rol)
        <tr>
						<td>{!! $rol->id !!}</td>
            <td>{!! $rol->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['roles.destroy', $rol->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('roles.show', [$rol->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-search"></i></a>
                    <a href="{!! route('roles.edit', [$rol->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Desea eliminar el Rol?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $roles->links() !!}
</div>
