@extends('layouts.app')

@section('content')
<section class="content">
	@include('adminlte-templates::common.errors')
	<div class="row">
		<img src="images/welcome.gif" alt="" class="center-block"/>
		<h2 class="text-center">Bienvenidos al sistema de Administración <br>de Proyectos de Investigación de la SICADyTT</h2>
	</div>
</section>
@endsection
