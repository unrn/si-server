<div class="form-group col-md-4">
		{!! Form::label('institucion', 'Sede:') !!}
		{!! Form::select('institucion', $instituciones, null, ['class' => 'form-control', 'placeholder' => 'Todas las sedes']) !!}
</div>
<div class="form-group col-md-4">
		{!! Form::label('area_tematica', 'Área Temática:') !!}
		{!! Form::select('area_tematica', $area_tematicas, null, ['class' => 'form-control', 'placeholder' => 'Todas las áreas']) !!}
</div>
<div class="form-group col-md-4">
		{!! Form::label('area_sigeva', 'Área SIGEVA:') !!}
		{!! Form::select('area_sigeva', $area_sigevas, null, ['class' => 'form-control', 'placeholder' => 'Todas las áreas']) !!}
</div>
<div class="form-group col-md-3">
	{!! Form::label('convocatoria', 'Convocatoria:') !!}
	{!! Form::select('convocatoria', ['2008' => '2008', '2009' => '2009', '2010' => '2010', '2011' => '2011', '2012' => '2012', '2013' => '2013', '2014' => '2014', '2015' => '2015', '2016' => '2016'], null, ['class' => 'form-control', 'placeholder' => 'Todas las convocatorias']) !!}
</div>
<div class="form-group col-md-3">
	{!! Form::label('duracion', 'Duracion:') !!}
	{!! Form::select('duracion', ['Anual' => 'Anual', 'Bienal' => 'Bienal', 'Trienal' => 'Trienal'], null, ['class' => 'form-control', 'placeholder' => 'Todas las duraciones']) !!}
</div>
<div class="form-group col-md-3">
	{!! Form::label('tipo_investigacion', 'Tipo Investigacion:') !!}
	{!! Form::select('tipo_investigacion', ['Aplicada' => 'Aplicada', 'Básica' => 'Básica', 'Desarrollo Experimental' => 'Desarrollo Experimental'], null, ['class' => 'form-control', 'placeholder' => 'Todos los tipos']) !!}
</div>
<div class="form-group col-md-3">
	{!! Form::label('tipo_proyecto', 'Tipo Proyecto:') !!}
	{!! Form::select('tipo_proyecto', $tipo_proyectos,null, ['class' => 'form-control', 'placeholder' => 'Todos los tipos']) !!}
</div>
<div class="form-group col-md-6">
	{!! Form::label('investigador_id', 'Director:') !!}
	{!! Form::text('term', null, ['class' => 'form-control', 'id' => 'term', 'placeholder' => 'Todos los directores']) !!}
	{!! Form::hidden('investigador_id') !!}
</div>
<div class="form-group col-md-6">
	{!! Form::label('activos', 'Proyectos Activos:') !!}
	<div class="input-group">
		<div class="input-group-btn">
			{!! Form::button('Desde', ['class' => 'btn', 'type' => 'button']) !!}
		</div>
		{!! Form::date('activo_desde', null, ['class' => 'form-control']) !!}
		<div class="input-group-btn">
			{!! Form::button('Hasta', ['class' => 'btn', 'type' => 'button']) !!}
		</div>
		{!! Form::date('activo_hasta', null, ['class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group col-md-12">
	{!! Form::submit('Buscar', ['class' => 'btn btn-primary']) !!}
</div>
@section('scripts')
<script type="text/javascript">
	$(document).ready(function (){
		$("#term").autocomplete({
	  	source: "/autocomplete/investigadores",
	  	minLength: 1,
	  	select: function(event, ui) {
				$('#term').val(ui.item.value);
				$('#investigador_id').val(ui.item.id);
	  	}
		});
		$("#term").keyup(function (){
			if ($("#term").val().length === 0) {
				$("#investigador_id").val(null);
			}
		});
	});
</script>
@endsection
