@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Reportes de Proyectos</h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li><a href="{{ url('/reportes') }}">Reportes</a></li>
	    <li class="active">Proyectos</li>
	  </ol>
  </section>
  <section class="content">
		<div class="clearfix"></div>
		@include('flash::message')
		<div class="clearfix"></div>
    @include('adminlte-templates::common.errors')
    <div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Filtros</h3>
			</div>
			<div class="box-body">
				{!! Form::open(['route' => 'reportes.proyectos.get', 'method' => 'GET']) !!}
					@include('reportes.proyectos.fields')
				{!! Form::close() !!}
			</div>
    </div>
  </section>
@endsection
