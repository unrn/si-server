@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1 class="pull-left">Reporte</h1>
		<ol class="breadcrumb">
		  <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
		  <li><a href="{{ url('/reportes') }}">Reporte</a></li>
		  <li class="active">Listar</li>
		</ol>
  </section>
  <section class="content">
		</br>
    <div class="clearfix"></div>
    @include('flash::message')
    <div class="clearfix"></div>
    <div class="box box-primary">
      <div class="box-body">
        @include('reportes.integrantes.table')
      </div>
    </div>
    <p class="text-center"><a href="{{ url()->previous() }}" class="btn btn-primary">Volver</a></p>
  </section>
@endsection
