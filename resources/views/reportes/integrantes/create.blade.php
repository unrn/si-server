@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Reportes Integrantes</h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li><a href="{{ url('/reportes') }}">Reportes</a></li>
	    <li class="active">Integrantes</li>
	  </ol>
  </section>
  <section class="content">
    @include('adminlte-templates::common.errors')
    <div class="box">
			<div class="box-header with-border">
			  <h3 class="box-title">Filtros</h3>
			</div>
			<div class="box-body">
				{!! Form::open(['route' => 'reportes.integrantes.get', 'method' => 'GET']) !!}
					@include('reportes.integrantes.fields')
				{!! Form::close() !!}
			</div>
    </div>
  </section>
@endsection
