<div class="form-group col-md-12">
	{!! Form::label('investigador_id', 'Investigador:') !!}
	{!! Form::text('term', null, ['class' => 'form-control', 'id' => 'term', 'placeholder' => 'Todos los integrantes']) !!}
	{!! Form::hidden('investigador_id') !!}
</div>
<div class="form-group col-sm-4">
	{!! Form::label('edad', 'Edad:') !!}
	<div class="input-group">
		<div class="input-group-btn">
			{!! Form::button('Desde', ['class' => 'btn', 'type' => 'button']) !!}
		</div>
		{!! Form::number('edad_min', null, ['class' => 'form-control', 'min' => '16']) !!}
		<div class="input-group-btn">
			{!! Form::button('Hasta', ['class' => 'btn', 'type' => 'button']) !!}
		</div>
		{!! Form::number('edad_max', null, ['class' => 'form-control', 'max' => '100']) !!}
	</div>
</div>
<div class="form-group col-md-4">
		{!! Form::label('sexo', 'Sexo:') !!}
		{!! Form::select('sexo', ['M' => 'Masculino', 'F' => 'Femenino', 'O' => 'Otro'], null, ['class' => 'form-control', 'placeholder' => 'Todos los géneros']) !!}
</div>
<div class="form-group col-md-4">
		{!! Form::label('area_tematica', 'Área Temática:') !!}
		{!! Form::select('area_tematica', $area_tematicas, null, ['class' => 'form-control', 'placeholder' => 'Todas las áreas']) !!}
</div>
<div class="form-group col-md-4">
		{!! Form::label('cat_incentivo', 'Categoria Incentivo:') !!}
		{!! Form::select('cat_incentivo', $categorias, null, ['class' => 'form-control', 'placeholder' => 'Todas las categorias']) !!}
</div>
<div class="form-group col-md-4">
		{!! Form::label('codigo', 'Proyecto:') !!}
		{!! Form::text('codigo', null, ['class' => 'form-control', 'placeholder' => 'Todos los proyectos']) !!}
</div>
<div class="form-group col-md-12">
	{!! Form::submit('Buscar', ['class' => 'btn btn-primary']) !!}
</div>
@section('scripts')
<script type="text/javascript">
	$(document).ready(function (){
		$("#term").autocomplete({
	  	source: "/autocomplete/investigadores",
	  	minLength: 1,
	  	select: function(event, ui) {
				$('#term').val(ui.item.value);
				$('#investigador_id').val(ui.item.id);
	  	}
		});
		$("#term").keyup(function (){
			if ($("#term").val().length === 0) {
				$("#investigador_id").val(null);
			}
		});
	});
</script>
@endsection
