{!! $dataTable->table(['width' => '100%', 'class' => 'hover table table-bordered']) !!}

@section('scripts')
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
		<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css">
    <script src="//cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
		<!-- Export Buttons -->
		<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
		<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
		<!-- Seleccionar Columnas -->
		<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js"></script>
		<!-- Seleccionar Filas -->
		<script src="//cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
    <!-- <script src="../../vendor/datatables/buttons.server-side.js"></script> -->
    {!! $dataTable->scripts() !!}
@endsection
