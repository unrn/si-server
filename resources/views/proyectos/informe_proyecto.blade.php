<!-- Proyecto Id Field -->
{{ Form::hidden('proyecto_id', $proyecto->id) }}
<div class="col-md-12">
	<div class="box">
		<div class="box-header with-border">
			<a href="{{ route('informeProyectos.index') }}" class='btn btn-warning pull-right' style="margin-left: 10px">
				<i class="fa fa-bars"></i>
			</a>
			<a href="{{ route('informeProyectos.create', ['proyecto_id' => $proyecto->id]) }}" class='btn btn-primary pull-right'>
					<i class="fa fa-plus-circle"></i>
					Agregar Informe
			</a>
		</div>
	</div>
</div>
@foreach($informes as $informe)
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Informe de {{$informe->tipo}}</h3>
			</div>
			<div class="box-body">
				<!-- Id Field -->
				{{ Form::hidden('informe_id[]', $informe->id) }}
				<!-- Tipo Field -->
				<div class="form-group col-sm-3">
			    {!! Form::label('tipo', 'Tipo:') !!}
					{!! Form::select('tipo[]', ['Avance' => 'Avance', 'Final' => 'Final', 'Pedido de Prorroga' => 'Pedido de Prorroga'], $informe->tipo, ['class' => 'form-control']) !!}
				</div>
				<!-- Fecha Presentacion Field -->
				<div class="form-group col-sm-3">
					{!! Form::label('fecha_presentacion', 'Fecha Presentacion:') !!}
					{!! Form::date('fecha_presentacion[]', $informe->fecha_presentacion, ['class' => 'form-control']) !!}
				</div>
				<!-- Fecha Entrega Field -->
				<div class="form-group col-sm-3">
					{!! Form::label('fecha_entrega_papel', 'Fecha Entrega Papel:') !!}
					{!! Form::date('fecha_entrega_papel[]', $informe->fecha_entrega_papel, ['class' => 'form-control']) !!}
				</div>
				<!-- Fecha Entrega Field -->
				<div class="form-group col-sm-3">
					{!! Form::label('fecha_entrega_digital', 'Fecha Entrega Digital:') !!}
					{!! Form::date('fecha_entrega_digital[]', $informe->fecha_entrega_digital, ['class' => 'form-control']) !!}
				</div>
				<!-- Descripcion Field -->
				<div class="form-group col-sm-12">
			    {!! Form::label('descripcion', 'Descripcion:') !!}
					{!! Form::textarea('descripcion[]', $informe->descripcion, ['class' => 'form-control', 'size' => '30x3']) !!}
				</div>
			</div>
	</div>
</div>
@endforeach
<!-- Submit Field -->
<div class="form-group col-sm-12">
	{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
	<a href="{!! route('informeProyectos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
