<div class="col-md-12">
	<div class="box" style="border: 2px solid grey">
    <div class="box-header">
      <h3 class="box-title">Informes del Proyecto</h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
      <table class="table table-striped">
        <tbody>
					<tr>
						<th>Informe</th>
						<th>Fecha de Presentación</th>
						<th>Entrega en Papel</th>
						<th>Entrega en Digital</th>
		        <th>Descripción</th>
          </tr>
					@foreach($informes as $informe)
            <tr>
							<td>{!! $informe->tipo !!}</td>
							<td>{!! $informe->fecha_presentacion !!}</td>
							<td>{!! $informe->fecha_entrega_papel !!}</td>
	            <td>{!! $informe->fecha_entrega_digital !!}</td>
							<td>{!! $informe->descripcion !!}</td>
            </tr>
					@endforeach
        </tbody>
			</table>
    </div><!-- /.box-body -->
  </div>
</div>
