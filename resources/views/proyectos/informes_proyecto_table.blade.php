<div class="col-md-12">
	<div class="box" style="border: 2px solid grey">
    <div class="box-header">
      <h3 class="box-title">Informes del Proyecto ({{sizeof($informes)}})</h3>
			@if(Auth::user()->type)
			<a href="{{ route('informeProyectos.create', ['proyecto_id' => $proyecto->id]) }}" class='btn btn-primary pull-right'>
					<i class="fa fa-user-plus"></i>
					Agregar Informe
			</a>
			@endif
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
      <table class="table table-striped">
        <tbody>
					<tr>
						<th>Estado</th>
						<th>Informe</th>
						<th>Fecha de Presentación</th>
						<th>Entrega en Papel</th>
						<th>Entrega en Digital</th>
		        <th>Descripción</th>
						<th colspan="3">Acción</th>
          </tr>
					@foreach($informes as $informe)
            <tr>
							@if($informe->fecha_entrega_papel > $informe->fecha_presentacion || ($informe->fecha_entrega == null && $informe->proyecto()->fecha_baja != null))
								<td><span class="badge bg-red"><i class="fa fa-thumbs-o-down"></i></span></td>
							@else
								<td><span class="badge bg-green"><i class="fa fa-thumbs-o-up"></i></span></td>
							@endif
							<td>{!! $informe->tipo !!}</td>
							<td>{!! $informe->fecha_presentacion !!}</td>
							<td>{!! $informe->fecha_entrega_papel !!}</td>
	            <td>{!! $informe->fecha_entrega_digital !!}</td>
							<td>{!! $informe->descripcion !!}</td>
							@if(Auth::user()->type)
								<td>
									{!! Form::open(['route' => ['informeProyectos.destroy', $informe->id], 'method' => 'delete']) !!}
									<div class='btn-group'>
									    <a href="{{ route('informeProyectos.edit', $informe->id) }}" class='btn btn-success btn-xs'>
									        <i class="fa fa-edit"></i>
									    </a>
									    {!! Form::button('<i class="fa fa-trash"></i>', [
									        'type' => 'submit',
									        'class' => 'btn btn-danger btn-xs',
									        'onclick' => "return confirm('¿Desea eliminar el informe de proyecto?')"
									    ]) !!}
									</div>
									{!! Form::close() !!}
								</td>
							@endif
            </tr>
					@endforeach
        </tbody>
			</table>
    </div><!-- /.box-body -->
  </div>
</div>
