@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Proyectos</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ url('/proyectos') }}">Proyectos</a></li>
      @if(Auth::user()->type)
        <li class="active">Editar</li>
      @else
        <li class="active">Listar</li>
      @endif
		</ol>
  </section>
  <section class="content">
		<div class="clearfix"></div>
		@include('flash::message')
		<div class="clearfix"></div>
    @include('adminlte-templates::common.errors')
    <div class="row">
			<div class="col-md-12">
			  <!-- Custom Tabs -->
			  <div class="nav-tabs-custom">
			  	<ul class="nav nav-tabs">
				    <li class="active">
							<a href="#tab_1" data-toggle="tab" aria-expanded="true">
								<span class="fa fa-folder" aria-hidden="true"></span> Datos Proyecto
							</a>
						</li>
					  <li class="">
							<a href="#tab_2" data-toggle="tab" aria-expanded="false">
								<span class="fa fa-users" aria-hidden="true"></span> Integrantes
							</a>
						</li>
            @if (Auth::user()->type)
            <li class="">
              <a href="#tab_3" data-toggle="tab" aria-expanded="false">
                <span class="fa fa-file-powerpoint-o" aria-hidden="true"></span> Informes
              </a>
            </li>
            @endif
					</ul>
					<div class="tab-content">
					  <div class="tab-pane active" id="tab_1">
							<div class="row">
								{!! Form::model($proyecto, ['route' => ['proyectos.update', $proyecto->id], 'method' => 'patch']) !!}
										@include('proyectos.fields')
								{!! Form::close() !!}
							</div>
					  </div><!-- /.tab-pane -->
					  <div class="tab-pane" id="tab_2">
							<div class="row">
								@include('proyectos.integrantes_proyecto_table')
							</div>
					  </div><!-- /.tab-pane -->
            @if (Auth::user()->type)
              <div class="tab-pane" id="tab_3">
                <div class="row">
                  {!! Form::open(['route' => ['informeProyectos.updateFrom', $proyecto->id], 'method' => 'put']) !!}
                    {{-- @include('proyectos.informe_proyecto') --}}
                    @include('proyectos.informes_proyecto_table')
                  {!! Form::close() !!}
                </div>
              </div><!-- /.tab-pane -->
            @endif
					</div><!-- /.tab-content -->
				</div><!-- nav-tabs-custom -->
			</div>
    </div>
  </section>
@endsection
