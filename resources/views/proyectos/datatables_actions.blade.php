<div class='btn-group'>
    <a href="{{ route('proyectos.show', $id) }}" class='btn btn-info btn-xs'>
        <i class="fa fa-search"></i>
    </a>
    @if (Auth::user()->type)
      <a href="{{ route('integrantes.create', ['proyecto_id' => $id]) }}" class='btn btn-primary btn-xs'>
          <i class="fa fa-user-plus"></i>
      </a>
      <a href="{{ route('proyectos.edit', $id) }}" class='btn btn-success btn-xs'>
          <i class="fa fa-edit"></i>
      </a>
    @endif
</div>
