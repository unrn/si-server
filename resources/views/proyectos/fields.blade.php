<div class="col-md-12 disabled-input">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proyecto</h3>
		</div>
		<div class="box-body">
			<!-- Codigo Field -->
			<div class="form-group col-sm-4">
			    {!! Form::label('codigo', 'Codigo (*):') !!}
			    {!! Form::text('codigo', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<!-- Tipo Id Field -->
			<div class="form-group col-sm-2">
					{!! Form::label('tipo_id', 'Tipo de Proyecto (*):') !!}
					{!! Form::select('tipo_id', $tipo_proyectos, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar Tipo','required' => 'true']) !!}
			</div>
			<!-- Relosucion Alta Id Field -->
			<div class="form-group col-sm-2">
					{!! Form::label('resolucion_alta_id', 'Resolución (*):') !!}
					{!! Form::hidden('resolucion_alta_id') !!}
					@if(strpos(url()->current(), 'create'))
						{!! Form::text('resolucion_alta', null, ['id' => 'resolucion_alta', 'class' => 'form-control', 'placeholder' => 'número/año', 'required' => 'true']) !!}
					@else
						{!! Form::text('resolucion_alta', $term, ['id' => 'resolucion_alta', 'class' => 'form-control', 'placeholder' => 'número/año', 'required' => 'true']) !!}
					@endif
			</div>
			<!-- Acreditado Field -->
			<div class="form-group col-sm-4">
				{!! Form::label('acreditado', 'Acreditado (*):') !!}
				<div class="input-group">
						<span class="input-group-addon">
							{{-- {!! Form::checkbox('acreditado', null, ['class' => 'form-control']) !!} --}}
							{!! Form::radio('acreditado', 1, ['class' => 'form-control']) !!} Si
						</span>
						<span class="input-group-addon">
							{{-- {!! Form::checkbox('acreditado', null, ['class' => 'form-control']) !!} --}}
							{!! Form::radio('acreditado', 0, ['class' => 'form-control']) !!} No
						</span>
      		<input type="text" class="form-control" value="¿El proyecto está acreditado?" disabled>
    		</div>
			</div>
			<!-- Titulo Field -->
			<div class="form-group col-sm-12">
			    {!! Form::label('titulo', 'Titulo (*):') !!}
			    {!! Form::text('titulo', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 disabled-input">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Datos del Proyecto</h3>
		</div>
		<div class="box-body">
			<!-- Fecha Field -->
			<div class="form-group col-sm-3">
			    {!! Form::label('fecha_inicio', 'Fecha Inicio (*):') !!}
			    {!! Form::date('fecha_inicio', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<!-- Fecha Fin Field -->
			<div class="form-group col-sm-3">
			    {!! Form::label('fecha_fin', 'Fecha Fin (*):') !!}
			    {!! Form::date('fecha_fin', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<!-- Fecha Prorroga Field -->
			<div class="form-group col-sm-3">
			    {!! Form::label('fecha_prorroga', 'Fecha Prorroga:') !!}
			    {!! Form::date('fecha_prorroga', null, ['class' => 'form-control']) !!}
			</div>
			<!-- Fecha Prorroga Field -->
			<div class="form-group col-sm-3">
			    {!! Form::label('fecha_baja', 'Fecha Baja:') !!}
			    {!! Form::date('fecha_baja', null, ['class' => 'form-control']) !!}
			</div>
			<!-- Duracion Field -->
			<div class="form-group col-sm-4">
			    {!! Form::label('duracion', 'Duracion (*):') !!}
					{!! Form::select('duracion', ['Anual' => 'Anual', 'Bienal' => 'Bienal', 'Trienal' => 'Trienal'], null, ['class' => 'form-control', 'placeholder' => 'Seleccionar duración', 'required' => 'true']) !!}
			</div>
			<!-- Convocatoria Field -->
			<div class="form-group col-sm-4">
			    {!! Form::label('convocatoria', 'Convocatoria (*):') !!}
			    {!! Form::select('convocatoria', ['2008' => '2008', '2009' => '2009', '2010' => '2010', '2011' => '2011', '2012' => '2012', '2013' => '2013', '2014' => '2014', '2015' => '2015', '2016' => '2016'], null, ['class' => 'form-control', 'placeholder' => 'Seleccionar Convocatoria']) !!}
			</div>
			<!-- Monto Field -->
			<div class="form-group col-sm-4">
					{!! Form::label('monto', 'Monto (*):') !!}
					<div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            {!! Form::number('monto', null, ['class' => 'form-control', 'step' => '0.01', 'required' => 'true']) !!}
          </div>
			</div>
			<!-- Institucion Id Field -->
			<div class="form-group col-sm-10">
			    {!! Form::label('institucion_id', 'Institucion (Unidad Ejecutora) (*):') !!}
			    {!! Form::select('institucion_id', $instituciones, null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<!-- Ejecucion Field -->
			<div class="form-group col-sm-2">
			    {!! Form::label('ejecucion', 'Ejecución (*):') !!}
					<div class="input-group">
            <span class="input-group-addon"><i class="fa fa-percent"></i></span>
            {!! Form::number('ejecucion', null, ['class' => 'form-control', 'step' => '0.01', 'min' => 0, 'max' => 100, 'required' => 'true']) !!}
          </div>
			</div>
			<!-- Resumen Field -->
			<div class="form-group col-sm-12">
					{!! Form::label('resumen', 'Resumen:') !!}
					{!! Form::textarea('resumen', null, ['class' => 'form-control', 'size' => '30x5']) !!}
			</div>
		</div>
	</div>
</div>
<div class="col-md-12 disabled-input">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Datos de Investigación</h3>
		</div>
		<div class="box-body">
			<!-- Tipo Investigacion Field -->
			<div class="form-group col-sm-3">
					{!! Form::label('tipo_investigacion', 'Tipo Investigacion (*):') !!}
					{!! Form::select('tipo_investigacion', ['Aplicada' => 'Aplicada', 'Básica' => 'Básica', 'Desarrollo Experimental' => 'Desarrollo Experimental'], null, ['class' => 'form-control','placeholder' => 'Seleccionar Investigación']) !!}
			</div>
			<!-- Area Tematica Id Field -->
			<div class="form-group col-sm-4">
			    {!! Form::label('area_tematica_id', 'Área Temática (*):') !!}
			    {!! Form::select('area_tematica_id', $tematicas, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar A. Temática', 'required' => 'true']) !!}
			</div>
			<!-- Area Sigeva Id Field -->
			<div class="form-group col-sm-5">
					{!! Form::label('area_sigeva_id', 'Área en SIGEVA (*):') !!}
					{!! Form::select('area_sigeva_id', $area_sigevas, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar A. de SIGEVA', 'required' => 'true']) !!}
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<!-- Submit Field -->
	<div class="box-footer" style="text-align:center;">
		{!! Form::submit('Guardar', ['class' => 'btn btn-primary', 'id' => 'submitButton']) !!}
		<a id="cancelButton" href="{!! route('proyectos.index') !!}" class="btn {{ Auth::user()->type ? 'btn-default' : 'btn-primary' }}">{{ Auth::user()->type ? 'Cancelar' : 'Volver' }}</a>
	</div>
</div>

@section('scripts')
<!-- Fijar como seguir -->
<script type="text/javascript">
	$(document).ready(function () {
		var resolucion = JSON.parse('{!! isset($proyecto) ? json_encode($proyecto->resolucionAlta()) : json_encode(NULL) !!}');
		var user = {!! json_encode(Auth::user()->type) !!};
		if (!user) {
			$(".disabled-input :input").attr("disabled", true);
			$("#submitButton").hide();
		}
		// $('#resolucion_alta').keyup(function (){
		// 	var route = '/resolucionAltas/search/' + $('#resolucion_alta').val();
		// 	$.get(route, function(response){
		// 		if(response.resol !== null) {
		// 			resolucion = response.resol;
		// 			alert(resolucion);
		// 			$('#resolucion_alta_id').val(resolucion.id);
		// 		} else {
		// 			resolucion = null;
		// 			$('#resolucion_alta_id').val(null);
		// 			$('#fecha_inicio').val(null);
		// 			$('#fecha_fin').val(null);
		// 		}
		// 	});
		// });
		$("#resolucion_alta").autocomplete({
	  	source: "/autocomplete/resolucionAltas",
	  	minLength: 1,
	  	select: function(event, ui) {
				$('#resolucion_alta').val(ui.item.numero);
				$('#resolucion_alta_id').val(ui.item.id);
				resolucion = ui.item.resolucion;
	  	}
		});
		$("#resolucion_alta").keyup(function (){
			if ($("#resolucion_alta").val().length === 0) {
				$("#resolucion_alta_id").val(null);
			}
		});
		$('#duracion').change(function () {
			if (resolucion) {
				$('#fecha_inicio').val(resolucion.fecha_inicio);
				switch ($('#duracion').val()) {
					case 'Anual':
						$('#fecha_fin').val(resolucion.fecha_fin_anual);
						break;
					case 'Bienal':
						$('#fecha_fin').val(resolucion.fecha_fin_bienal);
						break;
					case 'Trienal':
						$('#fecha_fin').val(resolucion.fecha_fin_trienal);
						break;
				}
			}
		});
	});
</script>
@endsection
