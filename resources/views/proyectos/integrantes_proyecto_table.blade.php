<div class="col-md-12">
	<div class="box" style="border: 2px solid grey">
    <div class="box-header">
      <h3 class="box-title">Listado de Integrantes del Proyecto ({{sizeof($integrantes)}})</h3>
			@if(Auth::user()->type)
			<a href="{{ route('integrantes.create', ['proyecto_id' => $proyecto->id]) }}" class='btn btn-primary pull-right'>
					<i class="fa fa-user-plus"></i>
					Agregar Integrante
			</a>
			@endif
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
      <table class="table table-striped">
        <tbody>
					<tr>
						<th>Estado</th>
						<th>Integrante</th>
						<th>Proyecto</th>
						<th>Perfil</th>
		        <th>Fecha Alta</th>
						<th>Fecha Baja</th>
						<th>Horas</th>
						<th colspan="3">Acción</th>
          </tr>
					@foreach($integrantes as $integrante)
            <tr>
							@if($integrante->fecha_baja < $integrante->proyecto()->fecha_fin)
								<td><span class="badge bg-red"><i class="fa fa-thumbs-o-down"></i></span></td>
							@else
								<td><span class="badge bg-green"><i class="fa fa-thumbs-o-up"></i></span></td>
							@endif
							<td>{!! $integrante->investigador()->fullName() !!}</td>
							<td>{!! $integrante->proyecto()->codigo !!}</td>
							<td>{!! $integrante->perfil()->nombre !!}</td>
	            <td>{!! Carbon\Carbon::parse($integrante->fecha_alta)->format('d-m-Y') !!}</td>
							<td>{!! Carbon\Carbon::parse($integrante->fecha_baja)->format('d-m-Y') !!}</td>
							<td>{!! $integrante->horas !!}</td>
							@if(Auth::user()->type)
								<td>
									{!! Form::open(['route' => ['integrantes.destroy', $integrante->id], 'method' => 'delete']) !!}
									<div class='btn-group'>
									    <a href="{{ route('integrantes.edit', $integrante->id) }}" class='btn btn-success btn-xs'>
									        <i class="fa fa-edit"></i>
									    </a>
									    {!! Form::button('<i class="fa fa-trash"></i>', [
									        'type' => 'submit',
									        'class' => 'btn btn-danger btn-xs',
									        'onclick' => "return confirm('¿Desea eliminar al integrante?')"
									    ]) !!}
									</div>
									{!! Form::close() !!}
								</td>
							@endif
            </tr>
					@endforeach
        </tbody>
			</table>
    </div><!-- /.box-body -->
  </div>
</div>
