<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proyecto</h3>
		</div>
		<div class="box-body">
			<!-- Codigo Field -->
			<div class="form-group col-sm-4">
			    {!! Form::label('codigo', 'Codigo (*):') !!}
			    {!! Form::text('codigo', $proyecto->codigo, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Tipo Id Field -->
			<div class="form-group col-sm-2">
					{!! Form::label('tipo_id', 'Tipo de Proyecto (*):') !!}
					{!! Form::text('tipo_id', $proyecto->tipoProyecto()->abreviacion, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Relosucion Alta Id Field -->
			<div class="form-group col-sm-2">
					{!! Form::label('resolucion_alta_id', 'Resolución (*):') !!}
					{!! Form::text('resolucion_alta', $proyecto->resolucionAlta()->numero, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Acreditado Field -->
			<div class="form-group col-sm-4">
				{!! Form::label('acreditado', '¿El proyecto está acreditado?:') !!}
				{!! Form::text('acreditado', $proyecto->acreditado == 0 ? 'NO' : 'SI', ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Titulo Field -->
			<div class="form-group col-sm-12">
			    {!! Form::label('titulo', 'Titulo (*):') !!}
			    {!! Form::text('titulo', $proyecto->titulo, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Datos del Proyecto</h3>
		</div>
		<div class="box-body">
			<!-- Fecha Field -->
			<div class="form-group col-sm-3">
			    {!! Form::label('fecha_inicio', 'Fecha Inicio (*):') !!}
			    {!! Form::date('fecha_inicio', $proyecto->fecha_inicio, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Fecha Fin Field -->
			<div class="form-group col-sm-3">
			    {!! Form::label('fecha_fin', 'Fecha Fin (*):') !!}
			    {!! Form::date('fecha_fin', $proyecto->fecha_fin, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Fecha Prorroga Field -->
			<div class="form-group col-sm-3">
			    {!! Form::label('fecha_prorroga', 'Fecha Prorroga:') !!}
			    {!! Form::date('fecha_prorroga', $proyecto->fecha_prorroga, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Fecha Prorroga Field -->
			<div class="form-group col-sm-3">
			    {!! Form::label('fecha_baja', 'Fecha Baja:') !!}
			    {!! Form::date('fecha_baja', $proyecto->fecha_baja, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Duracion Field -->
			<div class="form-group col-sm-4">
			    {!! Form::label('duracion', 'Duracion (*):') !!}
					{!! Form::text('duracion', $proyecto->duracion,['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Convocatoria Field -->
			<div class="form-group col-sm-4">
			    {!! Form::label('convocatoria', 'Convocatoria (*):') !!}
			    {!! Form::text('convocatoria', $proyecto->convocatoria, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Monto Field -->
			<div class="form-group col-sm-4">
					{!! Form::label('monto', 'Monto (*):') !!}
					<div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
            {!! Form::text('monto', $proyecto->monto, ['class' => 'form-control', 'disabled' => 'true']) !!}
          </div>
			</div>
			<!-- Institucion Id Field -->
			<div class="form-group col-sm-10">
			    {!! Form::label('institucion_id', 'Institucion (Unidad Ejecutora) (*):') !!}
			    {!! Form::text('institucion_id', $proyecto->institucion()->nombre, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Ejecucion Field -->
			<div class="form-group col-sm-2">
			    {!! Form::label('ejecucion', 'Ejecución (*):') !!}
					<div class="input-group">
            <span class="input-group-addon"><i class="fa fa-percent"></i></span>
            {!! Form::text('ejecucion', $proyecto->ejecucion, ['class' => 'form-control', 'disabled' => 'true']) !!}
          </div>
			</div>
			<!-- Resumen Field -->
			<div class="form-group col-sm-12">
					{!! Form::label('resumen', 'Resumen:') !!}
					{!! Form::textarea('resumen', $proyecto->resumen, ['class' => 'form-control', 'size' => '30x5', 'disabled' => 'true']) !!}
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Datos de Investigación</h3>
		</div>
		<div class="box-body">
			<!-- Tipo Investigacion Field -->
			<div class="form-group col-sm-3">
					{!! Form::label('tipo_investigacion', 'Tipo Investigacion (*):') !!}
					{!! Form::text('tipo_investigacion', $proyecto->tipo_investigacion, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Area Tematica Id Field -->
			<div class="form-group col-sm-4">
			    {!! Form::label('area_tematica_id', 'Área Temática (*):') !!}
			    {!! Form::text('area_tematica_id', $proyecto->areaTematica()->nombre, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
			<!-- Area Sigeva Id Field -->
			<div class="form-group col-sm-5">
					{!! Form::label('area_sigeva_id', 'Área en SIGEVA (*):') !!}
					{!! Form::text('area_sigeva_id', $proyecto->areaSigeva()->nombre, ['class' => 'form-control', 'disabled' => 'true']) !!}
			</div>
		</div>
	</div>
</div>
