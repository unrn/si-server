<div class="col-md-12">
	<div class="box" style="border: 2px solid grey">
    <div class="box-header">
      <h3 class="box-title">Proyectos asociados ({{sizeof($integrantes)}})</h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
      <table class="table table-striped">
        <tbody>
					<tr>
						<th>Estado</th>
						<th>Integrante</th>
						<th>Proyecto</th>
						<th>Perfil</th>
		        <th>Fecha Alta</th>
						<th>Fecha Baja</th>
						<th>Horas</th>
          </tr>
					@foreach($integrantes as $integrante)
            <tr>
							@if($integrante->fecha_baja < $integrante->proyecto()->fecha_fin)
								<td><span class="badge bg-red"><i class="fa fa-thumbs-o-down"></i></span></td>
							@else
								<td><span class="badge bg-green"><i class="fa fa-thumbs-o-up"></i></span></td>
							@endif
							<td>{!! $integrante->investigador()->fullName() !!}</td>
							<td>
								@if(Auth::user()->type)
									<a href="{!! route('proyectos.edit', [$integrante->proyecto()->id]) !!}" class='btn btn-warning btn-xs' title="Ir al Proyecto">{!! $integrante->proyecto()->codigo !!}</a>
								@else
									<a href="{!! route('proyectos.show', [$integrante->proyecto()->id]) !!}" class='btn btn-warning btn-xs' title="Ir al Proyecto">{!! $integrante->proyecto()->codigo !!}</a>
								@endif
							</td>
							<td>{!! $integrante->perfil()->nombre !!}</td>
	            <td>{!! Carbon\Carbon::parse($integrante->fecha_alta)->format('d-m-Y') !!}</td>
							<td>{!! Carbon\Carbon::parse($integrante->fecha_baja)->format('d-m-Y') !!}</td>
							<td>{!! $integrante->horas !!}</td>
            </tr>
					@endforeach
        </tbody>
			</table>
    </div><!-- /.box-body -->
  </div>
</div>
