<div class="btn-group">
	<a href="{{ route('investigadores.show', $id) }}" class="btn btn-info btn-xs"><i class="fa fa-search"></i></a>
	@if (Auth::user()->type)
		<a href="{{ route('investigadores.historial', $id) }}" class="btn btn-default btn-xs"><i class="fa fa-history"></i></a>
		<a href="{{ route('investigadores.edit', $id) }}" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
	@endif
</div>
