@extends('layouts.app')

@section('content')
  <section class="content-header">
  	<h1>
    	Historial de: {{$investigador->apellido}}, {{$investigador->nombre}}
			<a href="{!! route('investigadores.edit', [$investigador->id]) !!}" class='btn btn-success'><i class="fa fa-hand-o-left"></i> Volver</a>
    </h1>

  </section>
  <section class="content">
  	@include('adminlte-templates::common.errors')
		@include('flash::message')
    <div class="row">
			<div class="col-md-12">
			  <!-- Custom Tabs -->
			  <div class="nav-tabs-custom">
			  	<ul class="nav nav-tabs">
				    <li class="active">
							<a href="#tab_1" data-toggle="tab" aria-expanded="true">
								<span class="fa fa-history" aria-hidden="true">
								<i class="fa fa-graduation-cap"></i> Historial Académico
							</a>
						</li>
				    <li class="">
							<a href="#tab_2" data-toggle="tab" aria-expanded="false">
								<span class="fa fa-history" aria-hidden="true"></span>
								<i class="fa fa-pencil-square-o"></i> Historial Incentivos
							</a>
						</li>
				    <li class="">
							<a href="#tab_3" data-toggle="tab" aria-expanded="false">
								<span class="fa fa-history" aria-hidden="true"></span>
								<i class="fa fa-file-image-o"></i> Historial Lugar de Trabajos
							</a>
						</li>
				    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
				  </ul>
				  <div class="tab-content">
				    <div class="tab-pane active" id="tab_1">
							<div class="row">
								@include('investigadores.historial.academico.full_view')
							</div>
				    </div><!-- /.tab-pane -->
				    <div class="tab-pane" id="tab_2">
							<div class="row">
								@include('investigadores.historial.incentivo.full_view')
							</div>
				    </div><!-- /.tab-pane -->
				    <div class="tab-pane" id="tab_3">
							<div class="row">
								@include('investigadores.historial.lugarTrabajo.full_view')
							</div>
				    </div><!-- /.tab-pane -->
				  </div><!-- /.tab-content -->
				</div><!-- nav-tabs-custom -->
			</div>
    </div>
  </section>
@endsection
