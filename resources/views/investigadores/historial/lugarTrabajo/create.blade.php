<div class="box-body">
	{{ Form::hidden('investigador_id', $investigador->id) }}
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('lugar_trabajo_id', 'Lugar de Trabajo:') !!}
			{{ Form::select('lugar_trabajo_id', $lugartrabajos, null, ['class' => 'form-control', 'required' => 'true']) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('rol_id', 'Rol') !!}
			{{ Form::select('rol_id', $roles, null, ['class' => 'form-control', 'required' => 'true']) }}
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			{!! Form::label('fecha_inicio_lugartrabajo', 'Fecha Inicio:') !!}
			{{ Form::date('fecha_inicio_lugartrabajo', null, ['class' => 'form-control', 'required' => 'true']) }}
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			{!! Form::label('fecha_fin_lugartrabajo', 'Fecha Fin:') !!}
			{{ Form::date('fecha_fin_lugartrabajo', null, ['class' => 'form-control']) }}
		</div>
	</div>
	<div class="col-md-2">
		<div class="form-group">
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
		</div>
	</div>
</div>
