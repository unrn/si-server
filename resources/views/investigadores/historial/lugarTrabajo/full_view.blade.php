<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Nuevo</h3>
		</div>
		{!! Form::open(['route' => 'historialLugarTrabajos.store']) !!}
      @include('investigadores.historial.lugarTrabajo.create')
    {!! Form::close() !!}
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Listado</h3>
		</div>
		<div class="box-body">
			@include('investigadores.historial.lugarTrabajo.table')
		</div>
	</div>
</div>
