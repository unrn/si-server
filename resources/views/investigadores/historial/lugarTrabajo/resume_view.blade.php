<div class="col-md-6">
	<div class="box" style="border: 2px solid grey">
    <div class="box-header">
      <h3 class="box-title">Historial Lugares de Trabajos</h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
      <table class="table table-striped">
        <tbody>
					<tr>
						<th>Lugar de Trabajo</th>
						<th>Rol</th>
						<th>Fecha Inicio</th>
		        <th>Fecha Fin</th>
          </tr>
					@foreach($historial_lugartrabajos as $historia)
            <tr>
							<td>{!! $historia->lugarTrabajo()->nombre !!}</td>
							<td>{!! $historia->rol()->nombre !!}</td>
							<td>{!! $historia->fecha_inicio !!}</td>
	            <td>{!! $historia->fecha_fin !!}</td>
            </tr>
					@endforeach
        </tbody>
			</table>
    </div><!-- /.box-body -->
  </div>
</div>
