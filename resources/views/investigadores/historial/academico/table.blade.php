<table class="table table-responsive table-bordered" id="historialAcademicos-table">
    <thead>
			<th>Grado Académico</th>
      <th>Fecha Inicio</th>
      <th>Fecha Fin</th>
			<th>Usuario</th>
			<th>Modificado el</th>
      <th colspan="3">Acción</th>
    </thead>
    <tbody>
    @foreach($historial_academicos as $historia)
      <tr>
				<td>{!! $historia->gradoAcademico()->nombre !!}</td>
        <td>{!! $historia->fecha_inicio !!}</td>
        <td>{!! $historia->fecha_fin !!}</td>
				<td>{!! $historia->user !!}</td>
				<td>{!! $historia->update !!}</td>
        <td>
        	{!! Form::open(['route' => ['historialAcademicos.destroy', $historia->id], 'method' => 'delete']) !!}
          <div class='btn-group'>
            {!! Form::button('<i class="fa fa-trash-o"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
          </div>
          {!! Form::close() !!}
        </td>
      </tr>
    @endforeach
    </tbody>
</table>
