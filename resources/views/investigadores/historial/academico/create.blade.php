<div class="box-body">
	{{ Form::hidden('investigador_id', $investigador->id) }}
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('grado_academico', 'Grado Académico:') !!}
			{{ Form::select('grado_academico_id', $grados, null, ['class' => 'form-control', 'required' => 'true']) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('fecha_inicio_academico', 'Fecha Inicio:') !!}
			{{ Form::date('fecha_inicio_academico', null, ['class' => 'form-control', 'required' => 'true']) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('fecha_fin_academico', 'Fecha Fin:') !!}
			{{ Form::date('fecha_fin_academico', null, ['class' => 'form-control']) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
		</div>
	</div>
</div>
