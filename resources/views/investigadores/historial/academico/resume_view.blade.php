<div class="col-md-6">
	<div class="box" style="border: 2px solid grey">
    <div class="box-header">
      <h3 class="box-title">Historial Académico</h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
      <table class="table table-striped">
        <tbody>
					<tr>
						<th>Grado Académico</th>
						<th>Fecha Inicio</th>
		        <th>Fecha Fin</th>
          </tr>
					@foreach($historial_academicos as $historia)
            <tr>
							<td>{!! $historia->gradoAcademico()->nombre !!}</td>
							<td>{!! $historia->fecha_inicio !!}</td>
	            <td>{!! $historia->fecha_fin !!}</td>
            </tr>
					@endforeach
        </tbody>
			</table>
    </div><!-- /.box-body -->
  </div>
</div>
