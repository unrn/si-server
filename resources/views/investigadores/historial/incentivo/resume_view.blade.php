<div class="col-md-6">
	<div class="box" style="border: 2px solid grey">
    <div class="box-header">
      <h3 class="box-title">Historial Categoria Incentivos</h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
      <table class="table table-striped">
        <tbody>
					<tr>
						<th>Categoria Incentivo</th>
						<th>Fecha Inicio</th>
		        <th>Fecha Fin</th>
          </tr>
					@foreach($historial_incentivos as $historia)
            <tr>
							<td>{!! $historia->categoriaIncentivo()->nombre !!}</td>
							<td>{!! $historia->fecha_inicio !!}</td>
	            <td>{!! $historia->fecha_fin !!}</td>
            </tr>
					@endforeach
        </tbody>
			</table>
    </div><!-- /.box-body -->
  </div>
</div>
