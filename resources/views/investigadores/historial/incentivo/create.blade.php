<div class="box-body">
	{{ Form::hidden('investigador_id', $investigador->id) }}
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('cat_incentivo', 'Categoria Incentivo:') !!}
			{{ Form::select('cat_incentivo_id', $categorias, null, ['class' => 'form-control', 'required' => 'true']) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('fecha_inicio_incentivo', 'Año Inicio:') !!}
			{{ Form::number('fecha_inicio_incentivo', null, ['class' => 'form-control', 'required' => 'true', 'pattern' => '[0-9]', 'min' => 2000]) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::label('fecha_fin_incentivo', 'Año Fin:') !!}
			{{ Form::number('fecha_fin_incentivo', null, ['class' => 'form-control', 'pattern' => '[0-9]', 'min' => 2000]) }}
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
		</div>
	</div>
</div>
