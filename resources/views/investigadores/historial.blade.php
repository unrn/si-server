<section class="content">
	<div class="row">
			@include('investigadores.historial.academico.resume_view')
			@include('investigadores.historial.incentivo.resume_view')
			@include('investigadores.historial.lugarTrabajo.resume_view')
			<div class="row text-center">
				<a href="{{ route('investigadores.historial', ['id' => $investigador->id]) }}" class="btn btn-info"><i class="fa fa-info-circle"></i> Ampliar Historial</a>
			</div>
	</div>
</section>
