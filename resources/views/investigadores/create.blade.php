@extends('layouts.app')

@section('content')
 	<section class="content-header">
    <h1>Investigador</h1>
		<ol class="breadcrumb">
      <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="{{ url('/investigadores') }}">Investigadores</a></li>
      <li class="active">Crear</li>
    </ol>
  </section>
  <section class="content">
  	@include('adminlte-templates::common.errors')
    <div class="row">
      {!! Form::open(['route' => 'investigadores.store']) !!}
          @include('investigadores.fields')
      {!! Form::close() !!}
    </div>
  </section>
@endsection
