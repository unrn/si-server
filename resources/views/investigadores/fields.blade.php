	<div class="col-md-12 disabled-input">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Datos Personales</h3>
			</div>
			<div class="box-body">
				<div class="form-group col-md-6">
					{!! Form::label('apellido', 'Apellido (*):') !!}
					{{ Form::text('apellido', null, ['class' => 'form-control', 'pattern' => '^[a-zA-Z ñáéíóúÑÁÉÍÓÚÜü]*$', 'required' => 'true']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('nombre', 'Nombre (*):') !!}
					{{ Form::text('nombre', null, ['class' => 'form-control', 'pattern' => '^[a-zA-Z ñáéíóúÑÁÉÍÓÚÜü]*$', 'required' => 'true']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('dni', 'Numero de Documento (*):') !!}
					{{ Form::text('dni', null, ['class' => 'form-control', 'required' => 'true']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('cuil', 'CUIL/CUIT (*):') !!}
					{{ Form::text('cuil', null, ['class' => 'form-control', 'required' => 'true']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('fecha_nac', 'Fecha Nacimiento (*):') !!}
					{{ Form::date('fecha_nac', null, ['class' => 'form-control', 'required' => 'true']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('especialidad_id', 'Especialidad (*):') !!}
					{{ Form::select('especialidad_id', $especialidades, null, ['class' => 'form-control','placeholder' => 'Sin asignar']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('sexo', 'Sexo (*):') !!}
					{{ Form::select('sexo', ['M' => 'Masculino', 'F' => 'Femenino', 'O' => 'Otro'], null, ['class' => 'form-control', 'placeholder' => 'Seleccionar sexo', 'required' => 'true']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('email_personal', 'Email Personal:') !!}
					{{ Form::email('email_personal', null, ['class' => 'form-control', 'placeholder' => 'example@gmail.com']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('email_institucional', 'Email Institucional:') !!}
					{{ Form::email('email_institucional', null, ['class' => 'form-control', 'placeholder' => 'example@unrn.edu.ar']) }}
				</div>
				<div class="form-group col-md-6">
					{!! Form::label('legajo', 'Legajo (*):') !!}
					{{ Form::number('legajo', null, ['class' => 'form-control']) }}
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 disabled-input">
		<div class="box box-warning">
			<div class="box-header with-border">
	    	<h3 class="box-title">Datos Laborales</h3>
	  	</div>
			<div class="box-body">
				<div class="col-md-12">
					<div class="box">
						<div class="box-body">
							<div class="form-group col-md-4">
							  {!! Form::label('cat_incentivo', 'Categoría Incentivo:') !!}
								{{ Form::select('cat_incentivo_id', $categorias, isset($incentivo_actual->cat_incentivo_id) == false ? null : $incentivo_actual->cat_incentivo_id, ['class' => 'form-control','placeholder' => 'Seleccionar Categoría']) }}
								{{ Form::hidden('historial_incentivo_id', isset($incentivo_actual->id) == false ? null : $incentivo_actual->id) }}
							</div>
							<div class="form-group col-md-4">
								{!! Form::label('fecha_inicio_incentivo', 'Fecha Inicio:') !!}
								{{ Form::number('fecha_inicio_incentivo', isset($incentivo_actual->fecha_inicio) == false ? null : $incentivo_actual->fecha_inicio, ['class' => 'form-control', 'pattern' => '[0-9]', 'min' => 2000]) }}
							</div>
							<div class="form-group col-md-4">
								{!! Form::label('fecha_fin_incentivo', 'Fecha Fin:') !!}
								{{ Form::number('fecha_fin_incentivo', isset($incentivo_actual->fecha_fin) == false ? null : $incentivo_actual->fecha_fin, ['class' => 'form-control', 'pattern' => '[0-9]', 'min' => 2000]) }}
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="box">
						<div class="box-body">
							<div class="form-group col-md-4">
							  {!! Form::label('grado_academico', 'Grado Académico:') !!}
								{{ Form::select('grado_academico_id', $grados, isset($academico_actual->grado_academico_id) == false ? null : $academico_actual->grado_academico_id, ['class' => 'form-control', 'placeholder' => 'Seleccionar Grado Académico']) }}
								{{ Form::hidden('historial_academico_id', isset($academico_actual->id) == false ? null : $academico_actual->id) }}
							</div>
							<div class="form-group col-md-4">
								{!! Form::label('fecha_inicio_academico', 'Año Inicio:') !!}
								{{ Form::date('fecha_inicio_academico', isset($academico_actual->fecha_inicio) == false ? null : $academico_actual->fecha_inicio, ['class' => 'form-control']) }}
							</div>
							<div class="form-group col-md-4">
								{!! Form::label('fecha_fin_academico', 'Año Fin:') !!}
								{{ Form::date('fecha_fin_academico', isset($academico_actual->fecha_fin) == false ? null : $academico_actual->fecha_fin, ['class' => 'form-control']) }}
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="box">
						<div class="box-body">
							<div class="form-group col-md-3">
								{!! Form::label('lugar_trabajo_id', 'Lugar de Trabajo:') !!}
								{{ Form::select('lugar_trabajo_id', $lugartrabajos, isset($lugartrabajo_actual->lugar_trabajo_id) == false ? null : $lugartrabajo_actual->lugar_trabajo_id, ['class' => 'form-control', 'placeholder' => 'Seleccionar Lugar de Trabajo']) }}
								{{ Form::hidden('historial_lugartrabajo_id', isset($lugartrabajo_actual->id) == false ? null : $lugartrabajo_actual->id) }}
							</div>
							<div class="form-group col-md-3">
								{!! Form::label('rol_id', 'Rol:') !!}
								{{ Form::select('rol_id', $roles, isset($lugartrabajo_actual) == false ? null : $lugartrabajo_actual->rol()->id, ['class' => 'form-control', 'placeholder' => 'Seleccionar Rol']) }}
							</div>
							<div class="form-group col-md-3">
								{!! Form::label('fecha_inicio_lugartrabajo', 'Fecha Inicio:') !!}
								{{ Form::date('fecha_inicio_lugartrabajo', isset($lugartrabajo_actual->fecha_inicio) == false ? null : $lugartrabajo_actual->fecha_inicio, ['class' => 'form-control']) }}
							</div>
							<div class="form-group col-md-3">
								{!! Form::label('fecha_fin_lugartrabajo', 'Fecha Fin:') !!}
								{{ Form::date('fecha_fin_lugartrabajo', isset($lugartrabajo_actual->fecha_fin) == false ? null : $lugartrabajo_actual->fecha_fin, ['class' => 'form-control']) }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<!-- Submit Field -->
		<div class="box-footer" style="text-align: center;">
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary', 'id' => 'submitButton']) !!}
			<a id="cancelButton" href="{!! route('investigadores.index') !!}" class="btn {{ Auth::user()->type ? 'btn-default' : 'btn-primary' }}">{{ Auth::user()->type ? 'Cancelar' : 'Volver' }}</a>
		</div>
	</div>
@section('scripts')
<script type="text/javascript">
	$(document).ready(function (){
		var user = {!! json_encode(Auth::user()->type) !!};
		if (!user) {
			$(".disabled-input :input").attr("disabled", true);
			$("#submitButton").hide();
		}
	});
</script>
@endsection
