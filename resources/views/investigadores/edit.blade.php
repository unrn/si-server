@extends('layouts.app')

@section('content')
  <section class="content-header">
  	<h1>Investigador</h1>
		<ol class="breadcrumb">
      <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="{{ url('/investigadores') }}">Investigadores</a></li>
      @if(Auth::user()->type)
        <li class="active">Editar</li>
      @else
        <li class="active">Listar</li>
      @endif
    </ol>
  </section>
  <section class="content">
  	@include('adminlte-templates::common.errors')
		@include('flash::message')
		{{--
		<!-- <div class="alert {{ $investigador->isExtern() == 1 ? 'alert-error' : 'alert-success'}}">
			El investigador es <strong>{{ $investigador->isExtern() == 1 ? 'Externo' : 'Interno'  }}</strong>.
		</div> -->
		--}}
    <div class="row">
			<div class="col-md-12">
			  <!-- Custom Tabs -->
			  <div class="nav-tabs-custom">
			  	<ul class="nav nav-tabs">
				    <li class="active">
							<a href="#tab_1" data-toggle="tab" aria-expanded="true">
								<span class="fa fa-user-secret" aria-hidden="true"></span> Datos Investigador
							</a>
						</li>
				    <li class="">
							<a href="#tab_2" data-toggle="tab" aria-expanded="false">
								<span class="fa fa-pencil-square-o" aria-hidden="true"></span> Proyectos
							</a>
						</li>
            @if (Auth::user()->type)
              <li class="">
                <a href="#tab_3" data-toggle="tab" aria-expanded="false">
                  <span class="fa fa-history" aria-hidden="true"></span> Historial
                </a>
              </li>
              <li class="pull-right">
                <a href="{{ route('investigadores.historial', ['id' => $investigador->id]) }}" class="btn btn-success"><i class="fa fa-gear"></i>Ampliar Historial</a>
              </li>
            @endif
				  </ul>
				  <div class="tab-content">
				    <div class="tab-pane active" id="tab_1">
							<div class="row">
								{!! Form::model($investigador, ['route' => ['investigadores.update', $investigador->id], 'method' => 'patch']) !!}
											@include('investigadores.fields')
								{!! Form::close() !!}
							</div>
				    </div><!-- /.tab-pane -->
				    <div class="tab-pane" id="tab_2">
							<div class="row">
								@include('investigadores.proyectos_asociados')
							</div>
				    </div><!-- /.tab-pane -->
            @if (Auth::user()->type)
            <div class="tab-pane" id="tab_3">
              @include('investigadores.historial')
            </div><!-- /.tab-pane -->
            @endif
				  </div><!-- /.tab-content -->
				</div><!-- nav-tabs-custom -->
			</div>
    </div>
  </section>
@endsection
