<section class="content">
	<div class="row">
		<h1 class="pull-right">
			<button class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" data-toggle="modal" data-target="#addNota">Nuevo</button>
		</h1>
	  <div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Notas</h3>
				</div>
				<div class="box-body">
					<table class="table table-bordered" id="investigadores-notas">
					  <thead>
					    <th>Fecha</th>
					  	<th>Usuario</th>
					    <th>Descripción</th>
					    <th>Editar</th>
					  </thead>
					  <tbody>
							@foreach($notas as $nota)
					      <tr>
					        <td>{!! $nota->fecha !!}</td>
					        <td>{!! $nota->user !!}</td>
					        <td>{!! $nota->descripcion !!}</td>
									<td>
										<button id="editNota" value="{!! $nota->id !!}" class="btn btn-default btn-xs" data-toggle="modal" data-target="#updateNota" onclick="mostrar(this)"><i class="glyphicon glyphicon-edit"></i></button>
									</td>
								<tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Agregar Nota Modal -->
{!! Form::open(['route' => ['investigadorNotas.store'], 'method' => 'post']) !!}
	@include('investigadores.notas.add_modal')
{!! Form::close() !!}
<!-- Actualizar Nota Modal -->
{!! Form::open(['route' => ['investigadorNotas.update', $investigador->id], 'method' => 'patch']) !!}
	@include('investigadores.notas.edit_modal')
{!! Form::close() !!}
