@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Investigadores</h1>
		<ol class="breadcrumb">
      <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
      <li><a href="{{ url('/investigadores') }}">Investigadores</a></li>
      <li class="active">Listar</li>
    </ol>
  </section>
  <section class="content">
  	<div class="clearfix"></div>
    @include('flash::message')
    <div class="clearfix"></div>
    <div class="box box-primary">
    	<div class="box-body">
          @include('investigadores.table')
      </div>
    </div>
  </section>
@endsection
