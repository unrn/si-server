<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $institucion->id !!}</p>
</div>


<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $institucion->nombre !!}</p>
</div>


<!-- Abreviacion Field -->
<div class="form-group">
    {!! Form::label('abreviacion', 'Abreviacion:') !!}
    <p>{!! $institucion->abreviacion !!}</p>
</div>


<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $institucion->created_at !!}</p>
</div>


<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $institucion->updated_at !!}</p>
</div>


