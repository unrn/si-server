@extends('layouts.app')

@section('content')
 	<section class="content-header">
    <h1>Instituciones</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ url('/instituciones') }}"> Instituciones</a></li>
			<li class="active">Crear</li>
		</ol>
  </section>
  <section class="content">
  	@include('adminlte-templates::common.errors')
    <div class="row">
      {!! Form::open(['route' => 'instituciones.store']) !!}

          @include('instituciones.fields')

      {!! Form::close() !!}
    </div>
  </section>
@endsection
