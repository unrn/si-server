<table class="hover table table-bordered" id="instituciones-table">
    <thead>
        <th>Nombre</th>
        <th>Abreviacion</th>
        <th colspan="3">Acción</th>
    </thead>
    <tbody>
    @foreach($instituciones as $institucion)
        <tr>
            <td>{!! $institucion->nombre !!}</td>
            <td>{!! $institucion->abreviacion !!}</td>
            <td>
                {!! Form::open(['route' => ['instituciones.destroy', $institucion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('instituciones.show', [$institucion->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-search"></i></a>
										@if(!Auth::guest())
	                    <a href="{!! route('instituciones.edit', [$institucion->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-edit"></i></a>
	                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Desea eliminar la Institución?')"]) !!}
										@endif
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $instituciones->links() !!}
</div>
