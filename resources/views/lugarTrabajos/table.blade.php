<table class="hover table table-bordered" id="lugarTrabajos-table">
    <thead>
        <th>Nombre</th>
        <th>Abreviación</th>
				<th>Institución</th>
        <th colspan="3">Accion</th>
    </thead>
    <tbody>
    @foreach($lugarTrabajos as $lugarTrabajo)
        <tr>
            <td>{!! $lugarTrabajo->nombre !!}</td>
            <td>{!! $lugarTrabajo->abreviacion !!}</td>
						<td>{!! $lugarTrabajo->institucion()->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['lugarTrabajos.destroy', $lugarTrabajo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('lugarTrabajos.show', [$lugarTrabajo->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-search"></i></a>
	                  <a href="{!! route('lugarTrabajos.edit', [$lugarTrabajo->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-edit"></i></a>
	                  {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Desea eliminar el Lugar de Trabajo?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $lugarTrabajos->links() !!}
</div>
