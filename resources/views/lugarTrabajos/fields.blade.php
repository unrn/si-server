<div class="col-md-6">
	<div class="box box-primary">
			<div class="box-body">
				<div class="form-group">
						{!! Form::label('nombre', 'Nombre (*):') !!}
						{{ Form::text('nombre', null, ['class' => 'form-control', 'required' => 'true']) }}
				</div>
				<div class="form-group">
						{!! Form::label('abreviacion', 'Abreviacion (*):') !!}
						{{ Form::text('abreviacion', null, ['class' => 'form-control', 'required' => 'true']) }}
				</div>
				<div class="form-group">
						{!! Form::label('institucion_id', 'Institución (*):') !!}
						{{ Form::select('institucion_id', $instituciones, null, ['class' => 'form-control', 'required' => 'true']) }}
				</div>
			</div>
			<!-- Submit Field -->
			<div class="box-footer">
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
			<a href="{!! route('lugarTrabajos.index') !!}" class="btn btn-default">Cancelar</a>
			</div>
	</div>
</div>
