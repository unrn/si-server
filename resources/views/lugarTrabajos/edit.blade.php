@extends('layouts.app')

@section('content')
  <section class="content-header">
  	<h1>Lugares de Trabajo</h1>
		<ol class="breadcrumb">
		  <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
		  <li><a href="{{ url('/integrantes') }}">Lugares de Trabajo</a></li>
		  <li class="active">Editar</li>
		</ol>
  </section>
  <section class="content">
  	@include('adminlte-templates::common.errors')
    <div class="row">
    	{!! Form::model($lugarTrabajo, ['route' => ['lugarTrabajos.update', $lugarTrabajo->id], 'method' => 'patch']) !!}

        		@include('lugarTrabajos.fields')

    	{!! Form::close() !!}
    </div>
  </section>
@endsection
