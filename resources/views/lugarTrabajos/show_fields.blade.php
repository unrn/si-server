<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $lugarTrabajo->id !!}</p>
</div>


<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $lugarTrabajo->nombre !!}</p>
</div>


<!-- Abreviacion Field -->
<div class="form-group">
    {!! Form::label('abreviacion', 'Abreviacion:') !!}
    <p>{!! $lugarTrabajo->abreviacion !!}</p>
</div>


<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $lugarTrabajo->created_at !!}</p>
</div>


<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $lugarTrabajo->updated_at !!}</p>
</div>


