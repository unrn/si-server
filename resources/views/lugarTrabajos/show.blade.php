@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Lugar de Trabajo</h1>
		<ol class="breadcrumb">
		  <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
		  <li><a href="{{ url('/integrantes') }}">Lugares de Trabajo</a></li>
		  <li class="active">Mostrar</li>
		</ol>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-body">
        <div class="row" style="padding-left: 20px">
          @include('lugarTrabajos.show_fields')
          <a href="{!! route('lugarTrabajos.index') !!}" class="btn btn-default">Volver</a>
        </div>
      </div>
    </div>
  </section>
@endsection
