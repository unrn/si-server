@extends('layouts.app')

@section('content')
	<section class="content-header">
	  <h1>Error de Página 403</h1>
	<section>
	<section class="content">
    <div class="error-page">
      <h2 class="headline text-orange">403</h2>
      <div class="error-content">
        <h3><i class="fa fa-warning text-orange"></i> Oops! Acceso No Autorizado.</h3>
        <h4><b>Usted no tiene acceso a ésta página.</b></h4>
				<p>
					<a href="{{ url()->previous() }}" class="btn btn-warning">Volver a la página anterior</a>
				</p>
	    </div>
	  </div><!-- /.error-page -->
	</section><!-- /.content -->
@endsection
