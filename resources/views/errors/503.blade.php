<!DOCTYPE html>
<html>
  <head>
    <title>Mantenimiento</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.8/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.8/css/skins/_all-skins.min.css">
    <style>
      body,h1 {
        font-family: "Raleway", sans-serif;
        background-color: LightGray
      }
      body, html {height: 100%}
      .container { padding: 70px; }
    </style>
  </head>
	<body>
    <div class="container">
      <div class="text-center">
        <img src="{{ url('/images/503.jpg') }}" alt="" width="200px" height="178px">
      </div>
			<div class="text-center">
				<h1>503 Página en Construcción</h1>
				<h2>No todo está perdido!</h2>
				<p>Estamos trabajando para mejorar el sistema</p>
				<br>
				<a class="btn btn-primary" href="https://www.unrn.edu.ar"><i class="fa fa-home"></i>Página de la UNRN</a>
			</div>
    </div>
	</body>
</html>
