@extends('layouts.app')

@section('content')
	<section class="content-header">
	  <h1>Error de Página 400</h1>
	<section>
	<section class="content">
    <div class="error-page">
      <h2 class="headline text-yellow">400</h2>
      <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! Error de sintaxis.</h3>
        <h4>
					<b>El servidor no puede o no procesará la solicitud debido a un error aparente del cliente.</b>
        </h4>
				<p>
					<a href="{{ url()->previous() }}" class="btn btn-warning">Volver a la página anterior</a>
				</p>
	    </div>
	  </div><!-- /.error-page -->
	</section><!-- /.content -->
@endsection
