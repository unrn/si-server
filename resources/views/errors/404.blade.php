<!DOCTYPE html>
<html>
  <head>
    <title>404 Not Found</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.8/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.8/css/skins/_all-skins.min.css">
    <style>
      body,h1 {
        font-family: "Raleway", sans-serif;
        background-color: LightGray
      }
      body, html {height: 100%}
      .container { padding: 70px; }
    </style>
  </head>
	<body>
    <div class="container">
      <div class="text-center">
        <img src="{{ url('/images/404.png') }}" alt="" width="150px" height="227px">
      </div>
      <div class="text-center">
				<h1>404 Oops! Página No Encontrada</h1>
				<h2>La página a la que intenta acceder no existe.</h2>
				<p>No todo está perdido!</p>
				<br>
				<a class="btn btn-primary" href="{{ url()->previous() }}"><i class="fa fa-home"></i>Volver al inicio</a>
			</div>
    </div>
	</body>
</html>
