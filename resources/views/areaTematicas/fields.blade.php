<div class="col-md-6">
	<div class="box box-primary">
		<div class="box-body">
			<!-- Nombre Field -->
			<div class="form-group">
		    {!! Form::label('nombre', 'Nombre (*):') !!}
		    {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<!-- Submit Field -->
			<div class="form-group col-sm-12">
		    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
		    <a href="{!! route('areaTematicas.index') !!}" class="btn btn-default">Cancelar</a>
			</div>
		</div>
	</div>
</div>
