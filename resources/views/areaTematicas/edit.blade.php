@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Área Temática</h1>
				<ol class="breadcrumb">
		      <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
		      <li><a href="{{ url('/areaTematicas') }}"> Área Temática</a></li>
		      <li class="active">Editar</li>
		    </ol>
   </section>
   <section class="content">
  		@include('adminlte-templates::common.errors')
    	<div class="row">
     		{!! Form::model($areaTematica, ['route' => ['areaTematicas.update', $areaTematica->id], 'method' => 'patch']) !!}
      		@include('areaTematicas.fields')
       	{!! Form::close() !!}
      </div>
   </section>
@endsection
