<table class="hover table table-bordered" id="areaTematicas-table">
    <thead>
				<th>ID</th>
        <th>Nombre</th>
        <th colspan="3">Acción</th>
    </thead>
    <tbody>
    @foreach($areaTematicas as $areaTematica)
        <tr>
						<td>{!! $areaTematica->id !!}</td>
            <td>{!! $areaTematica->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['areaTematicas.destroy', $areaTematica->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('areaTematicas.show', [$areaTematica->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-search"></i></a>
                    <a href="{!! route('areaTematicas.edit', [$areaTematica->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('A¿Desea eliminar el Área Temática?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $areaTematicas->links() !!}
</div>
