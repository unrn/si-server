<div class="col-md-6">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title">Investigadores ({!! $totalInvestigadores !!})</h3>
			<div class="box-tools pull-right">
				<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div id="chart-investigador"></div>
		</div><!-- /.box-body -->
	</div>
</div>
<div class="col-md-12">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title">Proyectos por Sede</h3>
			<div class="box-tools pull-right">
				<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
				<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
			</div>
		</div>
		<div class="box-body">
			<div id="chart-sedes"></div>
		</div><!-- /.box-body -->
	</div>
</div>
@section('scripts')
@parent
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	google.charts.load("current", {packages:['corechart', 'bar']});
	google.charts.setOnLoadCallback(drawChartInvestigadores);
	google.charts.setOnLoadCallback(drawChartInstituciones);
	function drawChartInvestigadores() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Generos');
    data.addColumn('number', 'Cantidad de Investigadores');
    data.addRows([
			/* jshint ignore:start */
			@foreach($sexos as $sexo)
				['{{ $sexo->sexo }}', {{ $sexo->cantidad }}],
			@endforeach
			/* jshint ignore:end */
    ]);
		var options = {
			width:'100%',
			height:350,
			legend:'top',
      pieSliceText: 'value-and-percentage',
			is3D: true
		};
		var chart = new google.visualization.PieChart(document.getElementById('chart-investigador'));
		chart.draw(data, options);
	}
	function drawChartInstituciones() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Sedes');
    data.addColumn('number', 'PI');
		data.addColumn({type:'number', role:'annotation'});
		data.addColumn('number', 'PICT');
		data.addColumn({type:'number', role:'annotation'});
		data.addColumn('number', 'PICTO');
		data.addColumn({type:'number', role:'annotation'});
		data.addColumn('number', 'PROEVO');
		data.addColumn({type:'number', role:'annotation'});
		data.addColumn('number', 'PROSAP');
		data.addColumn({type:'number', role:'annotation'});
		data.addRows([
			/* jshint ignore:start */
			@foreach($instituciones as $inst)
				[
					'{{ $inst->abreviacion }}',
					{{ $inst->pi }}, {{ $inst->pi }},
					{{ $inst->pict }}, {{ $inst->pict }},
					{{ $inst->picto}}, {{ $inst->picto}},
					{{ $inst->proevo}}, {{ $inst->proevo}},
					{{ $inst->prosap}}, {{ $inst->prosap}},
				],
			@endforeach
			/* jshint ignore:end */
		]);
		var options = {
			vAxis: {title: 'Proyectos'},
			hAxis: {title: 'Sedes'},
			seriesType: 'bars',
			width: '100%',
	    height: 350,
    };
    var chart = new google.visualization.ColumnChart(document.getElementById('chart-sedes'));
    chart.draw(data, options);
  }
</script>
@endsection
