<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-aqua"><i class="fa fa-graduation-cap"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Investigadores</span>
			<span class="info-box-text"><small>en total</small></span>
      <span class="info-box-number">{{ $docentes }}</span>
    </div><!-- /.info-box-content -->
  </div><!-- /.info-box -->
</div><!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-red"><i class="fa fa-product-hunt"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Proyectos</span>
			<span class="info-box-text"><small>en total</small></span>
      <span class="info-box-number">{{ $totalProyectos }}</span>
    </div><!-- /.info-box-content -->
  </div><!-- /.info-box -->
</div><!-- /.col -->
<!-- fix for small devices only -->
<div class="clearfix visible-sm-block"></div>
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-green"><i class="fa fa-lock"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Proximamente</span>
      <span class="info-box-number">nnn</span>
    </div><!-- /.info-box-content -->
  </div><!-- /.info-box -->
</div><!-- /.col -->
<div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="fa fa-lock"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Proximamente</span>
      <span class="info-box-number">mmm</span>
    </div><!-- /.info-box-content -->
  </div><!-- /.info-box -->
</div><!-- /.col -->
