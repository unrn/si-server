@extends('layouts.app')

@section('content')
<section class="content-header">
	<h1>Contacto</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
		<li class="active">Contacto</li>
	</ol>
</section>
<section class="content">
	@include('adminlte-templates::common.errors')
	{!! Form::open(['route' => 'contact.send']) !!}
		<div class="col-md-12">
	    <div class="box box-primary">
	      <div class="box-header with-border">
	        <h3 class="box-title">Redactar mensaje nuevo</h3>
	      </div><!-- /.box-header -->
	      <div class="box-body">
					<div class="form-group col-md-6">
						{!! Form::label('email', 'De:') !!}
						{!! Form::text('email', Auth::user()->email, ['class' => 'form-control', 'disabled' => 'true']) !!}
					</div>
					<div class="form-group col-md-6">
					  {!! Form::label('to', 'Para:') !!}
					  {!! Form::text('to', 'msanhueza@unrn.edu.ar', ['class' => 'form-control', 'required' => 'true', 'disabled']) !!}
					</div>
					<div class="form-group col-md-12">
					  {!! Form::label('subject', 'Asunto:') !!}
					  {!! Form::text('subject', null, ['class' => 'form-control', 'required' => 'true']) !!}
					</div>
					<div class="form-group col-md-12">
						{!! Form::label('messageBody', 'Mensaje:') !!}
						{!! Form::textarea('messageBody', null, ['class' => 'form-control', 'required' => 'true', 'size' => '30x5']) !!}
					</div>
	      </div><!-- /.box-body -->
	      <div class="box-footer">
	        <div class="pull-right">
	          <button type="submit" class="btn btn-primary"><i class="fa fa-send-o"></i> Enviar</button>
	        </div>
	        <a href="{!! url('/home') !!}" class="btn btn-default"><i class="fa fa-times"></i> Descartar</a>
	      </div><!-- /.box-footer -->
	    </div><!-- /. box -->
  	</div><!-- /.col -->
	{!! Form::close() !!}
</section>
@endsection
