@extends('layouts.app')

@section('content')
<section class="content-header">
	<h1> Inicio Administrador</h1>
	<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
  </ol>
</section>
<section class="content">
	@include('adminlte-templates::common.errors')
	<div class="clearfix"></div>
	@include('flash::message')
	<div class="clearfix"></div>
	<div class="row">
		@include('home.buscadores')
	</div>
	<div class="row">
		@include('home.info')
	</div>
	<div class="row">
		@include('home.charts')
	</div>
</section>
@endsection
<!-- Ver secciones de scripts en los archivos charts y buscadores. La utilización de la directiva @parent sirve para anidar scripts al html  -->
