<div class="col-md-12">
	<div class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title">Buscador Rápido</h3>
		</div>
		<div class="box-body">
			<div class="form-group col-sm-6">
				{!! Form::label('investigador_id', 'Buscar Investigador (*):') !!}
				<div class="input-group input-group-md">
					{!! Form::text('term', null, ['class' => 'form-control', 'id' => 'term', 'placeholder' => 'Buscar por apellido y/o nombre']) !!}
					<span class="input-group-btn">
						<a id="investigador_id" href="#" class="btn btn-success btn-flat">Buscar</a>
					</span>
				</div>
			</div>
			<div class="form-group col-sm-6">
				{!! Form::label('proyecto_id', 'Buscar Proyecto (*):') !!}
				<div class="input-group input-group-md">
					{!! Form::text('term2', null, ['class' => 'form-control', 'id' => 'term2', 'placeholder' => 'Buscar por código interno']) !!}
					<span class="input-group-btn">
						<a id="proyecto_id" href="#" class="btn btn-success btn-flat">Buscar</a>
					</span>
				</div>
			</div>
		</div><!-- /.box-body -->
	</div>
</div>
@section('scripts')
@parent
<script type="text/javascript">
	$(document).ready(function (){
		var user = {!! json_encode(Auth::user()->type) !!};
		$("#term").autocomplete({
	  	source: "/autocomplete/investigadores",
	  	minLength: 1,
	  	select: function(event, ui) {
				$('#term').val(ui.item.value);
				$('#investigador_id').attr('href', '/investigadores/'+ ui.item.id + (user ? '/edit' : ''));
	  	}
		});
		$("#term").keyup(function (){
			if ($("#term").val().length === 0) {
				$("#investigador_id").attr('href', '#');
			}
		});
		$("#term2").autocomplete({
			source: "/autocomplete/proyectos",
			minLength: 1,
			select: function(event, ui) {
				$('#term2').val(ui.item.value);
				$('#proyecto_id').attr('href', '/proyectos/'+ ui.item.id + '/edit');
			}
		});
		$("#term2").keyup(function (){
			if ($("#term2").val().length === 0) {
				$("#proyecto_id").attr('href', '#');
			}
		});
	});
</script>
@endsection
