<table class="table table-responsive" id="tipoProyectos-table">
    <thead>
				<th>ID</th>
        <th>Nombre</th>
        <th>Abreviacion</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($tipoProyectos as $tipoProyecto)
        <tr>
						<td>{!! $tipoProyecto->id !!}</td>
            <td>{!! $tipoProyecto->nombre !!}</td>
            <td>{!! $tipoProyecto->abreviacion !!}</td>
            <td>
                {!! Form::open(['route' => ['tipoProyectos.destroy', $tipoProyecto->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tipoProyectos.show', [$tipoProyecto->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-search"></i></a>
                    <a href="{!! route('tipoProyectos.edit', [$tipoProyecto->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Desea eliminar el Tipo de Proyecto?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $tipoProyectos->links() !!}
</div>
