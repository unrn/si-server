<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tipoProyecto->id !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $tipoProyecto->nombre !!}</p>
</div>

<!-- Abreviacion Field -->
<div class="form-group">
    {!! Form::label('abreviacion', 'Abreviacion:') !!}
    <p>{!! $tipoProyecto->abreviacion !!}</p>
</div>
