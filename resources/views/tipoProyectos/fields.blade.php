<div class="col-md-6">
	<div class="box box-primary">
		<div class="box-body">
			<!-- Nombre Field -->
			<div class="form-group">
			    {!! Form::label('nombre', 'Nombre (*):') !!}
			    {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<!-- Abreviacion Field -->
			<div class="form-group">
			    {!! Form::label('abreviacion', 'Abreviacion (*):') !!}
			    {!! Form::text('abreviacion', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
		</div>
		<!-- Submit Field -->
		<div class="box-footer">
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
			<a href="{!! route('tipoProyectos.index') !!}" class="btn btn-default">Cancelar</a>
		</div>
	</div>
</div>
