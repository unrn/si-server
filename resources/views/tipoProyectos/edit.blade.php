@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Tipos de Proyectos</h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li><a href="{{ url('/tipoProyectos') }}">Tipos de Proyectos</a></li>
	    <li class="active">Editar</li>
	  </ol>
  </section>
  <section class="content">
    @include('adminlte-templates::common.errors')
    <div class="row">
      {!! Form::model($tipoProyecto, ['route' => ['tipoProyectos.update', $tipoProyecto->id], 'method' => 'patch']) !!}

          @include('tipoProyectos.fields')

      {!! Form::close() !!}
    </div>
  </section>
@endsection
