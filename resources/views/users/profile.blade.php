@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Mi Perfil</h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li class="active">Mi Perfil</li>
	  </ol>
  </section>
  <section class="content">
  	<div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>
    <div class="col-md-5">
			{!! Form::open(['route' => 'users.profile.updateAvatar', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
			<div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="{{ url('/images/avatars/', Auth::user()->avatar) }}" alt="User profile picture">
          <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>
          <p class="text-muted text-center">{{ Auth::user()->email }}</p>
					<ul class="list-group list-group-unbordered">
            <li class="list-group-item">
							<div class="form-group has-feedback{{ $errors->has('avatar') ? ' has-error' : '' }}">
								{!! Form::hidden('_token', csrf_token()) !!}
								{!! Form::label('avatar', 'Cambiar Avatar (*):') !!}
								{{ Form::file('avatar', ['class' => 'form-control', 'required' => 'true', 'accept' => 'image/png, image/jpeg, image/jpg']) }}
								@if ($errors->has('avatar'))
									<span class="help-block">
										<strong>{{ $errors->first('avatar') }}</strong>
									</span>
								@endif
							</div>
            </li>
          </ul>
        </div><!-- /.box-body -->
				<div class="box-footer">
					{!! Form::submit('Guardar', ['class' => 'btn btn-primary btn-block']) !!}
				</div>
      </div>
			{!! Form::close() !!}
    </div>
		<div class="col-md-7">
			{!! Form::open(['route' => 'users.profile.resetPassword', 'method' => 'post']) !!}
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Editar Perfil de Usuario</h3>
				</div>
				<div class="box-body">
					{!! Form::hidden('_token', csrf_token()) !!}
					<div class="form-group has-feedback{{ $errors->has('password_actual') ? ' has-error' : '' }}">
						{!! Form::label('password_actual', 'Contraseña Actual (*):') !!}
						{!! Form::password('password_actual', ['class' => 'form-control', 'minlength' => 6, 'required' => 'true']) !!}
						@if ($errors->has('password_actual'))
							<span class="help-block">
								<strong>{{ $errors->first('password_actual') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
						{!! Form::label('password', 'Nueva Contraseña (*):') !!}
						{!! Form::password('password', ['class' => 'form-control', 'minlength' => 6, 'required' => 'true']) !!}
						@if ($errors->has('password'))
							<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						{!! Form::label('password_confirmation', 'Confirmar Contraseña (*):') !!}
						{!! Form::password('password_confirmation', ['class' => 'form-control', 'minlength' => 6, 'required' => 'true']) !!}
						@if ($errors->has('password_confirmation'))
							<span class="help-block">
								<strong>{{ $errors->first('password_confirmation') }}</strong>
							</span>
						@endif
					</div>
				</div>
				<div class="box-footer">
					{!! Form::submit('Restablecer Contraseña', ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
			{!! Form::close() !!}
		</div>
  </section>
@endsection
