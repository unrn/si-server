@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Usuarios</h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li><a href="{{ url('/usuarios') }}">Usuarios</a></li>
	    <li class="active">Crear</li>
	  </ol>
  </section>
  <section class="content">
    @include('adminlte-templates::common.errors')
    <div class="row">
      {!! Form::open(['route' => 'usuarios.store']) !!}

          @include('users.fields')

      {!! Form::close() !!}
    </div>
  </section>
@endsection
