<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Editar Usuario</h3>
		</div>
		<div class="box-body">
			<div class="form-group col-sm-12">
			    {!! Form::label('name', 'Nombre Completo (*):') !!}
			    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<div class="form-group col-sm-12">
			    {!! Form::label('email', 'Email (*):') !!}
			    {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<div class="form-group col-sm-12">
			    {!! Form::label('password', 'Password (*):') !!}
					<div class="input-group">
						{!! Form::password('password', ['class' => 'form-control', 'disabled' => true, 'minlength' => 6, 'required' => 'true']) !!}
      			<span class="input-group-addon">
        			<input type="checkbox" id="iPass">
      			</span>
    			</div>
			</div>
			<div class="form-group col-sm-12">
			    {!! Form::label('type', 'Tipo de Usuario (*):') !!}
			    {!! Form::select('type', ['0' => 'Usuario', '1' => 'Administrador'], null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
			<div class="form-group col-sm-12">
				<div class="checkbox icheck">
					<label><input type="checkbox" id="terminos"> Acepto los <a href="#">términos y condiciones</a></label>
				</div>
			</div>
		</div>
		<div class="box-footer" style="text-align:center;">
			{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
			<a href="{!! route('usuarios.index') !!}" class="btn btn-default">Cancelar</a>
		</div>
	</div>
</div>
@section('scripts')
<!-- iCheck -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">
<script>
    $(function () {
        $('#terminos').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%' // optional
        });
				$('#iPass').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            increaseArea: '15%' // optional
        });
    });
		$('#iPass').on('ifChecked', function(event){
  		$('#password').attr('disabled', false);
		});
		$('#iPass').on('ifUnchecked', function(event){
  		$('#password').attr('disabled', true);
		});
</script>
@endsection
