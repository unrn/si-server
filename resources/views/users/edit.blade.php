@extends('layouts.app')

@section('content')
  <section class="content-header">
  	<h1>Usuarios</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
			<li><a href="{{ url('/instituciones') }}"> Usuarios</a></li>
			<li class="active">Editar</li>
		</ol>
  </section>
  <section class="content">
  	@include('adminlte-templates::common.errors')
    <div class="row">
    	{!! Form::model($user, ['route' => ['usuarios.update', $user->id], 'method' => 'patch']) !!}

        		@include('users.fields')

    	{!! Form::close() !!}
    </div>
  </section>
@endsection
