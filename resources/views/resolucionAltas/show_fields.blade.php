<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $resolucionAlta->id !!}</p>
</div>

<!-- Numero Field -->
<div class="form-group">
    {!! Form::label('numero', 'Numero:') !!}
    <p>{!! $resolucionAlta->numero !!}</p>
</div>

<!-- Anio Field -->
<div class="form-group">
    {!! Form::label('anio', 'Anio:') !!}
    <p>{!! $resolucionAlta->anio !!}</p>
</div>

<!-- Fecha Field -->
<div class="form-group">
    {!! Form::label('fecha', 'Fecha:') !!}
    <p>{!! $resolucionAlta->fecha !!}</p>
</div>

<!-- Fecha Inicio Field -->
<div class="form-group">
    {!! Form::label('fecha_inicio', 'Fecha Inicio:') !!}
    <p>{!! $resolucionAlta->fecha_inicio !!}</p>
</div>

<!-- Fecha Fin Anual Field -->
<div class="form-group">
    {!! Form::label('fecha_fin_anual', 'Fecha Fin Anual:') !!}
    <p>{!! $resolucionAlta->fecha_fin_anual !!}</p>
</div>

<!-- Fecha Presentacion 1 Field -->
<div class="form-group">
    {!! Form::label('fecha_presentacion_1', 'Fecha Presentacion 1:') !!}
    <p>{!! $resolucionAlta->fecha_presentacion_1 !!}</p>
</div>

<!-- Fecha Fin Bienal Field -->
<div class="form-group">
    {!! Form::label('fecha_fin_bienal', 'Fecha Fin Bienal:') !!}
    <p>{!! $resolucionAlta->fecha_fin_bienal !!}</p>
</div>

<!-- Fecha Presentacion 2 Field -->
<div class="form-group">
    {!! Form::label('fecha_presentacion_2', 'Fecha Presentacion 2:') !!}
    <p>{!! $resolucionAlta->fecha_presentacion_2 !!}</p>
</div>

<!-- Fecha Fin Trienal Field -->
<div class="form-group">
    {!! Form::label('fecha_fin_trienal', 'Fecha Fin Trienal:') !!}
    <p>{!! $resolucionAlta->fecha_fin_trienal !!}</p>
</div>

<!-- Fecha Presentacion 3 Field -->
<div class="form-group">
    {!! Form::label('fecha_presentacion_3', 'Fecha Presentacion 3:') !!}
    <p>{!! $resolucionAlta->fecha_presentacion_3 !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $resolucionAlta->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $resolucionAlta->updated_at !!}</p>
</div>

