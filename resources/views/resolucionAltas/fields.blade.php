<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
	<!-- Numero Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('numero', 'Numero (*):') !!}
			    {!! Form::text('numero', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'numero/año']) !!}
			</div>

			<!-- Fecha Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('fecha', 'Fecha (*):') !!}
			    {!! Form::date('fecha', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-body">
			<!-- Fecha Inicio Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('fecha_inicio', 'Fecha Inicio (*):') !!}
			    {!! Form::date('fecha_inicio', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>

			<!-- Fecha Fin Anual Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('fecha_fin_anual', 'Fecha Fin Anual (*):') !!}
			    {!! Form::date('fecha_fin_anual', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>

			<!-- Fecha Presentacion 1 Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('fecha_presentacion_1', 'Fecha Presentacion 1:') !!}
			    {!! Form::date('fecha_presentacion_1', null, ['class' => 'form-control']) !!}
			</div>

			<!-- Fecha Fin Bienal Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('fecha_fin_bienal', 'Fecha Fin Bienal:') !!}
			    {!! Form::date('fecha_fin_bienal', null, ['class' => 'form-control']) !!}
			</div>

			<!-- Fecha Presentacion 2 Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('fecha_presentacion_2', 'Fecha Presentacion 2:') !!}
			    {!! Form::date('fecha_presentacion_2', null, ['class' => 'form-control']) !!}
			</div>

			<!-- Fecha Fin Trienal Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('fecha_fin_trienal', 'Fecha Fin Trienal:') !!}
			    {!! Form::date('fecha_fin_trienal', null, ['class' => 'form-control']) !!}
			</div>

			<!-- Fecha Presentacion 3 Field -->
			<div class="form-group col-sm-6">
			    {!! Form::label('fecha_presentacion_3', 'Fecha Presentacion 3:') !!}
			    {!! Form::date('fecha_presentacion_3', null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<!-- Submit Field -->
		<div class="box-footer" style="text-align: center;">
		  {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
		  <a href="{!! route('resolucionAltas.index') !!}" class="btn btn-default">Cancelar</a>
		</div>
	</div>
</div>
