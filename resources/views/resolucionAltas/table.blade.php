<table class="hover table table-bordered" id="resolucionAltas-table">
    <thead>
        <th>Número</th>
        <th>Fecha</th>
        <th>Fecha Inicio</th>
        <th>Fecha Fin Anual</th>
        <th>Fecha Fin Bienal</th>
        <th>Fecha Fin Trienal</th>
        <th colspan="3">Acción</th>
    </thead>
    <tbody>
    @foreach($resolucionAltas as $resolucionAlta)
        <tr>
            <td>{!! $resolucionAlta->numero !!}</td>
            <td>{!! Carbon\Carbon::parse($resolucionAlta->fecha)->format('d-m-Y') !!}</td>
            <td>{!! Carbon\Carbon::parse($resolucionAlta->fecha_inicio)->format('d-m-Y') !!}</td>
            <td>{!! Carbon\Carbon::parse($resolucionAlta->fecha_fin_anual)->format('d-m-Y') !!}</td>
            <td>{!! $resolucionAlta->fecha_fin_bienal != null ? Carbon\Carbon::parse($resolucionAlta->fecha_fin_bienal)->format('d-m-Y') : '<span style="color:red">NO POSEE</span>' !!}</td>
            <td>{!! $resolucionAlta->fecha_fin_trienal != null ? Carbon\Carbon::parse($resolucionAlta->fecha_fin_trienal)->format('d-m-Y') : '<span style="color:red">NO POSEE</span>' !!}</td>
            <td>
                {!! Form::open(['route' => ['resolucionAltas.destroy', $resolucionAlta->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('resolucionAltas.show', [$resolucionAlta->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-search"></i></a>
                    <a href="{!! route('resolucionAltas.edit', [$resolucionAlta->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Desea eliminar la Resolución de Alta?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $resolucionAltas->links() !!}
</div>
