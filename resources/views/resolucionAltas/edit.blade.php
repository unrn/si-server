@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Resoluciones de Alta</h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li><a href="{{ url('/resolucionAltas') }}">Resoluciones de Alta</a></li>
	    <li class="active">Editar</li>
	  </ol>
  </section>
  <section class="content">
    @include('adminlte-templates::common.errors')
  	<div class="row">
    	{!! Form::model($resolucionAlta, ['route' => ['resolucionAltas.update', $resolucionAlta->id], 'method' => 'patch', 'files' => true]) !!}

          @include('resolucionAltas.fields')

      {!! Form::close() !!}
    </div>
	</section>
@endsection
