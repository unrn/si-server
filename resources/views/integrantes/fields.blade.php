<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">Nuevo Integrante</h3>
		</div>
		<div class="box-body">
			<!-- Proyecto id -->
			{{ Form::hidden('proyecto_id', $proyecto->id) }}
			<!-- Investigador Field -->
			<div class="form-group col-md-8">
			    {!! Form::label('investigador_id', 'Investigador (*):') !!}
					@if(strpos(url()->current(), 'create'))
						{!! Form::text('term', null, ['class' => 'form-control', 'id' => 'term']) !!}
					@else
						{!! Form::text('term', $term, ['class' => 'form-control', 'id' => 'term']) !!}
					@endif
			    {!! Form::hidden('investigador_id') !!}
			</div>
			<!-- Perfil Field -->
			<div class="form-group col-md-4">
			    {!! Form::label('perfil_id', 'Perfil (*):') !!}
			    {!! Form::select('perfil_id', $perfiles, null, ['class' => 'form-control', 'placeholder' => 'Seleccionar un Perfil', 'required' => 'true']) !!}
			</div>
			<!-- Fecha Alta Field -->
			<div class="form-group col-md-4">
			    {!! Form::label('fecha_alta', 'Fecha Alta (*):') !!}
					@if(strpos(url()->current(), 'create'))
			    	{!! Form::date('fecha_alta', $proyecto->fecha_inicio, ['class' => 'form-control', 'required' => 'true']) !!}
					@else
						{!! Form::date('fecha_alta', null, ['class' => 'form-control', 'required' => 'true']) !!}
					@endif
			</div>
			<!-- Fecha Baja Field -->
			<div class="form-group col-md-4">
			    {!! Form::label('fecha_baja', 'Fecha Baja (*):') !!}
					@if(strpos(url()->current(), 'create'))
			    	{!! Form::date('fecha_baja', $proyecto->fecha_prorroga != null ? $proyecto->fecha_prorroga : $proyecto->fecha_fin, ['class' => 'form-control', 'required' => 'true']) !!}
					@else
						{!! Form::date('fecha_baja', null, ['class' => 'form-control', 'required' => 'true']) !!}
					@endif
			</div>
			<!-- Horas Field -->
			<div class="form-group col-md-4">
			    {!! Form::label('horas', 'Horas (*):') !!}
			    {!! Form::number('horas', null, ['class' => 'form-control', 'required' => 'true']) !!}
			</div>
		</div>
	</div>
</div>
<!-- Submit Field -->
<div class="col-md-12">
	<div class="box-footer">
		{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
		<a href="{!! route('integrantes.index') !!}" class="btn btn-default">Ver todos los integrantes</a>
	</div>
</div>
@section('scripts')
<script type="text/javascript">
	$(document).ready(function (){
		$( "#term" ).autocomplete({
	  source: "/autocomplete/investigadores",
	  minLength: 1,
	  select: function(event, ui) {
	  	$('#term').val(ui.item.value);
			$('#investigador_id').val(ui.item.id);
	  }
	});
	});
</script>
@endsection
