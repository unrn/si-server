@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Integrante para el proyecto <a href="{!! route('proyectos.edit', [$proyecto->id]) !!}" class="btn btn-success">{{$proyecto->codigo}}</a></h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li><a href="{{ url('/integrantes') }}">Integrantes</a></li>
	    <li class="active">Editar</li>
	  </ol>
  </section>
  <section class="content">
		<div class="clearfix"></div>
		@include('flash::message')
		<div class="clearfix"></div>
  	@include('adminlte-templates::common.errors')
    <div class="row">
    	{!! Form::model($integrante, ['route' => ['integrantes.update', $integrante->id], 'method' => 'patch']) !!}

            @include('integrantes.fields')

    	{!! Form::close() !!}
    </div>
  </section>
@endsection
