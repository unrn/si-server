<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
		  <h3 class="box-title">Nuevo Informe</h3>
		</div>
		<div class="box-body">
			<!-- Proyecto id -->
			{{ Form::hidden('proyecto_id', $proyecto->id) }}
			<!-- Tipo Field -->
			<div class="form-group col-sm-3">
				{!! Form::label('tipo', 'Tipo:') !!}
				{!! Form::select('tipo', ['Avance' => 'Avance', 'Final' => 'Final', 'Prorroga' => 'Pedido de Prorroga'],null, ['class' => 'form-control']) !!}
			</div>
			<!-- Fecha Presentacion Field -->
			<div class="form-group col-sm-3">
				{!! Form::label('fecha_presentacion', 'Fecha Presentacion:') !!}
				{!! Form::date('fecha_presentacion', null, ['class' => 'form-control']) !!}
			</div>
			<!-- Fecha Entrega Field -->
			<div class="form-group col-sm-3">
				{!! Form::label('fecha_entrega_papel', 'Fecha Entrega Papel:') !!}
				{!! Form::date('fecha_entrega_papel', null, ['class' => 'form-control']) !!}
			</div>
			<!-- Fecha Entrega Field -->
			<div class="form-group col-sm-3">
				{!! Form::label('fecha_entrega_digital', 'Fecha Entrega Digital:') !!}
				{!! Form::date('fecha_entrega_digital', null, ['class' => 'form-control']) !!}
			</div>
			<!-- Descripcion Field -->
			<div class="form-group col-sm-12">
				{!! Form::label('descripcion', 'Descripcion:') !!}
				{!! Form::textarea('descripcion', null, ['class' => 'form-control', 'size' => '30x3']) !!}
			</div>
		</div>
		<!-- Submit Field -->
		<div class="box-footer">
	    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
	    <a href="{!! route('informeProyectos.index') !!}" class="btn btn-default">Ver todos los informes</a>
		</div>
	</div>
</div>
