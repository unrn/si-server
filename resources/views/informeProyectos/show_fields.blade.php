<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $informeProyecto->id !!}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    <p>{!! $informeProyecto->tipo !!}</p>
</div>

<!-- Descripcion Field -->
<div class="form-group">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    <p>{!! $informeProyecto->descripcion !!}</p>
</div>

<!-- Fecha Presentacion Field -->
<div class="form-group">
    {!! Form::label('fecha_presentacion', 'Fecha Presentacion:') !!}
    <p>{!! $informeProyecto->fecha_presentacion !!}</p>
</div>

<!-- Fecha Entrega Field -->
<div class="form-group">
    {!! Form::label('fecha_entrega', 'Fecha Entrega:') !!}
    <p>{!! $informeProyecto->fecha_entrega !!}</p>
</div>
