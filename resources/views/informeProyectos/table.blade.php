<table class="hover table table-bordered" id="informeProyectos-table">
    <thead>
				<th>Proyecto</th>
        <th>Tipo</th>
        <th>Descripcion</th>
        <th>Fecha Presentacion</th>
        <th>Fecha Entrega Papel</th>
				<th>Fecha Entrega Digital</th>
        <th>Acción</th>
    </thead>
    <tbody>
    @foreach($informeProyectos as $informeProyecto)
        <tr>
						<td><a href="{!! route('proyectos.edit', [$informeProyecto->proyecto()->id]) !!}" class='btn btn-warning btn-xs'>{!! $informeProyecto->proyecto()->codigo !!}</a></td>
            <td>{!! $informeProyecto->tipo !!}</td>
            <td>{!! $informeProyecto->descripcion !!}</td>
            <td>{!! $informeProyecto->fecha_presentacion !!}</td>
            <td>{!! $informeProyecto->fecha_entrega_papel !!}</td>
						<td>{!! $informeProyecto->fecha_entrega_digital !!}</td>
            <td>
                {!! Form::open(['route' => ['informeProyectos.destroy', $informeProyecto->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Desea eliminar el Informe de Proyecto?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $informeProyectos->links() !!}
</div>
