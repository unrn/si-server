@extends('layouts.app')

@section('content')
  <section class="content-header">
    <h1>Informe Proyecto <a href="{!! route('proyectos.edit', [$proyecto->id]) !!}" class="btn btn-success">{{$proyecto->codigo}}</a></h1>
		<ol class="breadcrumb">
	    <li><a href="{{ url('/home') }}"><i class="fa fa-home"></i> Home</a></li>
	    <li><a href="{{ url('/informeProyectos') }}"> Informe Proyectos</a></li>
	    <li class="active">Editar</li>
	  </ol>
  </section>
  <section class="content">
    @include('adminlte-templates::common.errors')
    <div class="row">
    	{!! Form::model($informeProyecto, ['route' => ['informeProyectos.update', $informeProyecto->id], 'method' => 'patch']) !!}

          @include('informeProyectos.fields')

      {!! Form::close() !!}
    </div>
  </section>
@endsection
