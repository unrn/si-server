<table class="hover table table-bordered" id="perfiles-table">
    <thead>
        <th>Nombre</th>
        <th colspan="3">Accion</th>
    </thead>
    <tbody>
    @foreach($perfiles as $perfil)
        <tr>
            <td>{!! $perfil->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['perfiles.destroy', $perfil->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('perfiles.show', [$perfil->id]) !!}" class='btn btn-info btn-xs'><i class="fa fa-search"></i></a>
                    <a href="{!! route('perfiles.edit', [$perfil->id]) !!}" class='btn btn-success btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Desea eliminar el Perfil?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="pull-right">
	{!! $perfiles->links() !!}
</div>
