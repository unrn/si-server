<li class="header">MENU DE NAVEGACIÓN</li>
<li><a href="{{ url('/home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
<li><a href="{{ url('/investigadores') }}"><i class="fa fa-user-secret"></i> <span>Investigadores</span></i></a></li>
<li><a href="{{ url('/proyectos') }}"><i class="fa fa-folder-open"></i>Proyectos</a></li>
<li class="header">REPORTES</li>
<li>
  <a href="{{ url('/reportes/integrantes') }}">
    <i class="fa fa-pie-chart"></i> <span>Integrantes</span></i>
  </a>
</li>
<li>
  <a href="{{ url('/reportes/proyectos') }}">
    <i class="fa fa-line-chart"></i> <span>Proyectos</span></i>
  </a>
</li>
<li>
  <a href="{{ url('/reportes/proyectosActivos/get') }}">
    <i class="fa fa-bar-chart"></i> <span>Proyectos Vigentes</span></i>
  </a>
</li>
<li class="header">MAS OPCIONES</li>
<li><a href="{{ url('/profile') }}"><i class="fa fa-user"></i> <span>Mi perfil</span></a></li>
<li><a href="{{ url('/contact') }}"><i class="fa fa-envelope-o"></i> <span>Enviar Correo</span></a></li>
