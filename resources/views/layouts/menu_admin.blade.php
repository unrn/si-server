<li class="header">MENU DE NAVEGACIÓN</li>
<li><a href="{{ url('/home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
<li><a href="{{ url('/investigadores') }}"><i class="fa fa-user-secret"></i> <span>Investigadores</span></i></a></li>
<li class="treeview">
  <a href="#">
    <i class="fa fa-folder-open"></i> <span>Proyectos</span> <i class="fa fa-angle-left pull-right"></i>
  </a>
  <ul class="treeview-menu">
		<li><a href="{{ url('/proyectos') }}"><i class="fa fa-circle-o"></i> Listar Proyectos</a></li>
		<li><a href="{{ url('/informeProyectos') }}"><i class="fa fa-file-powerpoint-o"></i> <span>Informes</span></a></li>
		<li><a href="{{ url('/integrantes') }}"><i class="fa fa-users"></i> <span>Integrantes</span></i></a></li>
		<li><a href="{{ url('/tipoProyectos') }}"><i class="fa fa-tasks"></i> <span>Tipo Proyectos</span></a></li>
		<li><a href="{{ url('/resolucionAltas') }}"><i class="fa fa-toggle-up"></i> <span>Resol. de Alta</span></a></li>
  </ul>
</li>
<li><a href="{{ url('/especialidades') }}"><i class="fa fa-wrench"></i> <span>Especialidades</span></a></li>
<li><a href="{{ url('/lugarTrabajos') }}"><i class="fa fa-file-image-o"></i> <span>Lugares de Trabajo</span></a></li>
<li><a href="{{ url('/usuarios') }}"><i class="fa fa-users"></i> <span>Usuarios</span></a></li>
<li class="header">REPORTES</li>
<li>
  <a href="{{ url('/reportes/integrantes') }}">
    <i class="fa fa-pie-chart"></i> <span>Integrantes</span></i>
  </a>
</li>
<li>
  <a href="{{ url('/reportes/proyectos') }}">
    <i class="fa fa-line-chart"></i> <span>Proyectos</span></i>
  </a>
</li>
<li>
  <a href="{{ url('/reportes/proyectosActivos/get') }}">
    <i class="fa fa-bar-chart"></i> <span>Proyectos Vigentes</span></i>
  </a>
</li>
<li class="header">MAS OPCIONES</li>
<li><a href="{{ url('/instituciones') }}"><i class="fa fa-institution"></i> <span>Instituciones</span></a></li>
<li class="treeview">
  <a href="#">
    <i class="fa fa-cubes"></i> <span>Áreas</span> <i class="fa fa-angle-left pull-right"></i>
  </a>
  <ul class="treeview-menu">
		<li><a href="{{ url('/areaTematicas') }}"><i class="fa fa-circle-o"></i> Temáticas</a></li>
		<li><a href="{{ url('/areaSigevas') }}"><i class="fa fa-circle-o"></i> de SIGEVA</a></li>
  </ul>
</li>
<!-- <li><a href="#"><i class="fa fa-book"></i> <span>Expedientes</span></a></li> -->
<li><a href="{{ url('/perfiles') }}"><i class="fa fa-male"></i> <span>Perfiles</span></a></li>
<li><a href="{{ url('/roles') }}"><i class="fa fa-gear"></i> <span>Roles</span></a></li>
