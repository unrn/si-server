<!DOCTYPE html>

<html>
	<head>
	    <meta charset="UTF-8">
	    <title>SADI</title>
	    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
			<link rel="shortcut icon" href="https://cdn2.iconfinder.com/data/icons/occupations-2/500/occupation-48-256.png" >
	    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css"> -->
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.8/css/AdminLTE.min.css">
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.8/css/skins/_all-skins.min.css">
	    <!-- Ionicons -->
	    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
			<!-- Yajra DataTables -->
			<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css"> -->
			<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.css">
			<!-- jQuery UI -->
			<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.css"/>
			<!-- MorrisCharts -->
			<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
			<!-- <link href="http://jquery-ui-bootstrap.github.io/jquery-ui-bootstrap/css/custom-theme/jquery-ui-1.10.3.custom.css" rel="stylesheet"/> -->
	</head>
@if (Auth::user()->type)
	<body class="skin-red sidebar-mini">
@else
	<body class="skin-blue sidebar-mini">
@endif
    <div class="wrapper">
      <!-- Main Header -->
			<header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
					<span class="logo-mini">
						<b>A</b>LT
					</span>
					<span class="logo-lg">
            <b>Admin</b>LTE
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Notifications Menu -->
              <li class="dropdown notifications-menu">
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                  <!-- Menu Toggle Button -->
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                  <!-- The user image in the navbar-->
	                  <img src="{{ url('/images/avatars/', Auth::user()->avatar) }}"                       class="user-image" alt="User Image"/>
	                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
	                  <span class="hidden-xs">
	                    {{ Auth::user()->name}}
	                  </span>
                  </a>
                  <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                    <li class="user-header">
                      <img src="{{ url('/images/avatars/', Auth::user()->avatar) }}" class="img-circle" alt="User Image"/>
                      <p>
                      	{{ Auth::user()->name }}
												@if (Auth::user()->type)
													<small>ADMINISTRADOR</small>
												@else
													<small>USUARIO</small>
												@endif
											</p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                      <div class="pull-left">
                        <a href="{{ url('/profile') }}" class="btn btn-default btn-flat">Perfil</a>
                      </div>
                      <div class="pull-right">
                        <a href="{{ url('/logout') }}" class="btn btn-primary btn-flat">Salir</a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
						</li>
          </div>
        </nav>
    	</header>
      <!-- Left side column. contains the logo and sidebar -->
      @include('layouts.sidebar')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          @yield('content')
      </div>
      <!-- Main Footer -->
      <footer class="main-footer" style="max-height: 100px;text-align: center">
          <strong>Copyright © 2016 <a href="#">SICADyTT</a>.</strong> Todos los derechos reservados.
      </footer>
    </div>
		<!-- jQuery 2.1.4 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
		<!-- jQuery UI 1.12 -->
		<script   src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
    <!-- AdminLTE App -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.8/js/app.js"></script>

    <!-- Datatables -->
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

    @yield('scripts')
	</body>
</html>
