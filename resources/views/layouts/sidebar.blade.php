<aside class="main-sidebar" id="sidebar-wrapper">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ url('/images/avatars/', Auth::user()->avatar) }}" class="img-circle"
                     alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><a href="{{ url('/profile') }}">{{ Auth::user()->name}}</a></p>
                <!-- Status -->
                <a href="{{ url('/profile') }}"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- Sidebar Menu -->

        <ul class="sidebar-menu">
					@if (Auth::user()->type)
						@include('layouts.menu_admin')
					@else
						@include('layouts.menu_user')
					@endif
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
